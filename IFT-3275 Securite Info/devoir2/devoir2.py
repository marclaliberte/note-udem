#%%
#https://asecuritysite.com/encryption/sqm?val1=5&val2=128
def squareAndMultiply(x,y):
    exp = bin(y)
    value = x
 
    for i in range(3, len(exp)):
        value = value * value
        print i,":\t",value
        if(exp[i:i+1]=='1'):
            value = value*x
	    print i,"*:\t",value
    return value
#%%
#https://en.wikibooks.org/wiki/Algorithm_Implementation/Mathematics/Extended_Euclidean_algorithm
def EEA(a,b):
	if a == 0:
        return (b, 0, 1)
    else:
        g, x, y = EEA(b % a, a)
        return (g, y - (b // a) * x, x)

#%%
#https://gist.github.com/Ayrx/5884790
def millerRabin(n,k):
     if n == 2 or n == 3:
        return True

    if n % 2 == 0 :
        return False

    r, s = 0, n - 1
    while s % 2 == 0:
        r += 1
        s //= 2
    for _ in xrange(k):
        a = random.randrange(2, n - 1)
        x = pow(a, s, n)
        if x == 1 or x == n - 1:
            continue
        for _ in xrange(r - 1):
            x = pow(x, 2, n)
            if x == n - 1:
                break
        else:
            return False
    return True

#%%
def genKeys():
    