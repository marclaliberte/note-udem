# tp4 Revision a date

## AES

- Essentiellement un SPN
- longueure de block de 128 bits
- plusieurs clefs a certaines longueures (128, 192, 256), ce qui affecte le nombre de rounds et le key schedule

notre block est inserer dans une matrice 4x4 ou chaque bytes est inserer de gauche a droite, haut en bas.
on appel cette matrice le state

## Deroulement d'un round

Stade : 

1. Add Round Key : xor entre le state et notre sous-clef de round Ki
2. SubBytes : chaque bytes est substituer selon un lookup table. on le separer en 2 nibbles. on les mets dans un table (le moins signification en largeur et le plus signification en hauteur).
3. ShiftRows : Dans le state : 1ere row on fait rien, 2e row left shift rotationnel de 1 position, 3e row meme affaire de 2 position et 4e row de 3 position.
4. MixColumns : 

| boj| = |2|3|1|1|  | a0j|
| b1j| = |1|2|3|1|  | a1j|
| b2j| = |1|1|2|3|  | a2j|
| b3j| = |3|1|1|2|  | a3j|

cest un transformation lineaire.

Propriete qui si 2 inputs varient de  b>0 bytes, le output varie d'au moins 5-b bytes

donc un petit changement dans le plain text, un gros changement dans le output.

Il nous manque encore une cle a la fin, un SPN de n rounds, prend n+1 cles. 

Detail important : durant le dernier round on ne fiat pas le stade 4, il est remplacer par un xor avec une cle de plus (stade 1), cest la cle kn+1. 

Necessairement pour spn marche, il faut que ce soit reversbiel, donc les sub box sont 1:1

Si la table n'est pas rectangulaire, on ne pourra pas avoir n^2 possibilites, donc ce nest pas reversible. 

## RSA

Soit l'anneau
$$ z \bmod m =\{0, ..., m-1\} $$

un inverse :
$$ x * x^{-1}=e $$

Une fonction phi de euler :
$$ \phi(m) = le\ nombre\ dinputs\ x \in z \bmod m\ tq\ pgcd(x,m)=1$$
x et m sont premiers entre eux.

1 n'est pas considere comme nombre premier

ex : 
$$ \phi(6) = z \bmod m\ = \{0,1,2,3,4,5\} \\ pgcd(0,6) = 6\\ pgcd(1,6) = 1 \\ pgcd(2,6) = 2 \\ pgcd(3,6) = 3 \\ pgcd(4,6) =2 \\ pgcd(5,6) = 1 \\ \phi(6) = 2$$

### theoreme du calcul de phi

$$ m=p_1^{e1}, p_2^{e2}, ... , p_n^{en} $$
$$ \phi(m) = \prod_{i=1}^{n} (p_i^{e_i}-p_i^{e_i-1}) $$