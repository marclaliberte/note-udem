# tp5

## Rappel RSA

$C = ENC_{e_{kpub}} (m) \equiv m^e \bmod n$
$m=DEC_{C_{kpr}}(c) \equiv c^d \bmod n$
$kpub = (n,e)$
$kpr = (d)$

1. Choisir 2 grands nombres premiers p et q
2. Calculer $p\times q=n$ 
3. Calculer $\phi(n=(p-1)\times (q-1)$
4. Selectionner l'exposant public $e \in \{1,2,...,\phi-1\}$ tq $pcgc(e, \phi(n))$ fait en sorte que $e^{-1}$ existe 
5. Calculer KPrivee d tq $d \times e \equiv 1 \bmod \phi (n)$

------

## Exponentiation rapide modulaire 

algo naif
si on veut calculer $x^{13}$ on fait $x*x*x....$ 13 fois

en realite $|e|$ ou $|d|$ cest 1024 bits donc ca nous donne $2^{1024}$ possibilite, on peut pas brute force ca.

disons on a $x^{26} = (((x^2*x)^2)^2*x)^2$ ca nous fait 6 operations

Descriptions de l'algorithme de base (AKA square & mult)

$x^{26} = x^{{11010}_2} = x^{{h_4h_3h_2h_1h_0}_2}$

Les iterations : 

0. $x = x^{1_2}$ initialisation $\rightarrow h_4$
1. a) $(x^1)^2 = x^2 = x^{{10_2}}$ mult, h3=1
   b) $x^2*x = x^3 = x^{{11_2}}$
2. a) $(x^3)^2 = x^6 = x^{{110_2}}$ square $\rightarrow h_2$
   b) ya rien
3. a) $(x^6)^2 = x^{12} = x6{{1100_2}}$ square $\rightarrow h_1$
    b) $x^{12}*x = x^{13} = x^{{1101_2}}$ mult $h_1= 1$
4. a) $(x^{13})_2 = x^{26}$ squaure $\rightarrow h_0$ 

---------

## Sliding Window

Il est existe un algo qui sappel Sliding Window (cest ca qui va eter demadner pour le dev2).

ca ameliore lalgo de square mult de 25%. 

Expostant de taille t

nombres doperations  = 1.5t

donc pour $1024 \times 1.5 = 1536$ operations

### Encryption rapide

en theorie, on peut utiliser un e aussi petit qu'on veut en pratique. mais il faut que notre padding soit fait en consequence. 

les petites valeures de qui sont utiles :

|    e     |         $e_2$         | #mulit sqaure |
| :------: | :-------------------: | :-----------: |
|    3     |        $11_2$         |       2       |
|    17    |       $10001_2$       |       5       |
| $2^{16}$+1 | $10000000000000001_2$ |      17       |

cest des nombres qui ont un faible poid Hamming.

petite valeure de d implique une fouille exhaustive realiste et on veut eviter ca.

donc d va etre 1024 bits.

En tant que Bob, nous avons p et q

Au lieu d'effectuer un calcul long modulo n, nous pouvons en effectuer 2 plus court modulo p et modulo q en **3 etapes**

1. Transformation dans le domaine CRT (Chines reminder theorem)
2. Calcul dans le domaine CRT
3. Transformation inverse

faisont le par etapes (pas mal ca qui va falloir faire dans le devoir)

1. Representation modulaire de c $x_p \equiv c \bmod p$ et $x_q \equiv c \bmod q$
2. Exponentiation dans le domaine CRT $y_p \equiv x_p^{d_p} \bmod p$ et $y_q \equiv x_q^{d_q} \bmod q$
   1. $d_p \equiv d \bmod (p-1)$ 
   2. $d_q \equiv d \bmod (q-1)$
3. inverse de la transformation
   1. $m \equiv (q *K_p)y_p+(p * K_q)y_q \bmod n$ tq
   2. $K_p \equiv q^{-1} \bmod p$
   3. $K_q \equiv p^{-1} \bmod q$
   4. qui sont des valeures precalculess

EX:

$p = 11\\ q=13 \\ n = 11*13 = 143 \\ e = 7 \\ d = e^{-1} \equiv 103 \bmod 120\\ c=15$

calcul naif (on fait les etapes dans le rappel)

$15^{103} \bmod 143$

mais on veut pas ca

1. $x_p \equiv 15 \equiv 4 \bmod 11 \\ x_q \equiv 15 \equiv 2 \bmod 13$
2. $d_p \equiv 103 \equiv 3 \bmod 10 \\ d_q \equiv 103 \equiv 7 \bmod 12 \\ y_p \equiv 4^3 \bmod 11 \equiv 9 \bmod 11 \\ y_q \equiv 2^7 \bmod 13 \equiv 11 \bmod 13$
3. $K_p \equiv q^{-1} \bmod p \equiv 13^{-1} \bmod 11 \equiv 6 \bmod 11 \\ k_q \equiv p^{-1} \bmod q \equiv 11^{-1} \bmod 13 \equiv 13 \\ m \equiv (q *K_p)y_p+(p * K_q)y_q \bmod n) \equiv (13*6)9 | (11*6)11 \bmod 143 \equiv 707+726 =1428 \equiv 141 \bmod 143$
    1. kp et kq sont caculle avec euclide etendu

1428 cest le message clair

