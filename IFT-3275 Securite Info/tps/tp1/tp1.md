# TP1

chiffrement par decalage 

$$ K=3 \rightarrow cesar $$

$$ ENC_K (m_1 ... m_l) = c_1  ... c_l \\ c_i[(m_i+k)\bmod 26] \\ DEC_l(c_1, ... c_l) = m_1 ... m_l \\ m_i = [(c_i-k)\bmod 26]$$

espace cle : (les valeurs que peut prendre K)
$$ |K| = 26 $$

k = cest en fait le decalage utiliser

on peut toujours faire une analyse pour voir si les mots ont la bonne forme (si le texte fait du sens)

soit f_i, la frequence du ieme symbole d'un texte, 26 correspond aux notre de lettres dans lalphabet

$$ f_i = {1 \over{26}} \\ \sum_{i=0}^{25} f_i^2 = \sum_{i=0}^{25} (1/26)^2 \approx 0.038 $$

en anglais 
$$ \sum_{i=0}^{25} f_i^2 \approx 0.065 $$

soit h_i, la frequence d'une lettre dans un texte chiffre par decalage

on sait que
$$ f_i = h(i+k)\bmod 26 $$
on doit avoir un texte suffisament grand pour que la loie des grands nombres nous aide, bref pour que la distribution des lettre dans le message soit assez proche de la distributions des lettres habituelle dans le langage.

$$ \sum_{i=0}^{25} f_i^2  = \sum_{i=0}^{25} f_i(h(i+k)\bmod 26)  \approx 0.065$$
(en anglais)

ex : en anglais le "e" revient 14% du temps, et si on voit un "z" qui revient autour de 14%, ca risque detre un "e". 

On utlise le mod 26, pcq cest comme qui est encrypter. 

$$ \sum_{i=0}^{25} h_i^2 \approx 0.065$$

MESSAGE = 
```
JE DONNE UNE DEMO = message
AB CABCA BCA BCAB = cle  (qui est appliquer de maniere cyclique)
JF ...
```

soit
$$ K = \{K1, ... K_p\}\\ p= periode \\ \rightarrow M= \{m_1...m_l\} \\ c\{c_1... c_l\}$$

courant = lettres du chiffres suivant
ex : toutes les lettres ont ete chiffres par A
$$ c_j, c_{j+p}, c_{j+2p}... \\ 0 \leq j \leq p $$

il y v avoir un courrant pour chaque symbole qui compose la cle

si on connait p, il suffit de chifree le courant par decalage avec cette technique.

donc la question, esst comment est-ce quon trouve la valeur de p?

donc pour savoir si cest un courant, la distribution ne sera pas uniforme. si ce nest PAS un courant, sa distribution va etre uniforme. 

$$  \sum_{i=0}^{25} h_i^2 \approx 0.065 \rightarrow courant $$

dans le fond, cest une fouille exhaustive intelligente;

parlons-en

si a un password de chiffre , on va toucher au total a 
$$ 10^4 *4 $$
touches

comment on fait ca intelligemment, si la truc se rappelle toujours des 4 derniers caracteres quon a entrer

on prend 123456, on essaie 1234, 2345, 3456

soit
$$ k\ symboles \in \Sigma = \{k_1, ..., k_k\} $$
on a k^n chaines possibles quon va appeler des mots

un cycle de de Bruijn est une chaine cylique ou ces k^n mots sont observers exactement une fois comme sous-chaine.
$$ B(K,N) = cycle de deBruijn $$

ex:
$$ \Sigma =\{0,1\} \\ B(2,3) = 00010111 $$
```
000 = present
001 = present
010 = present
011 = present
100 = (present car cyclique)
101 = present
110 = pas la
111 = present
```

Comment on fait pour generer ca? (theorie des graphes)

## definition
Un graphe de deBruijn de n dimensions, est un graphe oriente, composer de
$$ k^n $$
sommets, donc k^n points de departs, k^n symboles dans la chaines
les sommets representent les mots de de longueure n des mots, a partir de k symboles dun alphabet et d'aretes representant les chevauchement de longueures n-1 entre ces mots. 

faisons un exemple

on fait un un graphe avec les 3 bits suivant et les aretes vont du suffixe au prefixe. les chevauchements sont de longueure 2. 

suffixe = 00, prefixe = 00 aussi

de 000 vers 001 et on ecris 0**00**1

de 000 vers lui meme = 0000

001 vers 010 = 0010

010 vers 101 = 0101

101 vers 011 = 1011

111 vers lui meme = 1111
et ainsi de suite pour tous les sommets

ensuite on va suivre un circuit hamiltonien dans le graphe (jamais 2 fois au meme sommet)

euler = arrete

hamilton = sommets




