# TP7

## Crible algebrique part 1

en anglais : general field number sieve (beginners guide) *recommanded*

### Section A, methode de factorisation de difference des carres

Nous voulons factoriser un nombre composer $n=pq$

Soient $s,r \in \Z$ tq $s^2 \equiv r^2 \bmod n$ ou $s^2-r^2 \equiv 0 \bmod n$

$n=pq$

| = divise

$n|(s^2-r^2) \implies$ $pq|(s^2-r^2) \implies$ $pq|(s+r)(s-r) \implies$, $p|(s+r)(s-r) \cup q|(s+r)(s-r)$

Lemme utile (thoerie des nombres) :

si $c|ab \cup pgcd(b,c)=1\implies c|a$

nous avons donc $p|(s+r) \cap p|(s-r)\\q|(s+r) \cap q|(s-r)$

autrement dit, il est impossible d'avoir p ne divise pas (s+r) and p ne divise pas (s-r)

### Scenarios de division possible

| $p/(s+r)$ | $p/(s-r)$ | $q/(s+r)$ | $q/(s-r)$ | $pgcd(pq, s+r)$ | $pgcd(pq, s-r)$ | factorisation possible? |
| :-------: | :-------: | :-------: | :-------: | :-------------: | :-------------: | :---------------------: |
|     F     |     V     |     F     |     V     |        1        |       pq        |           non           |
|     F     |     V     |     V     |     F     |        q        |        p        |           oui           |
|     F     |     V     |     V     |     V     |        q        |       pq        |           oui           |
|     V     |     F     |     F     |     F     |        p        |        q        |           oui           |
|     V     |     F     |     V     |     F     |       pq        |        1        |           non           |
|     V     |     F     |     V     |     V     |       pq        |        q        |           oui           |
|     V     |     V     |     F     |     V     |        p        |       pq        |           oui           |
|     V     |     V     |     V     |     F     |       pq        |        p        |           oui           |
|     V     |     V     |     V     |     V     |       pq        |       pq        |           non           |

probabilite de factorisation possible = 2/3 = 66%

### Section B

parametre libre

1. polynome : $f: \R \rightarrow \R$ muni du coefficent $a \in \Z$
2. $m \in \N$ tq $f(m) \equiv 0\bmod n$

Il est facile de trouver $f$ et $m$ si on determine $m$ en premier. Considerons l'expansion en base $m$ de $n$. 

$n=a_dm^d+a_{d-1}m^{d-1}+...+a_0$

nous pouvons definir $f$ comme etant $f(m)=a_dm^d+a_{d-1}m^{d-1}+...+a_0$ donc $f(m)=n\implies f(m)\equiv 0\bmod n$

### Section C 
 l'anneau $\Z[\theta]$

 soit $\theta \in \C$ une racine de f

 $\Z[\theta] =\{x|x=a_{d-1}\theta^{d-1}+a_{d-2}\theta^{d-2}+...+a_0,\ a_j \in \Z\}$

thm : en definissant la multiplication comme la multiplication de polynomesm $\Z[\theta]$ forme un anneau (skip la preuve)

Soient $A,B \in \Z[\theta]$. soit $a(x)$ et $b(x)$, deux polynome tq $a(\theta)=A$ et $b(\theta)=B$

Considerons $a(x)b(x)=e(x)f(x)+c(x)$

$AB=a(x)b(x)|_{x=\theta}= e(x)f(x)|_{x=\theta}+c(x)|_{x=\theta}$

$e(\theta)f(\theta)+C=e(\theta)*0 +C = C$

Note : $C \in \Z[\theta]$

De ce fait, $a(x)|_{x=\theta} \times b(x)|_{x=\theta}=[a(x)b(x)\bmod f(x)]|_{x=\theta}$