- [TP2](#tp2)
  - [Types dattaques](#types-dattaques)
  - [Reseau de Feistel](#reseau-de-feistel)
  - [DES](#des)
  - [Boites a substitution](#boites-a-substitution)
  - [Double encryption](#double-encryption)
    - [Procedure](#procedure)
  - [3DES](#3des)
  - [Cle faible](#cle-faible)
# TP2

## Types dattaques
1. Attaque a texte clair connu
$$ \{m, ENC_k(m)\} $$
le message m n'est pas choisi par lattaquant

2. Attaque a texte clair choisi
$$ \{m, ENC_k(m)\} $$
le message est choisi

3. Attaque a texte chiffre choisi
ou lattaquant possede ENC_k(m) pour un message m et DEC_k(c) pour un chiffre c choisir

$$ \{m, ENC_k(m)\} $$ 
= Cryptanalyse Differentielle

## Reseau de Feistel
Le bloc est separer en 2
$$ L_i-1\ et\ R_i-1 $$

L = left et R = right
On fait un Xou avec le F_i (et la cle K_i) et L_i-1 devient R_i et R_i-1 devient L_i, cest un round de Feistel

F_i est une fonction qui na pas besoin detre revesible (2 way function)

Demontrer que 1 round est reversible pour tout F_i

$$ output = (L_i, R_i) $$
quel est 
$$ (L_{i-1},\ R_{i-1}) $$

$$ R_{i-1} = L_i  \\ L_{i-1} = R_i \oplus F_i(R_{i-1}, K_i)$$
$$ \Rightarrow F_i\ can\ be\ one\ way\ function $$

![feistel](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Feistel_cipher_diagram_en.svg/1022px-Feistel_cipher_diagram_en.svg.png)

## DES
DES : data encryption standard

64 bits (8 blocs de 8 bits), chaque 8e bit de chaque bloc, cest un bit de parite. ya donc 56 bits utilsee pour vrai.
$$ |K| = 2^{56} $$

Si on revient a Feistel, on a 16 rounds, on a donc 16 F_i, et la cle va changer a chaque etape

cest un key schedule : cree 16 cles de 48 bits a partir de la master key de 56 bits.

ya donc 16 sous-cles de 48 bits. 

On prend la cle de 56 bits et on fait PC-1 

PC = permuted choice

cela fait une permutation des 56 bits et les separer le blocs en 2 (28 bits chacun) ensuite on bit shift chaque blocs vers la gauche de une ou 2 positions. 

PC-2 va selectionner 24 bits de chaques blocs (48 bits) pour former K_1 (premiere sous-cle) et ainsi de suite apres ca. 

![images](https://upload.wikimedia.org/wikipedia/commons/0/06/DES-key-schedule.png)

**petite note : la seule maniere de briser ca cest la force brute. le fait que cest rendu desuet cest que les ordinateurs sont rendus assez fort pour que 2^56 soit pas trop long a chercher**

Une fois que lon a nos 16 cles , cest la DES Mangler function.

Chaque bloc va etre de 64 bits.

On va les separer en 2 (Left et Right), on prend input de 32 bits et on lenvoie dans lexpangler function. qui nous donne un intermediaire de 48 bits

On prend notre cle de 48 bits et on fait un XOR avec lintermediaire, qui nous donne un autre intermediaire quon va separer en 6 bits.
$$ s_1,\ ...,\ s_8 $$

il faut allez voir cest quoi les tables de sboxes sur les internets. qui vont etre 4 bits chaque, on les concatene (ca nous fait 32 bits), on fait une permutation et ca nous donne un output de 32 bits.

il va donc y avoir une perte dinformation

pour chaque 4 entree de sbox, ca va donner la meme sortie, cest le principe qui fait que ce nest pas reversible.

![sbox](https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Data_Encription_Standard_Flow_Diagram.svg/500px-Data_Encription_Standard_Flow_Diagram.svg.png)
## Boites a substitution

Chaque sbox est unef onction 4 a 1, c-a-d 4 inputs sont associes a 1 outputs car 

1. chaque lignes du tableau contient chaque possibilite doutput
2. changer 1 bit de linput change au moins 2 bits de loutput.

cest le principe de diffusion 

mettons que linput : 101100, les bits du milieur 0110, 

LEFT = pour s_1, cela correspond a 2, output = 0010 our le left

RIGHT =  on prend le bloc juste en dessous, donc louput cest 1 = 0001

**notons que cest seulement 1 bit qui change**

-----------

## Double encryption

$$ ENC_{K_1, K_2}'(M) \equiv ENC_{K_2}(ENC_{K_1}(m))$$

Naif : 2^113 bits de securite -> s^2*56

Attaque "meet in the middle"

```
x = input
y = ouput
```
$$ y = ENC_{K_1, K_2}'(x) $$

supposons quon a des pairs (x,y)

cest donc une attaque a texte clair connu, qui est lattaque la plus basique. 

### Procedure
$$ 1.\ \forall K_1 \in \{0,1\}\ calculer\ z = DES_{k_1}(x) \\ store (z, k_1) \in L_1 \\ 2.\ \forall k_2 \in \{0,1\}\ calucler z= DES^{-1}_{k_2}(y) \\ 
store (z,k_2) \in L_2\\ 3.\ si\ \exists (z, k_1) \in L_1 \land (z,k_2) \in L_2\ nous\ savons\ que\ nous\ avons\ les\ bonnes\ valeurs\ de\ k_1, k_2
$$
$$ k_1 \rightarrow 2^{56} \\ k_2 \rightarrow 2^{56} $$
total :
$$ 2 \cdot 2^{56} = 2^{56+1} = 2^{57} $$

---------

## 3DES

$$ c = DES_{k_3}(DES_{k_2}^{-1}(DES_{k_1}(m))) $$

$$ m = DES_{k_1}^{-1}(DES_{k_2}(DES_{k_3}^{-1}(c))) $$

MEET IN THE MIDLE

$$ z = DES_{k_1}(m) \rightarrow 2^{56} \\ z = DES_{k_2}(DES_{k_3}^{-1}(y))  \rightarrow 2^{112} \\ 2^{56} + 2^{112} \approx 2^{112}$$

------

## Cle faible

Soit K une clef telle que
$$ DES_k(m) = DES_k^{-1}(m) $$

$$ k_{1+i} = k_{16-i} \\ i \in \{0,...,7\}$$

Il en existe 4
![weakkeys](https://en.wikipedia.org/wiki/Weak_key)

