# TP6

## Padding dans RSA & Hasching

les problemes avec RSA

- Probleme avec le *schoolbook* RSA c'est que cest deterministe et on ne veut pas ca...

cela permet des attaques a textes clairs choisis. 

on veut obtenir la securite semantique, cela veut dire que si par exemple on a un texte clair et 2 cyphers, on nest pas capbable de savoir lequel est le bon.

pour deux messagges $m_1$ et $m_2$, il est impossible de savoir lequel donne $ENC_{kPub}(m)=c$

cest la notion d'indistinguabilite

- il y a une 2e attaque, qui est lattaquer de Coppersmith

- les textes clair $m=0,1,-1$ donnent $c = 0,1,-1$ respectivement (cest un probleme)

- Utilisation des petits exposants publiques est problematique.

- Malleabilite : la transformation d'un chiffre en un autre, nous donne une transformation connue entre les messages clairs correspondants

Nous changeons le chiffre $c \rightarrow s^ec$ tq $s =\{1,...,n-1\}$

donc on fait $(s^ec)^d = s^{ed}m^{ed} \equiv sm \bmod n$, donc sans savoir c, on est capable de jouer avec la valeur du resultat.

------

Les manieres de faire du padding avec RSA

## OAEP - Optimal Asynmmetric Encryption Padding

cest ca quon va faire dans le dev2.

$m$ = messagaire

$k$ = |n| en bytes

$|H|$ = longueure de loutput d'une fonction de hashage

$|m|$ = longueure du message en bytes

$L$ = label ou string nulle par defaut

Protocole:

1. Generer une string $PS$ de longueure $k-|m|-2|H|-2$ de bytes nulles

(ce calcul peut donner 0, cest permis) donc $|PS| = 0$

2. Concatener `Hash(L)`, $PS$, `0x01` et le message $m$

nous appelons ce resultat le `DATA BLOCK`, $DB$ et $|DB|=k-|H|-1$

$DB = Hash(L) || PS || 0x01 || m$

3. Generer une chaine de bytes aleatoires de longueure $|H|$ cest le `seed`
4. $dbMask = MGF(seed, k-|H|-1)$

le MGF cest en fait SHA-1, mgf cest le mask generator function

5. $maskedDB= DB \oplus dbMask$
6. $seedMask = MGF(maskedDB, |H|)$
7. $maskedSeed = seed \oplus seedMask$
8. Concatener une byte null, maskedSeed, maskedDB = message encoded $EM$


|  0x00   | $seed \oplus seedMask$ | $(Hash(L) \cdot PS \cdot 0x01 \cdot m) \oplus dbMask$ |
| :-----: | :--------------------: | :---------------------------------------------------: |
| 1 bytes |          $H$           |                        $k-H-1$                        |

tout ca est de longueure k

--------------

## Fonction de hashage cryptographique

