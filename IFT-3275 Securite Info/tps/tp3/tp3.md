# tp3

## Boites a substituion

| Input  |   0   |   1   |   2   |   3   |   4   |   5   |   6   |   7   |   8   |   9   |   A   |   B   |   C   |   D   |   E   |   F   |
| :----: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| output |   E   |   4   |   D   |   1   |   2   |   F   |   B   |   8   |   3   |   A   |   6   |   C   |   5   |   9   |   0   |   7   |

subkey k1 mixing cest basicly un XOR

sil y a 4 roundes, cest 5 cles. il faut noter quand dans la vrai vie il devrait y avoir une subsitution entre la sortie et le subkey k5 mixing.

si on a pas la 5e souscle, on peut commencer a inverser les permutations et susbistution, cest comme si le round nexistait pas. on peut passer direct a la k4.

un table de la sorte il y a 16! (factorielle), comment la choisir? on prend celle qui ne se fait pas exploiter par ce quon va montrer aujourdhui.

## Difference

x' = 001010
x''= 011000
delta sera la difference (xor)

$$ \Delta X = 010010  \\ \Delta = a \oplus B$$

|   A   |   B   | Delta |
| :---: | :---: | :---: |
|   0   |   0   |   0   |
|   1   |   0   |   1   |
|   0   |   1   |   1   |
|   1   |   1   |   0   |

ex :

$$ \Delta X = B \\ \Delta Y  \\ \Delta X = 1011 \\ x' \oplus (x' \oplus \Delta X) $$
x' = 0110
x''= 1101

si x' = 0000 cest trivial pas mal comme operation

x' = 6 ,correspond a B dans la table de substitution 11 = 1011

Y' = 1011

pour y'' on par de x'' et on regarde la table cnore
Y'' = 1001

$$ \Delta Y = Y' \oplus Y'' = S(x') \oplus S(x' \oplus \Delta X) \\ \Delta Y = 0010 = 2 $$

si on prend delta x = 1011, on a une chance sur 2 que delta y = 0010. 

pour trouver le delta y, ca prend 2 for loop imbrique. 

```
i=0-16
    j = 0-16
        S(x') xor S(x' xor \Delta X)
```

ca va donner la table de frequence (no4 thoerique du devoir)

il dit que cest une bonne idee de commecner avec le 8 pour le devoir.

delta P = 0000 1011 0000 0000
delta I = 0000 0110 0000 0110

$$ \Delta P , \Delta I $$
cest la differentielle caracteristique

$$ P(\ observer \ (\Delta P, \Delta I)) = 1/2 (1/16)^3  = 0.02636$$

cest une probabilite plus haute quelle devrait etre

la sous cle partielle de k5 = k5* cest une cle de 8 bits.

pour le devoir, il suffit de commecner avec la plus haute prob 8. 

le delta i devrait nous donner 2 fois 4 0 de suites

$$ \Delta X = x' \oplus x'' \\ = (x' \oplus k_1^*) \oplus (x'' \oplus k^*_1) \\ = x' \oplus (k_1^* \oplus k_1 ^*) \oplus x'' \\ 
= x' \oplus x'' = \Delta X
 $$

Les no
1. theoreme de secracy de shannon
2. tp2


https://diffcryptanal.herokuapp.com

ce quon doit sender

```json
{
    "plaintext": ["0000000000000001"],
    "team_num" : 0
}
```
On doit utiliser le SPNServer.java, qui va soccuper de sender la request pour nous.

le encrypt method prend un arraylist

```java
SPNServer server = new SPNServer();

int team_num = 0;
ArrayList<String> req = new ArrayList<String>();
req.add("0000000000000001")
req.add("0000000000000011")

server.encrypt(req, team_num)
```