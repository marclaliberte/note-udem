# TP8

## Sections E-I

Definition : une base de facteurs rationnels est un ensemble fini de nombre premiers. $R=\{p: p\ est\ premier\}$ tq $p \leq M,\ M \in \N$

Definition : un entier $L \in \Z$ est lisse sur $R$ si $R$ contient tous les facteurs premiers de $L$

lisse ca veut dire que : voir le document suection II-E

definition: une base de facteur algebrique est un ensemble fini $\{a+\theta b\} \in \Z[\theta]$ ou $a,b \in \Z$ et $\forall(a,b) \nexists c, d, e \in \Z [\theta]$ tq $d=a+\theta b$ et $a + \theta b$ est un nombre premier ideal.

def : un element $L \in \Z[\theta]$ est lisse sur une base de faceurs algebriques $A\ si\ \exists WCA$ tq $\Pi\ c+\theta d= L$

theoreme : soit $f(x)$ un polynome avec coefficients entirs et $\theta \in \C$ une racine de $f(x)$. l'ensemble des paires $\{(r,p)\}$ ou p est premeir et $r \in \Z[\theta]$ et $f(r) \equiv 0 \bmod p$ est une correspondances bijective avec l'ensemble $a+\theta b \in \Z[\theta]$ qui satisfont le critere pour former $A$.

## Section E-II

Soit $R$ composees de nombre premiers $q_i$ et $A \in \Z[\theta]$ representer par  $\{(r,p)\}$ 