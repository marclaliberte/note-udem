# Devoir 1
## par Marc Laliberté matricule : 20085533 et Meshleen Hannoun matricule: 20055461

---------

### No.1

Comme il a été démontré dans le théorème de Shannon
pour avoir un cryptosystème avec le parfait secret (perfect secrecy)
$$ H(K) \geq H(M)$$

alors
$$ |K| \geq |M| $$

Soit
$$ K=M=C = \{0,1\}^n $$
les ensembles possibles de clefs (K), message(C) et message cryptes (C)

Si on dit que les clef
$$ K=0^l $$
sont interdites, alors
$$ |K| = |M| - 1 < |M| $$
ce qui viole le theoreme

Il n'est donc pas nécesseraire d'utiliser des générateurs de bits qui ne génèrent pas des clefs avec seulement des 0.

------------

### No.2

Demontrez que
$$ Fiestel_{f_1,f_2}(L_0,R_0) = (L_2, R_2) \Rightarrow Feistel_{f_2, f_1}(R_2,L_2) = (R_0,L_0) $$

Chaque etape d'un reseau de Feistel marche comme suit :

$$ L_{i+1} = R_i \\ R_{i+1} = L_i \oplus f(R_i, K_i)$$

Apres avoir applique n etapes d'un reseau de Feistel au texte clair 

$$ L_0\ et\ R_0 $$
avec un key schedule
$$ K_0,\ ...\ ,\ K_{n-1} $$
On obtient le message crypte 
$$ L_n\ et\ R_n $$
On peut demontrer que la decryption peut se faire en appliquant la meme technique d'encryption a Ln et Rn
avec un key shcedule inverse
$$ K_{n-1},\ ...\ ,\ K_0 $$
Nous obtenons
$$ R_i = L_{i+1} \\ L_i = R_{i+1} \oplus f(R_i, K_i) $$

Alors, apres avoir appliquer la technique a Ln et Rn avec la clef Kn-1, nous obtenons
$$ L_{n-1}\ et\ R_{n-1} $$
Appliquons la technique encore une fois avec la clef Kn-2
$$ L_{n-2}\ et\ R_{n-2} $$
Donc si on l'applique n fois avec le key schedule
$$ K_{n-1},\ ..., \ K_0 $$
Nous obtenons la fin 
$$ L_0\ et\ R_0 $$

------

### No.3

Si 
$$ DES_k(m) = \overline {DES_{\overline  k}(\overline m)} $$
alors
$$ L_i = R_{i-1} \Rightarrow \overline {L_i} = \overline{R_{i-1}} $$
Donc
$$ \overline{R_i} = \overline{L{i-1}} \oplus f(\overline{R_{i-1}}, \overline{k_i}) = \overline{L_{i-1}} \oplus f(R_{i-1}, k_i) = \overline{L_{i-1} \oplus f(R_{i-1}, k_i)} $$

$$ f(\overline R, \overline K) = f(R,K) $$
parce que dans la fonction, le complément de l'expansion R et le complément de la clef k sont XOR entre eux, ce qui annule le complément. 

Cela nous donne le complement d'un message chiffré lorsqu'on utilise le complement du message et de la clef. 

--------

### No.4

#### Table de differentiation

![table](tableImg.png)

Selon notre table voici les differentielles des subboxes

$$ s_{12}: \Delta X = E \rightarrow \Delta Y = 1  \\s_{24}: \Delta X = 4 \rightarrow \Delta Y = 4  \\s_{32}: \Delta X = 1 \rightarrow \Delta Y = 9 $$

ce qui nous donne
$$ \Delta P = 0000\ 1110\ 0000\ 0000 \\ \Delta I = 0100\ 0000\ 0000\ 0100 $$

Voici le schema

![schema](no4.jpg)



Notre cle maitre dequipe : `00111010101110100000`

Nous avons meme comparer avec une methode de vrai force brute sur les 20 bits pour etre sur que cela marchait ainsi qu'une methode qui test un plainText, qu'on a rouler plusieurs fois pour etre sur que c'etait bon.

-------

### No.5

Nous avons opté de le faire en javascript etant donné notre aisance avec ce language. C'etais beaucoup plus facile et rapide de faire des tests et se retourner rapidement. 

Voir le fichier Decrypt.js qui sera en annexe de nos fichiers de remise.

Le longueure de la cle est de 200 et elle est 

`iusedtobearenegadeiusedtofoolaroundbuticouldnttakethepunishmentandhadtosettledownnowimplayingitrealstraightandyesicutmyhairyoumightthinkimcrazybutidontevencarecauseicantellwhatsgoingonitshiptobesquare`

Le fichier resultant sera aussi en annexe de notre fichier de remise.

----------

### No.6

Nous avons trouver la sequence 10 000 resultante des cycles montrés dans l'exemple ainsi que les 40 000 mots resultants de le sequence, decale un a un. La sequence de 40 000 ne sera pas imprimee.