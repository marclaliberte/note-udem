/* Marc Laliberte, 20085533
 * Meshleen Hannoun, 20055461
 */

import java.util.*;

public class Bruijn{
	
	public static void print(String p) {
		System.out.println(p);
	}
	
	public static void printArray(int[] a) {
		for(int i=0;i<a.length;i++) {
			System.out.print(a[i]);
		}
	}
	public static int[] getOriginalWord(int size) {
		int[] output = new int[size];
		for(int i=0;i<size;i++) {
			output[i]=i%10;
		}
		return output;
	}
	
	public static int getFirstIndex(int[]a, int x) {
		
		for(int i =0; i<a.length;i++ ) {
			if(a[i]==x) {
				return i;
			}

		}
		return -1;
	}
	
	
	//Génère un cycle de de Bruijn pour des mots de longueur n
	//composés de k symboles différent.	
	public static String B(int k, int n){
		String result = "";

		//Taille de result
		int size = (int) Math.pow(k,n);
		//int size = 10;
		//Emmagasine en mémoire quel index du "mot du haut"
		//a été visité
		BitSet visited= new BitSet(size);
		//TO DO : Quelques variables à initialiser içi
		int[] word = getOriginalWord(size);
		int[] sortedWord = new int[size];
		
		System.arraycopy(word, 0, sortedWord, 0, size);
		
		Arrays.sort(sortedWord);
		print("Mot du haut");
		printArray(sortedWord);
		System.out.println();
		print("Mot original");
		printArray(word);
		System.out.println();

		print("Cycles:");
		while(result.length() != size){
			int indexDepart = visited.nextClearBit(0);
			result += sortedWord[indexDepart];
			visited.set(indexDepart,true);
			
			int newIndex=-1;
			String cycle = "("+indexDepart+",";
			
			//generate a cycle
			// should break if newIndex == indexDepart || newIndex < indexDepart
			int depart2 = indexDepart;
			while(true) {
				newIndex = getFirstIndex(word, sortedWord[depart2]);
				
				if(newIndex == indexDepart) {
					word[newIndex]=-3;
					visited.set(newIndex,true);
					cycle+=indexDepart+")";
					break;
				}
				
				result+=sortedWord[newIndex];
				visited.set(newIndex,true);
				word[newIndex] = -1;
				cycle+=newIndex+",";
				depart2 = newIndex;
				//break;
			}

		print(cycle);
		//break;
			
		}
		print("Final Sequence: ");
		print(result);
		return result;
	}
	
	private static String[] allWords(String s) {
		String[] output = new String[40000];
		for(int i=0;i<10000;i++) {
			int x = i+4;
			if(x>=s.length()) {
				output[i]= s.substring(i)+s.substring(0,x%10000);
			}else {
				output[i] = s.substring(i,x);
			}
			
		}
		return output;
	}

	
	public static void main(String args[]){

		//print(B(10,4));
		String word = B(10,4);
		//print(allWords(word)+"");

	}
}