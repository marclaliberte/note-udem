/* Marc Laliberte, 20085533
 * Meshleen Hannoun, 20055461
 */

var fs = require("fs");
var text;
var charArray=[ 'a','b','c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

var freqJson = {
    a: 0.082,
    b: 0.015,
    c: 0.028,
    d: 0.043,
    e: 0.127,
    f: 0.022,
    g: 0.02,
    h: 0.061,
    i: 0.07,
    j: 0.02,
    k: 0.08,
    l: 0.04,
    m: 0.024,
    n: 0.067,
    o: 0.015,
    p: 0.019,
    q: 0.001,
    r: 0.06,
    s: 0.063,
    t: 0.091,
    u: 0.028,
    v: 0.02,
    w: 0.024,
    x: 0.002,
    y: 0.02,
    z: 0.001
};


/* Helper functions */
function print(text) {
    console.log(text);
}

function isLetter(str) {
    return str.length === 1 && str.match(/[a-z]/i);
}

function getInitH(){
    var h = {};
    for(var keys in freqJson){
        h[keys] = 0
    }
    return h
}
/* read/write functions */
function readMainfile() {
    return fs.readFileSync("cipher.txt").toString();
}

function writeMainFile(text) {
    fs.writeFile("./result.txt", text, function(err) {
        if (err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    });
}

/* Main functions */
function getAllFis(text, isForKey){
    return calcDistributionTotal(calcDistribution(text),isForKey)
}

function calcDistributionTotal(object, isForKey) {
    var output = 0;

    for (var keys in object) {
        if(!isForKey)
            output += Math.pow(object[keys], 2);
        else
            output += object[keys]*freqJson[keys]
    }
    return output;
}


function calcDistribution(seq) {
    var h = getInitH();
    for (var i = 0; i < seq.length; i ++) {
        if (isLetter(seq.charAt(i))) {
            h[seq.charAt(i)]++;
        }
    }

    for (var keys in h) {
        h[keys] = h[keys] / seq.length;
    }
    return h
}

function getCourant(text, period, debut){
    var output = ""
    var index =0

    if(debut != 0){
        while(debut >0){
            if(isLetter(text.charAt(index)))debut--;
            index++;
        }
    }
    for(index; index < text.length;){
        if(!isLetter(text.charAt(index)))index++;
        else{
            output+=text.charAt(index)
            var scannerHelper = period
            while(scannerHelper > 0){
                index++
                if(index >= text.length)break;
                if(isLetter(text.charAt(index)))scannerHelper--   
            }
        }   
    }
    return output;
}

function getKeySize(text, tolerance) {
    var error = 0.065
    for(var k=2;k<1000;k++){
        var courant=getCourant(text, k);
        var fi = getAllFis(courant, false)
        var diff = Math.abs(0.065-fi)
        if(diff<= tolerance && diff < error){
            error=diff;
            var bestKeySize = k
        }
    }
    return bestKeySize   
}

function getKey(text, keySize) {
    var output=""
    for(var i=0;i<keySize;i++){
        var courant = getCourant(text, keySize, i)
        var bestDiff = 0.065
        var bestK;
        for(var k=0;k<26;k++){
            var newCourant = decrypt(courant, charArray[k])
            var fi = getAllFis(newCourant, true)
            if(Math.abs(fi-0.065) < bestDiff){
                bestDiff = Math.abs(fi-0.065);
                bestK=k;
            }
        }
        output+=charArray[bestK]
    }

   return output;
}

function decrypt(text, key) {
    var result = ""
    var keyIncrement=0
    for(var i =0 ;i< text.length;i++){
        if(isLetter(text.charAt(i))){
            var offsetKey = charArray.indexOf(key.charAt(keyIncrement%key.length))
            var cipherChar = text.charAt(i)
            var offsetText = charArray.indexOf(cipherChar)
            var real = offsetText-offsetKey;
            result+= charArray[(real+26)%26];
            keyIncrement++;
        }else{
            result+=text.charAt(i)
        }
    }
    return result;
}

function main() {
    text = readMainfile();
    var tolerance =0.004

    print("Computing key size... please wait...")
    var keySize = getKeySize(text, tolerance)
    print("keySize : "+ keySize)

    print("Computing key... please wait...")
    var key = getKey(text, keySize);
    print("key : "+ key)

    text = decrypt(text, key)

    writeMainFile(text)
}

main();
