/* Marc Laliberte, 20085533
 * Meshleen Hannoun, 20055461
 */

import java.util.*;

public class Differential{
	//Écrire votre numéro d'équipe içi !!!
	public static int teamNumber = 10;
	public static String masterKeyExemple = "00100100001111010101";
	
	
	public static SPNServer server = new SPNServer();

	//Différentielle d'entrée \Delta_P
	//ex : ""0000101100000000""
	public static String plain_diff_exemple = "0000101100000000";
	public static String plain_diff = "0000111000000000";

	//Différentielle intermédiaire \Delta_I
	//ex : "0000011000000110"
	public static String int_diff_exemple = "0000011000000110";
	public static String int_diff =   "0100000000000100";

	//Boîte à substitutions de l'exemple de la démonstration #3
	public static String[] sub_box_exemple = new String[]{"1110", "0100", "1101", "0001", "0010", "1111", "1011", "1000",
			   												"0011", "1010", "0110", "1100", "0101", "1001", "0000", "0111"};

	public static String[] sub_box_inv_exemple = new String[]{"1110", "0011", "0100", "1000", "0001", "1100", "1010", "1111", 
				   									  			"0111", "1101", "1001", "0110", "1011", "0010", "0000", "0101"};
	
	//Sorties des boîtes à substitutions du SPN
	public static String[] sub_box = new String[]{"0010", "1011", "1001", "0011", "0111", "1110", "1101", "0101", 
												  "1100", "0110", "0000", "1111", "1000", "0001", "0100", "1010"};

	//Entrées des boîtes à substitutions du SPN
	public static String[] sub_box_inv = new String[]{"1010", "1101", "0000", "0011", "1110", "0111", "1001", "0100", 
												      "1100", "0010", "1111", "0001", "1000", "0110", "0101", "1011"};

	//Permutations : --> Notez que la permutation "perm" inverse est la même puisqu'elle est symmétrique
	public static int[] perm = new int[]{0, 4, 8, 12, 1, 5, 9, 13, 2, 6, 10, 14, 3, 7, 11, 15};

	public static int[] pc1 = new int[]{15, 10, 5, 0, 16, 9, 7, 1, 17, 3, 19, 8, 6, 4, 18, 12, 14, 11, 13, 2};

	public static int[] pc1_inv = new int[]{3, 7, 19, 9, 13, 2, 12, 6, 11, 5, 1, 17, 15, 18, 16, 0, 4, 8, 14, 10};

	public static int[] pc2 = new int[]{9, 7, 0, 8, 5, 1, 4, 2, 16, 12, 19, 10, 17, 15, 13, 14};

	public static int[] pc2_inv = new int[]{2, 5, 7, 6, 4, 1, 3, 0, 11, 9, 14, 15, 13, 8, 12,10};
	
	public static int[][] produceDiffTable(){
		int[][] result = new int[16][16]; 
		
		for(int i = 0; i < 16; i++){
			
			String xPrime = genBitStringFromInt(i, 4);
			String yPrime = sub_box[i];
			
			for(int j = 0; j< 16; j++) {
				
				String deltaX = genBitStringFromInt(j, 4);
				
				String xPPrime = xor(deltaX,xPrime);
				String yPPrime = sub(xPPrime, sub_box);
				
				String deltaY = xor(yPrime, yPPrime);
				
				result[j][Integer.parseInt(deltaY,2)]+=1;
			}
		}
		return result;
	}
	
	public static void printTable(int[][] array) {
		for(int i = 0; i < 16; i++){
			System.out.print(i+" : ");
			for(int j = 0; j < 16; j++){
				System.out.print(array[i][j]+",");
			}
			System.out.println("\n");
		}
	}
	
	private static void print(String s) {
		System.out.println(s);
	}
	
	public static void printArray(int[] a) {
		for(int i=0;i<a.length;i++) {
			System.out.print(a[i]);
		}
		System.out.println("");
	}
	//Retroune 16 bits aléatoires en String
	public static String getRandomPlaintext(){
		String text = Integer.toBinaryString((int) Math.floor(Math.random()* 65536));

		while(text.length() != 16){
			text = "0" + text;
		}

		return text;
	}

	 public static String permute(String input, int[] perm)
      { 		
          String output = ""; 		
          for(int i = 0; i < perm.length; i++){
        	  output += input.charAt(perm[i]);	
   
          }
          return output;
     } 		
         
	//Prend une entrée de 4 bits et retourne la valeur
	//associée dans l'argument sub_box
	public static String sub(String input, String[] sub_box){
		int value = 0;

		for(int i = 0; i < input.length(); i++){
			value <<= 1;

			if(input.charAt(i) == '1'){
				value += 1;
			}
		}

		return sub_box[value];
	}

	//Retourne l'input ayant fait "amount" rotation(s) vers la gauche
	public static String left_shift(String input, int amount){
		return input.substring(amount) + input.substring(0, amount);
	}

	//Retourne l'input ayant fait "amount" rotation(s) vers la droite
	public static String right_shift(String input, int amount){
		return input.substring(input.length() - amount) + input.substring(0, input.length() - amount);
	}

	//Retourne [k_1, k_2, k_3, k_4, k_5] calculées à partir  
	//de la clef maître "master" selon la génération de  
	//sous-clefs de la troisième démonstration
	public static String[] gen_keys(String master, int n){
		String[] result = new String[n];

		String pc1_res = permute(master, pc1);

		String left = pc1_res.substring(0,10);
		String right = pc1_res.substring(10);

		for(int i = 0; i < n; i++){
			int shift = i % 2 + 1;
			left = left_shift(left, shift);
			right = left_shift(right, shift);

			String temp = left + right;
			String key = permute(temp, pc2);
			
			result[i] = key;
		}

		
		return result;
	}
	
	public static String getPartialMasterkey(String partialSubkey, int n){
		//Retrouver la clef maître partielle grâce au résultat 		
	    //de getPartialSubkey() en insérant des 'X' aux bits inconnues 
	
	    String pc2Debut = permute(partialSubkey, pc2_inv);
	    
	    //on rajoute des X aux index 3,6,11,18 qui sont les bits manquants rendu la;
	    pc2Debut = pc2Debut.substring(0,3)+"X"+pc2Debut.substring(3,5)+"X"+pc2Debut.substring(5,9)+"X"+pc2Debut.substring(9,15)+"X"+pc2Debut.charAt(15);
	    
	    String leftSide = pc2Debut.substring(0,10); 
	    String rightSide = pc2Debut.substring(10); 
	    
	    for(int i = 0; i < n; i++){ 
	        int shift = i % 2 + 1; 
	        leftSide = right_shift(leftSide, shift); 
	        rightSide = right_shift(rightSide, shift); 
	    } 
	    
	    String temp = leftSide + rightSide; 
	    return permute(temp, pc1_inv); 	
	}
	//Retourne un ou-exclusif entre les chaînes de caractères a et b
	public static String xor(String a, String b){
		if(a.length() != b.length()){
			return null;
		}
		String result = "";

		for(int i = 0; i < a.length(); i++){
			result += a.charAt(i) ^ b.charAt(i);
		}

		return result;
	}
	
	private static String subAllCipher(String cipher, boolean isDebug) {
		if(isDebug) {
			return sub(cipher.substring(0,4), sub_box_exemple)+
					sub(cipher.substring(4,8), sub_box_exemple)+
					sub(cipher.substring(8,12), sub_box_exemple)+
					sub(cipher.substring(12,16), sub_box_exemple);
		}else {
			return sub(cipher.substring(0,4), sub_box)+
					sub(cipher.substring(4,8), sub_box)+
					sub(cipher.substring(8,12), sub_box)+
					sub(cipher.substring(12,16), sub_box);
		}
		
	}
	
	private static String subInvAllCipher(String cipher, boolean isDebug) {
		if(isDebug) {
			return sub(cipher.substring(0,4), sub_box_inv_exemple)+
					sub(cipher.substring(4,8), sub_box_inv_exemple)+
					sub(cipher.substring(8,12), sub_box_inv_exemple)+
					sub(cipher.substring(12,16), sub_box_inv_exemple);
		}else {
			return sub(cipher.substring(0,4), sub_box_inv)+
					sub(cipher.substring(4,8), sub_box_inv)+
					sub(cipher.substring(8,12), sub_box_inv)+
					sub(cipher.substring(12,16), sub_box_inv);
		}
	}
	
	private static String subKeyMixing(String cipher, String key) {
		return xor(cipher,key);
	}
	
	public static String encrypt(String plaintext, String[] subkeys, boolean isDebug){
		String cipher = plaintext;

		for(int i = 0; i < 4; i++){
			//sub-key mixing
			String subKey = subkeys[i];
			cipher = subKeyMixing(cipher, subKey);

			//sub-boxes
			cipher = subAllCipher(cipher, isDebug);

			//permutation
			cipher = permute(cipher, perm);
		}

		//Final sub-key mixing (5th sub-key)
		cipher = subKeyMixing(cipher, subkeys[4]);
		
		return cipher;
	}

	private static String genBitStringFromInt(int i, int bitLength) {
		String realKey = Integer.toBinaryString(i);
		String addZeros = new String(new char[bitLength-realKey.length()]).replace("\0", "0");
		realKey = addZeros+realKey;
		return realKey;
	}
	

	public static String getPartialSubkey(){
		int[] counts = new int[256];

		ArrayList<String> plaintexts = new ArrayList<>();

		for(int i = 0; i < 1000; i++){
			//Créaton de paires de messages clairs qui satisfont
			//la différentielle d'entrée \Delta_P

			String text1 = getRandomPlaintext();
			String text2 = xor(text1, plain_diff);
			plaintexts.add(text1);
			plaintexts.add(text2);
		}

		//Encryption de ces messages clairs
		ArrayList<String> ciphers = server.encrypt(plaintexts,teamNumber);
		
		for(int j = 0; j < 256; j++){
			//Affectation du nombre de fois que chaque sous-clef partielle
			//j possible nous donne la différentielle intermédiaire 
			//"int_diff" à counts[j]
			
			String key = genBitStringFromInt(j,8);
			//hardcoded form - could be improved here
			key = key.substring(0,4)+"00000000"+key.substring(4,8);
			key = permute(key, perm);
			
			for(int i=0;i<ciphers.size();i+=2) {
				
				String cipher1 = subKeyMixing(ciphers.get(i), key);
				cipher1 = permute(cipher1, perm);
				cipher1 = subInvAllCipher(cipher1, false);
				
				String cipher2 = subKeyMixing(ciphers.get(i+1),key);
				cipher2 = permute(cipher2, perm);
				cipher2 = subInvAllCipher(cipher2,false);

				String diff = xor(cipher1, cipher2);
				
				if(diff.equals(int_diff)) {
					counts[j]++;
				}
			}
		}
		
		int maxIndex = 0;
		for (int i = 0; i < counts.length; i++) {
		    maxIndex = counts[i] > counts[maxIndex] ? i : maxIndex;
		}
		//Déterminer la sous-clef k_5^* avec des 'X' au bits inconnus
		String key = genBitStringFromInt(maxIndex,8);
		key = key.substring(0,4)+"XXXXXXXX"+key.substring(4,8);
		key = permute(key,perm);
		
		return key;
	}

	private static String partialMasterkeyPlusOne(String current, String partial) {
		int test = Integer.parseInt(current,2);
		
		String currentMask = "";
		for(int i=0;i<20;i++) {
			if(partial.charAt(i) == 'X') {
				currentMask += current.charAt(i);
			}
		}
		int mask = Integer.parseInt(currentMask,2);
		mask++;

		String newMask = genBitStringFromInt(mask, currentMask.length());
		int counter =0;
		
		String output="";
		for(int i=0;i<20;i++) {
			if(partial.charAt(i) == 'X') {
				output+=newMask.charAt(counter);
				counter++;
			}else {
				output+=partial.charAt(i);
			}
		}

		return output;
	}
	
	public static String bruteForce(String partialMasterkey, boolean isDebug){
		String result = partialMasterkey.replace("X", "0");
		boolean found = false;
		
		//Generating random plaintext
		String text = getRandomPlaintext();
		String res_server = server.encrypt(text,isDebug?0:teamNumber);
		
		for(int i = 0;i<4096 && !found; i++){
			//Déterminer lesquelles des 2^12 (4096) possibilités de bits
			//manquantes donnent la bonne clef maître
			
			String[] keys = gen_keys(result,5);
			String cipher = encrypt(text, keys, isDebug);
			
			if(cipher.equals(res_server)) {
				if(testKey(result, isDebug)){
					found=true;
					print("===============");
					print("Found");
					print("iteration : "+i);
					print("===============");
					break;
				}		
			}				
			result = partialMasterkeyPlusOne(result, partialMasterkey);
		}
		//Retourner la clef maître
		return result;
	}
	
	//verify if master key is the good one
	private static boolean testKey(String key, boolean isDebug) {
		String plaintext = getRandomPlaintext();
		int team_number = isDebug? 0 : teamNumber;
		String[] keys = gen_keys(key,5);
		String cipher = encrypt(plaintext, keys, isDebug);
		String res_server = server.encrypt(plaintext, team_number);
		return cipher.equals(res_server);
	}
	
	
	private static void debugEncrypt() {
		String plaintext = getRandomPlaintext();
		String[] keys = gen_keys(masterKeyExemple,5);
		String cipher = encrypt(plaintext, keys, true);
		String res_server = server.encrypt(plaintext, 0);
		System.out.println("crypted cipher  : "+cipher);
		System.out.println("server response : "+res_server);
		//encrypt is good
	}
	private static void AssertMasterkeyPlusOne() {
		String partial = "00XXX10101XX000XX101";
		String current = "00000101010000000101";
	
		System.out.println(current);
		current=partialMasterkeyPlusOne(current, partial);
		System.out.println(current);
		current=partialMasterkeyPlusOne(current, partial);
		System.out.println(current);
		current=partialMasterkeyPlusOne(current, partial);
		System.out.println(current);
	}
	
	private static String debugGetPartialSubKey() {
		int[] counts = new int[256];

		ArrayList<String> plaintexts = new ArrayList<>();

		for(int i = 0; i < 1000; i++){
			String text1 = getRandomPlaintext();
			String text2 = xor(text1, plain_diff_exemple);
			plaintexts.add(text1);
			plaintexts.add(text2);
		}

		ArrayList<String> ciphers = server.encrypt(plaintexts,0);
		
		for(int j = 0; j < 256; j++){

			String key = genBitStringFromInt(j,8);
			key = "0000"+key.substring(0,4)+"0000"+key.substring(4,8);
			key = permute(key, perm);

			for(int i=0;i<ciphers.size();i+=2) {
				
				String cipher1 = subKeyMixing(ciphers.get(i), key);
				cipher1 = permute(cipher1, perm);
				cipher1 = subInvAllCipher(cipher1, true);
				
				String cipher2 = subKeyMixing(ciphers.get(i+1),key);
				cipher2 = permute(cipher2, perm);
				cipher2 = subInvAllCipher(cipher2,true);

				String diff = xor(cipher1, cipher2);
				
				if(diff.equals(int_diff_exemple)) {
					counts[j]++;
				}
			}
		}
		
		int maxIndex = 0;
		for (int i = 0; i < counts.length; i++) {
		    maxIndex = counts[i] > counts[maxIndex] ? i : maxIndex;
		}
		print(maxIndex+"");
		String key = genBitStringFromInt(maxIndex,8);
		print(key);
		key = "XXXX"+key.substring(0,4)+"XXXX"+key.substring(4,8);
		key = permute(key,perm);
		return key;
	}
	private static void debugTrueBruteForce() {

		String result = "";
		boolean found = false;
		//Generating random plaintext
		String text = getRandomPlaintext();
		String res_server = server.encrypt(text,0);
	
		
		for(int i = 1;i<1000000; i++){
	
			result = genBitStringFromInt(i,20);
			String[] keys = gen_keys(result,5);
			String cipher = encrypt(text, keys, true);
			
			if(cipher.equals(res_server)) {
				if(testKey(result, true)){
					System.out.println("===============");
					System.out.println("Found");
					System.out.println("===============");
					break;
				}
						
			}		
		}

		//Retourner la clef maître
		System.out.println(result);
		System.out.println(masterKeyExemple);
	}
	
	

	
	private static void runDebugForDemo3() {
		String debugPartialKey = debugGetPartialSubKey();
		print(debugPartialKey);
		String partialMasterkey = getPartialMasterkey(debugPartialKey, 5);
		print(partialMasterkey);
		print(masterKeyExemple);
		bruteForce(partialMasterkey, true);
		print(masterKeyExemple);
	}
	
	
	
	public static String trueBruteForce(){
		String result = "";
		boolean found = false;
		

		String text = getRandomPlaintext();
		String res_server = server.encrypt(text,teamNumber);
		
		for(int i = 0;!found; i++){
			result = genBitStringFromInt(i,20);

			String[] keys = gen_keys(result,5);
			String cipher = encrypt(text, keys, false);
			
			if(cipher.equals(res_server)) {
				if(testKey(result, false)){
					found=true;
					System.out.println("===============");
					System.out.println("Found");
					System.out.println("===============");
					break;
				}
						
			}				

		
		}

		return result;
	}
	
	public static void main(String args[]){
		//Génération de la table des fréquences des différentielles de sortie
		//pour chaque différentielle d'entrée
				
		//printTable(produceDiffTable());
		
//		runDebugForDemo3();
	
		String partialSubkey = getPartialSubkey();
		System.out.println("Sous-clef partielle k_5^* : " + partialSubkey);
		
		String partialMasterkey = getPartialMasterkey(partialSubkey, 5);
		System.out.println("Clef maître partielle k^* : " + partialMasterkey);
		
		
		//Calcul de la clef maître par fouille exhaustive 
		String masterkey = bruteForce(partialMasterkey, false);
		System.out.println("Clef maître k :             " + masterkey);
		
		//run as much as we want to test the master key
//		print(testKey("00111010101110100000", false)+"");
		
		
		//Information utile --> clef de l'exemple de la démo 3 : 00100100001111010101
	}

}