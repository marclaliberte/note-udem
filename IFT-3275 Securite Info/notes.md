- [Notes de Securite](#notes-de-securite)
  - [Cours 1 - Intro](#cours-1---intro)
    - [substituion mono alphabetique](#substituion-mono-alphabetique)
    - [substituion++](#substituion)
    - [chiffre indechiffrable = code de vigenere](#chiffre-indechiffrable--code-de-vigenere)
    - [principe de kerckhoff](#principe-de-kerckhoff)
    - [2e guerre mondiale](#2e-guerre-mondiale)
  - [Cours 2 - Cryptographie moderne](#cours-2---cryptographie-moderne)
    - [principe de kerckhoff](#principe-de-kerckhoff-1)
  - [Cours 3 - Introduction](#cours-3---introduction)
  - [Cours 4 - Chiffrement symetrique](#cours-4---chiffrement-symetrique)
  - [Cours 5 - Chiffrement symetrique suite (Confidentialite)](#cours-5---chiffrement-symetrique-suite-confidentialite)
  - [Cours 6 - integrite a partir des cles secretes.](#cours-6---integrite-a-partir-des-cles-secretes)
  - [Cours 7 - integrite cles secretes (suite)](#cours-7---integrite-cles-secretes-suite)
  - [Cours 8 - integrite cles secretes (suite et fin) et Systèmes à clé publique](#cours-8---integrite-cles-secretes-suite-et-fin-et-syst%C3%A8mes-%C3%A0-cl%C3%A9-publique)
  - [Cours 9 Cle publique (suite)](#cours-9-cle-publique-suite)
  - [cours 10 RSA](#cours-10-rsa)
  - [Integrite a cle publique (signature)](#integrite-a-cle-publique-signature)
  - [cours 11 integrite 2 (cle publique)](#cours-11-integrite-2-cle-publique)
  - [Cours 12](#cours-12)
    - [Revision](#revision)
  - [Infrastructure](#infrastructure)
  - [Cours 14](#cours-14)
  - [cours 15 Paiements EMB](#cours-15-paiements-emb)
  - [Cours 16 suite paiements](#cours-16-suite-paiements)
  - [crypto quantique](#crypto-quantique)
  - [droits dacces](#droits-dacces)
  - [protection usage](#protection-usage)
# Notes de Securite

## Cours 1  - Intro

### substituion mono alphabetique
La faiblesse de la substitution mono alphabetique repose sur le fait que la distribution de l'alphabet n'est pas uniforme. 

Comment trouver la cle : comparer la distribution des lettres normales dans le langagues habituellement et comparer avec le message crypted. C'est un point de depart, mais cela peut aider a la brute force. 

nom de cette technique : attaque statistique (analyse de frequence)

*Notons qu'une lettre peut etre codee par elle-meme, cela peut meme etre considerer comme une augmentation de la securite*

### substituion++
principe :
1. rajouter un symbole doubleur pour les lettres doubles
2. des symboles qui ne veulent rien dire au hasard
3. les mots qui reviennent souvent ont leur symbole

### chiffre indechiffrable = code de vigenere
principe : utiliser une cle pour le decalagae d'un message (voir la diapo cest plutot clair)

pour trouver la cle : il faut commencer par trouver la taille de la cle, et comme tous les caracteres identiques sont coders par le meme decalage, ensuite cest facile de faire une analyse de frequence (statistique). on essaye des tailles differentes et on essaie de voir quand on sort de la distribution uniforme

### principe de kerckhoff
**ne pas baser la securite sur le fait que les gens de connaissant pas la technique, mais quils ne connaissant pas la cle**

### 2e guerre mondiale
avenement des machines : enigma et machine de turing. invention du transistor (1947)
le principe de enigma est une sorte de susbstituion mono-alphabetique dynamique. les rotors permuttent les fils qui vont changer le cle. cest une sequence de 3 substitutions (selon le nombres de rotors). cest comme utiliser un chiffre de vigenere avec une cle qui a une taille de
$$
26^3 \approx 17 000 
$$
En language alebrique cest une multiplication de permutation. 

le but de trouver : repose sur le fait quil faut trouver la position initiale des rotors, le nombres de rotors, les rotors choisis. donc 10 millions de millards de cles possibles. le cle changeait meme a chaque jour. 
une fois quon a la cle, la machine peut se decoder elle-meme. 

Il y avait un cahier qui indiquer quelle etait la cle du jour. il fallait evidemment avoir une moyen de distribuer ce cahier de maniere securitaire. Les ambassades avaient ce cahier. Les polonais avait voler le cahier meme avant la guerre. 

Pour diminuer les attaques statistiques, la partie de la cle a trois lettre etait choisie au hasard a chaque message. Notre destinataire na pas necessairement cette cle, donc dans le message, la cle du jour avait seulement ces 3 caracteres. 

cependant, cette technique na pas dimpact sur le vol de cle. cependant les attaques statistique sont beaucoup plus difficiles. 

ces 3 caracteres etait envoyer 2 fois de suite, une methode tres simple de corrections derreures. 


## Cours 2  - Cryptographie moderne

la puissance de calculs des machines modernes peut nous pousser a penser que cela aide ceux qui brise les systemes, mais cest tout a fait le contraire. 

si on a n operations pour coder un systeme et n^2 pour le briser, ce n'est pas securitaire. plus la vitesse de la machine augmente, pour decoder cest plus en plus long tout dependant de n. 

si cest 2^n cest encore pire

### principe de kerckhoff
**ne pas baser la securite sur le fait que les gens de connaissant pas la technique ou algorithme, mais quils ne connaissant pas la cle**

DES = data encryption standard. 

le gouvernement a diminuer la taille de la cle, les 8 dernies bits etant les bits de parity check. mais ils lont diminuer pour etre capable de la tracker correctement. 

avec 56 bits, on peut le briser en 1 journee. mais si on chiffre deux fois. cest pas 2 jours mais 2^56 etc...

## Cours 3  - Introduction

quelque chose qui est considere comme extremement cest securitaire, ce nest pas une bonne idee, surtout pcq ce n'est pas vrai ou irrealiste. 

dans linfrastructure de linternet, il ny a pas de securite, elle rajouter par dessus. 

rendre la cryptographie illegale ca va juste nuire aux gens qui sont honnetes

en france, dans les annees 60. si on utiliser une communication codee, fallait envoyer la clef au gouvernement. 

il faut discerner la cryptographie de la securite

le prof dit que google et apple refuse douvrir els comptes meme si cest le gouvernement qui le demande. les grosses compagnies veulent proteger notre vie privee malgre tout, elles ne veulent pas perdre la face publiquement. vu quelles sont relativement facile a remplacer, technologiquement parlant, elle font tout pour conserver leur image. 

notons qu'en securite on protege aussi contre les bris physique (panne de courant, tremblement de terre, etc).

si on a pas linformation dans les serveurs, elle ne pourra pas leaker. 


## Cours 4  - Chiffrement symetrique

message chiffre = cryptogramme.

casser le systeme veut dire trouver exactement ce qui est ete transmis

attaque text clair connu = on a un certain nombre de message, clair et chiffrer. cest une attaque tres forte, mais ce nest pas toujours possible. 

attaque a text clair choisi = on essaye de faire transmettre un message clair a lennemi pour nous aider a decoder. (les americains qui essayent de faire dire le noms des iles codees par les japonnais)

le chiffrement symetrique est securitaire contre toutes les types dattaques listess dans la diapo des attaques.

le masque jetable a ete prouver comme optimal, pas juste securitaire. 

pour lentropie : si on est sur du message transmis, lentropie est 0. si on est absolument incertain, cest 1. 

on va dire que cest le code est ideal si la seule attaque possible est la force brute, mais cest presque impossible a prouver. 

on peut toujours trouver des faiblesses caches dans les cles qu'on peut finir par trouver. 

des = data encryption standard.

## Cours 5  - Chiffrement symetrique suite (Confidentialite)

selon nos standards dajourdhui, enigma nest pas securitaire du tout pcq la machine ne peut pas coder un caracter par elle-meme. 

par exemple, on voit A, on sait tres bien que ce nest pas un A. 

en chiffrement symetrique, lalgo pour chiffrer et dechiffrer nest pas la meme, mais vont utilsier la meme la cle, la cle est symetrique. 

on a pas de preuve mathematique que les codes quon va voit que cest securitaire. 

**important quand on double la taille de la cle, on ne double pas la quantite de travail, cest une fonction exponentielle.** 

triple des : on code avec 2 cles, mais on chiffre avec lalgorithme original 3 fois. 

$$ 3DES{K_1K_2}= DES_{K_1}(DES_{K_2}^-1(DES_{K_1}(M))) $$

AES = advanced encryption standard. 

aes a une cle de 256 bits

en chiffrement de flux G_K(IV) = masque jetable = F. 

RC4 nest pas une bonne idee pcq ca fait longtemps quil a ete briser. 

modes de fonctionnement : terme quon utilise pour expliquer comment un outil crypto va etre utiliser/deployer

il faut noter que meme si les cles sont 256 bits, il faut etre capable de donner des datas de megs, gigs, teras. etc...

electronic codebook mode = code carrement tous les blocs de donnees un a la suite de lautre, selon la taille de la cle

le gros probleme pour la securite est que les messsage quon envoient ne sont pas des sequences au hasard. cest une base de pkoi le electronic notebook. 

un des avantages de electronic notebook est quil peut etre faire en parrallele. mais il y en a une coupe qu cest possible aussi. 

CBC (cypher bloc chaining)= utilise le premier bloc pour coder le 2e, etc. il ne peut pas etre utiliser en parralle et par exemple 1 bit derreur peut foutre le bordel. 

CTR (counter mode)= si on a le meme bloc, on va utilsier un incrementeur pour le meme bloc. 

CBC = va etre decoder de la meme maniere que le codage

CTR = cest pour eviter la reutilisation des blocs identiques. lincrementeur va faire office de bruit. 

faire attention en examen le prof peut utiliser **Counter Mode encryption**

CBC et CTR seraient les 2 meilleurs recommander.

ECB n'est pas recommander du tout.

pour lexemple de la telecommande a fenetre, il nes tpas vraiment necessaire de crypter le message pcq nimporte qui qui est sur place et voit le message coder, ca pouvoir savoir en regardant ce qui se passe que le message veut dire ouvre ou ferme la fenetre. cest deja beaucoup trop dinformations pour garder le message secret ou securitaire. 

cette situation na rien a voir avec la confidentialite. 

donc le probleme est plus de savoir est-ce que le message provient de la bonne entite (la bonne telecommande). le hacker pourrait attraper le signal et le renvoyer sans savoir ce quil veut dire (par exemple) et quand meme faire quelque chose qui nest pas souhaiter. 

**Il faut faire attention, le One Time Pad n'est pas un chiffrement par bloc du tout**

## Cours 6  - integrite a partir des cles secretes.

MAC = message authentication code (code didentification de message)

S_k = produit le tag

V_k = verifie le tag 

tout comme cest possible en theorie davoir un code qui ne se brise pas, cest possible davoir une transmission de tag parfaite qui ne se brise pas non plus. 

si on a un message de 16 bits coder sur 32 bits. on va avoir une quantite exponentielle de collisions dans le hashage. un mechant paquet de message vont hasher sur la meme fonction.

on aiemrais bien que ces collisions la soit ausis rare que possible. 

la cle secrete K est composee de 2 entiers (tres gros)

le message M va etre considere comme un entier aussi

## Cours 7  - integrite cles secretes (suite)

dans lexemple donner au debut des diapos, il serait possible de briser le systeme en envoyant un message au hasard qui na pas le bon format, pour causer un crash par exemple. 

cest pour cela quil faut etre strict lorsquon commence a definir le protocole de sentendre sur les definition de ce que va etre un message. 

pour le hashage crypto. on va vouloir se defini un scenario ideal pour avoir des definitions betons. dans ce cas-ci, on va vouloir avoir un oracle aleatoire pour le tag qui va nous generer un tag de 256 bits.  peut importe la taille du fichier a sechanger, on va avoir un hash de meme taille.

par exemple, si les 2 tags ont seulement 1 bits de differences, on va avoir 2 hash qui vont pas donner pantoute la meme chose (non correler), ca serais le cas ideal.

il serait impossible dobtenir ce genre de scenario dans la vie reelle avec du code, mais ca nous donne un guideline pour essayer dy arriver.

lhypothese calculatoire :
- il doit etre difficile de trouver une collision (en fait il ne faut pas du tout en trouver)

sha1 a ete briser en pratique en 2017, cela veut dire quils ont trouver 2 documents qui hash a la meme place, ce nest pas documents au hasard.

apparement HMAC n'est pas une bonne idee, mais le prof semble dire quil ne voit pas ce qui est pas correct ici.

le paradoxe danniversaire a 50% ou plus chances que 2 etudiants aient la meme date. 

## Cours 8 - integrite cles secretes (suite et fin) et Systèmes à clé publique

chiffre et authentifie : le mac va contenir de linformation sur le message. ce nest pas une technique securitaire.

eviter les redites : on peut rajouter un compteur, pour etre sur que le prochain message identique nait pas le meme compteur. mais il peut y avoir un probleme dordonnencement de pacquets.

on peut aujouter lheure au message pour faire la job du compteur. 

PGCD(a,m) = 1 veut dire que a et m sont copremiers.

RSA repose sur le principe dexponentiation modulaire. 

Si on connait les facteurs de N, on peut calculer D. Mais cest considerer difficile de factoriser N, donc de trouver D. 

## Cours 9 Cle publique (suite)

Il faut noter que l'asymetrie est importante et n'est pas egale des 2 parties (client et amazon par exemple).

un des gros problemes dans la securite informatique est que la pluspart des usagers ne sont pas cryptographes.

c'est un peu pourquoi RSA a ete implanted.

Tout le monde connait n et e. 

en rsa, chaque bits de plus a la racine 3e va doubler la difficulte. donc cest bcp plus que AES. 

lordinateur quantique serait le fleau de RSA, il pourrait le briser aussi vite que l'utiliser. 

## cours 10 RSA

dans la diapo sur OAEP, le r = seed pour le generateur au hasard.

pour lechange de Diffie-Hellman, le formattage est pas bon voici ce qui est ecris
$$ K=A^x \bmod p = (g^y)^x \bmod p = g^{xy} \bmod p = (g^x)^y \bmod p = O^y \bmod p = K \\ Alice: x \in Z_{p-1} \\ Bob : y \in Z_{p-1}$$

x est le secret d'Alice et le message qui passe de Alice vers Bob cest O. y est le secret de Bob et son message sera A. 

## Integrite a cle publique (signature)

cle prive est la signature (SK) et la cle publique (PK) est pour verifier la signature. 

## cours 11 integrite 2 (cle publique)

il faut faire une difference entre rsa rsa*

on ne peut pas considerer une attaque comme : ah ben ca, ca compte pas, ca derange pas... ca va toujours finir par deranger, planter un moment donner. 

rsa a des belles proprietes algebriques qui s'illustre. cela fait la richesse et le probleme de rsa. mais quand on rajoute le hash, ce hash n'est pas compatible avec les proprietes algebriques. 

ca fini sur les signatures numeriques

**fin de la matiere pour l'intra**

## Cours 12
### Revision 

Cle privee

- les  2 utilise lexposant modulaire.
- generer les 2 cles est un processus a sens unique (aussi appeler une trappe)
- d et e si on les multiplie ensemble, cela donne lidentite (ils sont inverse multiplicatifs)
- le remplissage est pour augmenter la securite
- on aimerais que le message soit choisi au hasard uniformement distribuer, mais ce nest jamais le cas. 
- le cle peut ne pas etre correleer avec rien (cest le best), mais le message ne peut pas etre comme ca, on n'envoie pas des messages au hasard.
- le remplissage nous donne limpression que le message est choisi au hasard.
- hache-et-signe : on va signer le hash. 

Dans lexemple a la fin

La BD a les cles publics de tous les users, tous le users sont la cle publiques de la BD.

E_pkd(Q) = chiffre la question pour que la BD puisse la lire
S_ska(E_pkd(Q)) = on signe la question qui est chiffrer. 

ska = secret key alice
ske = secret key eve.

la bonne solution

E_pkd(Q, S_ska(Q)) : on chiffre la question et le signation de la question en meme temps. cela pourrait etre relativement couteur pour le systeme. mais une attaque DDoS serait possible contre ce genre de situation.

## Infrastructure

les tiers de confiance ca peut etre google, microsoft, apple, etc. 

mais si leur serveur tombent en panne, on est demuni.

exercices :
- la carte peut signer a notre place
- envoyer un sms pour verifier que cest bien la personne serait une solution
- cela prendrait un systeme qui sidentifier sur la carte en fait. (biometrie par exemple). avec les cartes en ce moment, ce nest pas possible.
  


## Cours 14

Needham-Schroeder-Lowe va nous mettre dans une situation asymetrique, qui va toujours etre un probleme en crypto. Bob est conviancu qu ele protocole a echouer, mais Alice, na jamais recu le message. cest un probleme.

## cours 15 Paiements EMB

Pour les cartes de credit, cest en gros les compagnie de carte qui assument le risque de la non securite (a la base). Mais plus le systeme devient securitaire, plus cest le user qui devient responsable des problemes sil y en a. 

la repudiation cest le fait de nier quelque chose qui est un fait. dans le fond cest que s'il sont incapable de prouver que cest bien nous, la transaction ne sera pas valide. 

analyse comportementale cest qu'ils sont capable de savoir si la transactions tombe dans la norme de nos transactions habituelles. 

le prof dit que la bande magnetique cest tout comme n'avoir aucune securite, linformation est en clair dans la carte, cest donc facile de cloner. 

le prof dit qu'en ligne il ny a aucune securite en ce moment, mais les compagnies font tellement d'argent qu'elles sont pretes a accepter les risques. 


## Cours 16 suite paiements

le graphe de NIP hors ligne presenter chiffre a la carte est le genre de question de tp ou dexamen. 

## crypto quantique

les resultats de recherche en info quantique sont excessivement important pour la securite info. 

lexemple sur la diapo ou la lumiere se recombine a 100% avec les 2 mirroirs, cest faux, cest pour montrer que la mecanique quanitque est incomplete. la lumiere ne se comportera pas de la meme si on bloque en haut ou en bas. 

cest un exemple de superposition. 

le parrallelisme est exponentiel. 

linfo quantique ne nous aide pas calculer quelque chose 10 000 fois, cela nous aide a comprendre une caracteristique globale d'une situation. 

cest sur quil va avoir des questions la dessus, sur les concepts, mais pas necessairement matheamtiquement (inverser une matrice)

si on a des etats qui sont parfaitements distinguable, donc on peut avoir une superposition. 

avant de mesurer les superposition, il faut faire de linterference a travers un patron. 

lordinateur quantique peut mesurer les memes chsoes qun ordinateur normal, mais on va les mesurer avec une superposition des 2 valeures. 

un peut voir ca de maniere naive comme un parralelisme exponentiel. 

lidee de la cryptographie quantique, est quelle peut detecter sil y a de lespionnage. donc on ne sechange pas un message pcq il serait trop tard sil est espionner, on doit sechanger une cle secrete pour des messages chiffres par one time pad. 

si eve est capable dintercepter les photons polariser qui sont envoyer, elle va perturber le message et bob ne comprendra plus ce qui a ete envoyer. le prof dit que la polarisation fonctionne un peu comme les lunettes 3d au cinema, les photons sont envoyers de maniere polariser horizontale-verticale ou diagonale-diagonale. 

la cle que bob est capable de lire, ca devient leur cle qui peuvent communiquer. 

bb84 (benett-brassard en 1984) est un protocole dexpansion de cle. 

**important : si lordinateur quantique existe, on ne peut plus utiliser RSA ni Diffie-elman.**

la factorisation nest pas considerer comme NP complet, donc il ny a pas de preuves que cest vraiment difficile. meme si lordinateur quantique peut ne jamais arriver, il es toujours possible qu'un algorithme soit decouvert pour briser tout ca. Il faut que la securite standard commence a regarder pour utiliser dautre algorithmes en preparation. 

## droits dacces

pour mieux comprendre le systeme de treillis Bell-Lapadula

pas de lecture en haut : on lit pas le email du boss.

pas decriture en bas : on envoie pas linfo a un journaliste (on sort de pas de la compagnie avec une cle usb).

pour lexamen par exemple, si on veut garantir lintegrite, il va falloir connaitre le systeme de Biba

Bell-Lapadula = confidentialite, Biba = integrite

## protection usage

les mise a jour des antivirus sont chiffre en RSA et la cle publique est dans le programme meme.