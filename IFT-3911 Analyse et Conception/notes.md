# Analyse et Conceptions - notes
## cours 1 - intro

Pour heredite : il faut absolument etre capable de dire `${child} is a ${parent}` et que cest tout le temps vrai

dans lexemple de overriding, il n'est pas possible selon la cardinalite que plusieurs students aient la meme adresse, il faudrait avoir `0 ... *` pour que ce soit possible 

les attributs derivers sont habituellement des **getters**
Le prof decommande d'utiliser les attributs derives et plutot utiliser des methodes. 

pour les noms de roles, on peut aussi ajouter une petite fleche qui definie le sens de la lecture. sinon le role peut etre interpreter dans les 2 sens ou implicitement. En plus cela on peut aussi avoir des noms de role aux classes (pas juste sur la fleche). ces roles sont habituellement des variablses. par exemple `cashier at Person`

Dans les exemples des roles, les 2 graphes ne sont pas equivelents. dans le premier on peut avoir des gerants qui manage des gerants. ce nest pas possible dans le 2e

quand il y a une classe associative, il va y avoir une instance de classe entre chaque assosication. ex : employement entre company et person. 

- quelle est la difference entre les 3 associations ?
il faut faire les diagrammes dobjets pour les graphes 
1. on peut une personne qui travaille pour plusieurs company ou meme pour aucune en plus davoir les associations pour chaque role
2. a chaque fois quon veut associer une personne a une company, on doit passer par purchase. on doit donc avoir une classe entre (equivelent au point notation). les cardinalite change, mais elle revient au meme que 1. donc lassocation nest pas obligatoire ici
3. Cest une relation ternaire et la cardinalite va changer aussi. et le lot nest pas obligatoire dans la relation et peut etre plus que un. Lassociation est obligatoire

## cours 2-3 Conception du design

Pour effecture le paiement, l'objet Paiement pourrait etre dans Sale ou Register. Si on veut le mettre dans Register, on va devoir le passer en parametre a Sale dans une methode. Le but cest de le mettre le plus proche possible de lobject qui est le plus etroitement relier. Donc **sale** serait une meilleur choix car il va avoir moins de couplage pour Register. 

Si on veut avoir un **log** pour toutes les ventes, on doit avoir quelque chose qui contient toutes les Sales. Donc register est le choix. 

Dans le dessin avec le GUI, le getTotal sur le Sale est aussi une mauvaise idee qui va augmenter le couplage sur la classe UI. 

## cours 4 - Generation de code

4 types de transformations en les modeles et code source

1. generation de code
2. reingenierie (refactoring) = changer le code
3. retro ingenierie (reverse ingeneering) = passer du code au modele
4. transformation de modele

le round-trip ingeneering cest faire des tours de cette boucle

En UML, il existe la vue `4 + 1` il implique le SUS = system under study

Il cest composer de plusieurs diagrammes = etat, composantes, diagramme de classes, deploiement, diagrammes d'activites, Use cases  (il y en a 17 types). Donc 4 diagrammes(etats, composantes, calsses, cu) et + 1 etant le systeme qui represente la coherence. Chaque diagramme est un point de vue du systeme. 
Si 2 classes sont declarees avec le meme nom, mais differents attributs, heritage. le system va faire un package union. donc il va faire un union des 2 signatures pour faire une seule classe. UML pourrait permettre de lheritage multiple, mais si on decide par exemple de generer en Java, il va nous dire quon a un probleme. 

association qualifier : il faut le voir comme etant **League** a un dictionnaire dont le *key* est nickame et le *value* est le **Player**

## Cours 5 - Contraintes

Les contraintes avec seulement la cardinalite peut faire du sens programmatically, mais facile a briser semantiquement (que ca ne fait pas de sens avec la situation)

on va utilise le OCL = object contraint language

implies cest limplication et cest les meme regles que normalement. 

**lindexation commence a 1**

on peut utilsie notre propre methode dans les contraintes. 

quand on va chercher un attribut des elements d'un collection, le retour devient un bag pour eter sur si par exemple 2 attributs ont la meme valeure. 

c.employees.employer va retourner un bag avec plusieurs nom de la meme compagnie.

pour faires les contraintes, le prof suggere de se faire un diagramme dobjets de base pour essayer. 

en general, on devrait partir du parent (conteneur), ca va faire une requete plus petites.
```
context person inv:
    if self.wife-> <> 0 then self.wife.employers -> intersection(self.employers) -> isEmpty()
    and
    if ...
```
en gros (si jai une femme, il faut que lintersection entre lemployeur de ma femme et le mien soit vide)

et faut ecrire la meme chose pour le husband.

mais il y a moyen detre plus simple encore. voir diapo.

size() = 0 est la meme chose que isEmpty()

ne doit pas etre sont parent
```
context Person inv:
    self.parent -> select(p1, p2 | p1 <> p2)
```
reject est le completement select

## Cours 6 - Contraintes suites

1. Tous les clients de la banque doivent être à l’emploi.
```ocl
Context Bank inv:
    self.customer->forAll(c | c.isEmployed)
```

2. Tous les clients et les employés de la banque doivent avoir 18 ans ou plus.

```ocl
Context Bank inv:
    self.customer->forAll(c | c.age >= 18) AND self.employee(e | e.age >= 18)
```

3. Si tous les comptes d’un client ont une balance de 0, alors ses comptes doivent être fermés.

```ocl
Context Customer inv:
  not self.account -> exist(a | a.balance <> 0)
  implies self.acount->forAll(a|a.status = Status::close)

```

4. Un client est soit un employé d’une compagnie, soit le propriétaire de sa compagnie.
5. Dans une banque, tous les comptes doivent avoir un numéro unique et chaque compte est lié à exactement un client.
```ocl
Context Bank inv:
    self.account -> forAll (a1, a2 | a1.accountNumber <> a2.accountNumber)
```

pour lautre partie que chaque compte est lié à exactement un client. selon la multiplicite sur le graphe cest deja verifier. 

on aurait aussi pu ecrire

```
self.account -> isUnique(accountNumber)
```

## Cours 7 Couplage et cohesion

le degre de sophistication est relie inversement au degre de reutilisabilite

le fardeau est la taille de sa classe de reference

int na pas de fardeau. 

la fardeau dun enfant dans larbre dheritage = fardeau de ses 2 parents. 

dans lexemple C1, son fardeau est 12.

dans lautre exemple, longueure, son fardeau est 2, alors que boolean et double =0
point = 3, rectangle = 4

cest lunion d'ensemble, on ne compate la meme classe 2 fois.

un enum cest comme un primitif, classe de fondation.

le fardeau nest pas regarder en valeur absolue mais en relation avec le autres classes de lapplication, par rapport a la proximite ou non de la fondation.

le CBO, meme si au-dessus de 9 peut sembler mauvaise, cela peut dependre du projet, cest une metrique relative.

### cohesion

pour lexemple du trip, goByCar, etc... on peut faire une classe abstraite Trip avec la methode goBy() et des classes qui herite qui sont nommer CarTrip, TrainTrip, etc...

### Cours 8

principes de contravariance : si dans une sous-classe B on change les arguemnts dune methode quelle a heriter de A, on est plus restrictif et on perd l'avantage d'utiliser le polymorphisme. La methode devient donc une nouvelle methode carrement. Cest le principe de precondition (avant la methode quoi)

la covariance s'appliquerait plus sur les types de retours (le principe de postconditions)

le graphe un peu plus loin explique tres bien 

## cours 9 (principes)

dans lexemple dutilisation pour le principe de demeter. le 2e exemple est moins coupler entre eux, plus modulaire, plus reutilisable.

types vs classes : si on veut utiliser treeUsingPointers, il aurait ete un mauvaise idee de ne pas avoir graphusingPointers avant. 

Pour le probleme avec les oiseaux qui ne peuvent pas voler, on aurait du avoir 2 autres types qui heritent de bird, un qui peut voler et lautre non. cest les oiseaux meme qui heritent de ces classes. (ou 2 interfaces differents pour birds)

ex: ellipse

Ellipse
- Point A,B (x,y)
- axeMineur, AxeMajeur (double)
- setFoci(A,b,){
    this.A = a
    this.B = b
    }
getAire()
getA()
getB()

Cercle Extends Ellipse
SetFoci(a,b){
    this.A = a,
    this.B = b
}
+f(Ellipse : e){
    a=(1,R), b=(1,3)
    e.setFoci(a,b)
}
assert(e.getA()==a)
assert(e.getB()==b)

Il y a un des asserts qui va fail.

C'est donc une erreure que cercle herite de Ellipse, on aurait du avoir une super classe et les 2 heritent de cette classe. 

Si on a un OCP qui est violer, souvent dans le future on va finir par faire violer le LSP

## Cours 10 (principes de conception suite)

Composite Reuse Principle, le prof dit que cest un des plus importants. 

dependance des pacquets : dans son exemple le GUI depend de trop de chose. 

calcul de instabilite

pour la partie a gauche
$$ A = 1/4  \\ E = 3/4 $$
A depend de E, donc A depend dun pacquet plus instable que lui. pas une bonne idee

pour la partie a droit
$$ A=  0 \\ E = 4/4 =1$$

E depdend de A et A est 0% instable, donc une mielleure idee. 

Zone of pain = aucune classe abstraites et bcp de dependance.

Zone of uselessness = toutes des classes abstraites et pas beaucoup de dependance.

Idealement, on de devrait sur la droite de la Main Sequence.

que reflete ce design? on doit regarder ce qui est aux extremes. en general de le design est bon, mais on doit probablement donner une attention particuliere au points des extremites.
Par exemple en bas  gauche, serait trop stable pour l'abstraction. 

Celles en haut a droite ne devrait pas etre dans le code, elles ne sont (devraient pas) etre utilisee. 

Si un paquet est en bas a gauche, cela veut pas dire que cest mauvais ou faux, il faut avoir une bonne justification par contre.

cohesiob de paquet est moins important (ne sera pas lexamen)

dans un meme paquet, la classe abstraite va generelament rester dans le meme paquet que son implementation. linterface peut etre dans un autre paquet.

pkoi certains langages permettent plusieurs interfaces et mais pas lheritage multiple : un interface est une service qui est rendu, une classe peut rendre plusieurs services. si on herite de 2 classes, cela veut souvent dire quil y a un probleme dans la classification.

**Fin de la matiere pour lintra**


## Cours 11

|   M   |   T   |  p1   |  p2   |  p3   |  p4   |  p5   |  p6   |  p7   |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  M0   |   -   |   0   |   2   |   2   |   1   |   1   |   0   |   0   |
|  M1   |  t2   |   0   |   0   |   2   |   1   |   2   |   0   |   0   |
|  M2   |  t4   |   0   |   0   |   1   |   1   |   0   |   0   |   1   |
|  M3   |  t3   |   0   |   0   |   1   |   0   |   0   |   1   |   1   |

## Cours 13 Design patterns

### Adaptateur
on rajoute le interface target et ladapter qui adapte la nouvelle librairie a ce quon a besoin pour le client par exemple. 
dans les 2 methodes, celle du haut est plus avantageuse, mais la 2e pourrait etre plus efficace cote memoire. aussi si le adaptee a des enfants, cest plus facile pour la premiere methdoe dacceder aux entants par ladapter, mais ce nest pas possible dans le 2e facon. 

### Composite

Pour lexemple avec la salle et on peut zoomer dessus 

1. on aurait la classe abstraite pout tous les elements qui a la methode zoom() deplacer()
2. On aurait lit, chambre, livre, burean, univers qui heritent de la classe principale, et sont cmposites a la classes principales
3. elles ont toutes les opretions getChildren(), add(), remove()
4. pour linstant on a beaucoup de duplication de code, on va donc avoir une calsse Element composite et chaque feuille va heriter du composite.  et cette classe est donc un sous-element (composite) a la classe abstraite. 
5. avec ce design, on pourrait par exemple avoir une chambre qui contient une chambre. 

le pkoi on demarre avec une classe abstraite, cest pcq habituellement un interface est cest pour ajotuer des fonctionnalites.


### Mediateur

cest souvent utilsier pour les Chats systems. il faut faire attention avec lexemple de la reduction du couplage. meme si les collegues ne sont pas couplees entre eux, le mediateur peut devenir un anti-patron, et le mediateur devient trop vaste, moins de cohesion et on viole le Single Responsibility. 

### Commande
Le invoker est le bouton dans la couche GUI et le receiver est lapplication dans la couche metier. notons quil ny a pas de lien entre les 2. 

le but de ce patron est de pouvoir ajouter un etat a une methode (se rappeler des anciens, etc). Chaque commande est donc une classe. 

dans lexemple : le receveur est le document et linvocateur est le menuItem et lapplication est le client. la comamnde joue un peu le role du mediateur.

