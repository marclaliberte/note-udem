- [Info Theo Notes](#info-theo-notes)
  - [plan de cours](#plan-de-cours)
  - [Cours 1 - Intro](#cours-1---intro)
  - [Cours 2](#cours-2)
  - [Cours 3](#cours-3)
    - [Theoreme](#theoreme)
    - [Lemme](#lemme)
  - [Cours 4](#cours-4)
    - [Lemme 2](#lemme-2)
    - [Lemme 3](#lemme-3)
    - [Theoreme](#theoreme-1)
    - [Lemme 4](#lemme-4)
    - [Theoreme](#theoreme-2)
    - [Theoreme pompage](#theoreme-pompage)
  - [Cours 5 - Des generalites + Cantor](#cours-5---des-generalites--cantor)
    - [Theoreme Cantor](#theoreme-cantor)
  - [Cours 6](#cours-6)
    - [Definition](#definition)
    - [Definition](#definition-1)
    - [Remarque important](#remarque-important)
    - [Definition](#definition-2)
    - [definition](#definition)
    - [theoreme](#theoreme)
    - [theoreme](#theoreme-1)
  - [Cours 7](#cours-7)
    - [Theoreme](#theoreme-3)
    - [Theoreme](#theoreme-4)
    - [Lemme](#lemme-1)
    - [Definition - Expression reguliere](#definition---expression-reguliere)
    - [Definition](#definition-3)
  - [Cours 8 - RegEx suite](#cours-8---regex-suite)
    - [Theoreme](#theoreme-5)
  - [Cours 9](#cours-9)
    - [Theoreme Myhill - Nerode](#theoreme-myhill---nerode)
  - [Cours 10 Poussons notre crayon encore plus](#cours-10-poussons-notre-crayon-encore-plus)
    - [Theoreme Myhill - Nerode](#theoreme-myhill---nerode-1)
    - [Corollaire](#corollaire)
    - [Theoreme](#theoreme-6)
  - [Cours 11](#cours-11)
    - [Grammaire](#grammaire)
    - [Definition grammaire hors-contexte](#definition-grammaire-hors-contexte)
    - [Definition :](#definition)
  - [cours 12](#cours-12)
    - [Lemme](#lemme-2)
    - [pompiste avec ca](#pompiste-avec-ca)
    - [preparation de la preuve](#preparation-de-la-preuve)
    - [definition](#definition-1)
  - [Cours 13](#cours-13)
    - [Theoreme](#theoreme-7)
  - [Cours 14](#cours-14)
  - [Cours 15](#cours-15)
  - [Cours 16](#cours-16)
  - [Cours 17](#cours-17)
    - [Machines de Turing](#machines-de-turing)
  - [Cours 18](#cours-18)
  - [Cours 18 La fin approche!](#cours-18-la-fin-approche)
    - [THM](#thm)
    - [THM](#thm-1)
  - [Cours 19](#cours-19)
  - [Cours 20](#cours-20)
    - [Remarque sur le devoir](#remarque-sur-le-devoir)
    - [Theoreme (Rice)](#theoreme-rice)

# Info Theo Notes



## plan de cours
1. 2 intras
2. 1 dev par semaine en equipe 1-2
3. bureau = 3351

## Cours 1 - Intro
Pkoi on est ici? A quoi sert ce cours? Une bonne reponse est que si on performe dans ce cours, on va etre capable de bien programmer...? On est la pour comprendre ce que ca veut dire calculer. Un ordinateur veut dire computer, qui vient du mot compute (calculer). 

Donc qu'est-ce que ca veut dire calculer? Si on qualifie calculer par utiliser un ordinateur, donc le calcul est ou dans un traitement de texte. Un algorithme est une suite d'etape finie ou chaque etape est calculee ou implementee. 

On peut penser a un automate, donc calculer est un peu comme prendre un ordinateur dans un etat et l'amener dans un autre. 
Calculer veut dire executer un certain nombre d'etapes. **Le resutat d'un calcul n'est pas forcement un chiffre.**
Ce qui est derriere la pluspart des algos ce sont donc des etapes qui s'executent et qui donne un output. 

On va regarder les fondations de ca, de maniere premierement trivial et augmenter la difficulte de maniere iterative. On va esssater de repondre a cette question tout au long de la session. 

Il faut faire attention a donnee a lordinateur un probleme et prendre la reponse de maniere naive.

La premiere partie, on va donc devoir formaliser les problemes, chercher les definitions precises (theoreme). la 2e partie on va faire surtout des preuves. La notion ce qui est elementaire ou facile va changer ici. Cette notion va evoluer. Les idees derrieres vont rester compliquer, mais le principe reste facile. 

Premiere tache aujourdhui est de montrer que la machine ne peut pas tout faire. Les mathematiciens ont un avantages par rapport aux autres domaine. une fois quils ont etablis ou prouver quelque chose, cela reste vrai ensuite. Ils insistent a ce que les definitions soient precise. 

Pkoi on veut avoir des definitions precises. On veut prouver que ce qu'on dit est vrai, meme si ce n'est pas toujours possible. 

Le 2e chose, on va etablir un metalangage pour se comprendre. 

Exemple : 
< P > = 11000101010111010  un programme compiler P devient une suite de bits pour la machine
< X > = 1110101010101 les donnes en input sont aussi une de bits pour la machine
$$ 
(P,X) = 1110101010.....10001010001001111010
$$
Si on veut savoir si le programme est bon, est-ce qu'il marche. Est-ce qu'il va s'arreter avec un certain x ou il va boucler a l'infinie. On peut ecrire un programme qui va lui donner de X en input et qui va tester?

Supposons qu'on nous ait donner une fonction STOP(P,X) retourne un bool a savoir si le programme P s'arrete avec l'input X. 
Soit

```
bizarre(p):
    si STOP(p,p) alors BOUCLE():
    sinon return
```

qu'est-ce qui arrive si ```Bizarre(Bizzare())```? est-ce que ca s'arrete ou pas. Si bizarre s'arrete sur bizzare alors boucle
Le probleme ici est que la fonction STOP ne peut pas exister. On ne peut pas ecrire une fonction qui nous dit si un programme s'arrete sur une entree.
Il y une difference entre : 
$$ 
\forall x \exists y \\
\exists x \forall y 
 $$

Donc avant de pouvoir prouver cela, on va avoir besoin d'un langage plus precis. 

P = ecris en anglais un langage
X = peut etre string, int, whatever

*Rappellons nous que tout peut coder en suite de bit*

Quand cela se termine, la sortie est aussi binaire qui sera interpreter selon le codage. Donc un prend une suite binaire et la transforme dans une autre suite binaire. Donc il faut se demander si on va terminer avec la suite qu est voulue.

$$ 
\Sigma =  alphabet \\
w = word  \\
L = langage \\
 $$
Sigma est un ensemble fini et non-vide

L est un sous ensemble de mot de l'alphabet
On a un ruban avec des cases
|b|o|n|j|o|u|r|
On va avoir une tete qui se promene sur le ruban
 - alphabet = un ensemble fini non vide de symboles (lettres)
 - word = est une suite de simbole 
 - le nombre de symboles est notes |w| si w est le mot
$$ 
\Sigma 
$$
Sa longueure est le nombre de symboles. 

Notons qu'un livre peut aussi etre un mot en soit selon notre definition. 

$$ 
k \in \N \\ 
\Sigma ^k = {a_1, a_2, ... , a_k | a_i \in \Sigma, i=i...k}
 $$

est l'ensemble de mots de longueure k.

- le mot vide est note est de longueure 0
$$ \epsilon  = empty $$
$$ \Sigma^* = \Cup \Sigma K \\ 
    \Sigma^+ = \Sigma * \setminus {\epsilon}
 $$
 un langage est une partie de :
 $$ \Sigma^*   $$

Donc chaque programme est une probleme de reconnaissance de mots. 

---------------

- si 
$$ w = a_1, a_2, ... a_k \ et \ v = b_1, ... b_l $$
- alors : 
$$ 
wv = a_1 a_2 , ... , a_k b_1 b_2 , ... b_l
$$
cest la concatenation

-------------

Soit
$$ 
L \Subset \Sigma *
$$

Alors 
$$ 
k \in \N \\ 
L^k = \{u_1, u_2, ... , u_k | u_i \in L_{i=1, ... ,k}\} \\
L^0 = \{\epsilon\}
$$

L^0 = l'ensemble qui contient le mode de chaine vide. 
Exemple
$$ 
\Sigma = \{a,b,c\} \\ 
\Sigma^3 = \{aaa, bbb, cccc, aab, aac, bba, bbc, ...\}\\
3^3 = 27
$$
27 mots pour Sigma^3

Aussi : 
$$ 
\Sigma * = \{\epsilon, a, b, c, aa, ab, ac, ba, bb, bc, ca, cb, cc, ...\} \\ 
w = abcabc \\
v = aaa \\ 
v = a^3\\
wv = abcabcaaa \\ 
vw = aaaabcabc
$$

$$ 
L = \{w \in \Sigma^k | \ |w| \equiv 0(mod3)\} \\ 
L^L = \{aaabbb, aaaaabcbcaccc\}
$$
0(mod3) veut dire un multiple de 3, donc cest des mots de longueures de multiples de 3

--------

Def : 
$$ 
L^* = \Cup L^k \\
k \in \N $$

Probleme : 
$$ L^* = (L^*)^* ? $$

Prouvez que 
$$ L^2 = L $$

--------

## Cours 2

Un alphabet est un ensemble Sigma non vide

Def : un mont sur Sigma est defini recursivement : 
1. le mot vide epsilon est un mot de longueure 0
2. pour tout a in epsion, a est un mot de longueure 1. 
3. si 
$$ u_1 = a_1a_2 ... a_k  \ et \\
u_2 = b_1b_2 .... b_l $$
sont des mots de longueure k et l, respectivement. alors la concatenation de u_1 et u_2
$$ u_1u_2 = a_1a_2 ... a_kb_1b_2 ... b_l $$ 
est un mot.
On note la longueure de 
$$ u_1 = a_1a_2 ... a_k  $$
par |u|
$$ 
\Sigma = \{0,1,a\} \\ 
\{\epsilon, 0, 1, a, \epsilon0, 0\epsilon, 01, a, 1a, 011a\} \\
$$
le epsilon a une addition de 0, mais de doit etre annulee
Convention : 
$$ a, aa, aaa, ... kfois = a^k $$

$$ \Sigma^k \ pour\ k \in \N \\ \Sigma^0 = \{\epsilon\} \\ \Sigma^1 = \Sigma \\ \Sigma^* = \cup \Sigma^k \\ L \subseteq \Sigma^*$$

**Definition**
Soit Sigma un alphabat et 
$$ L \subseteq \Sigma^* $$
1. le completement de L note 
$$ \bar L $$
 est l'ensemble 
 $$ \Sigma^* \setminus L\\ \bar L = \{ w \in \Sigma^* \ w \notin L \} $$

2. la concatenation de 2 langages est
 $$ L_1 \cdot L_2 = \{u_1u_2 \ u_1 \in L_1, u_2 \in L_2\} $$

3. L_* est defini recurisvement par

a)
$$ L^0 = \{\epsilon\} $$
b)
$$ L^k = L\cdot L^{k-1}\ pour\ k>0$$

4. la fermeture de Kleene
$$ L^* = \cup\ L^K pour k \in \N $$

ex :
$$ L_1 = L \cdot L^0= L\cdot\{\epsilon\} = \{u\epsilon\ | u \in L\} = L $$
$$ \emptyset \cdot L = L \cdot \emptyset = \emptyset $$
On prent un mot dans L et un mot dans l'ensemble vide (rien), donc cela s'annule.

Notons quil faut faut attention $\emptyset \ncong \ \{\emptyset \}$ l'ensemble qui contient l'ensemble vide n'est pas vide

------------
Question principale : Etant donne $\Sigma , w \in \Sigma^*, L \subseteq \Sigma^*$
est-ce que $w \in L ?$
Cest une question quon va poser tout au long du cours

------

Le prof dessine un ruban et une tete de lecture (machine de Turing)
$$ |a_1|a_2|a_3|a_4|...|a_n $$
Le controle va lire lettre et changer d'etat et aller lire sur une autre case du ruban. apres un certain nombre de deplacement et de lecture et la machine va nous dire oui ou non le mot lu appartient ou non au langage. Les machines peut aussi ne pas repondre si c nest pas le cas. elle peut donc ne pas sarreter. 
Comment definir une telle machine ?
q = etat, a ou b = symbole et d = direction et p = etat acceptant
$$ (q, a) = (q, b, d)  ou p$$

On introduit donc les automate a etat fini (FSM). Comment les definir formellement?

Definition : Un FSM est
$$ M = (Q, \Sigma, \delta, s, F) $$
- Q est un ensemble fini d'etats
- Sigma est l'alphabet
- delta est une fonction de transition
- s est l'etat initial
- F est une ensemble d'etat aceptants
$$ s \in Q \\ F \subseteq Q \\ \delta : Q * \Sigma \rightarrow Q$$
Notons que le FSM na pas de memoire, sauf si on note le numero de letat. Cest une memoire finie (limite), un peu comme les registres.

Ex : 
$$ \Sigma =\{a,b\} \\ L = \{w \in \Sigma^*\ |\ |w| \equiv O(mod2)   \}$$
Donc on accepte les mots de longueures paires
$$ Q = \{a_0, a_1\} \\ \Sigma =\{a,b\} \\ s=q_0 \\ F=\{q_0 \} \\ \delta(q_i, x) = q_{i+1(mod 2), i=0,1 x\in \Sigma}$$

La fonction delta peut aussi definie par un tableau
la 3e facon va etre de dessiner lautomate. 
![0mod2](graphes/mod2.png)

Comment prouver que ca marche?
$$ \hat \delta(q_0, aaaa....bb) = q_0 $$

on veut prouver que 
$$ \hat \delta(q_0, w) = q_{|w|(mod2)} $$

definition :
Soit un FSM $M = (Q, \Sigma, \delta, s, F)$
soit
$$ w \in \Sigma^*, w=a_1, ... a_l \ \ ( a_i \in \Sigma, i=1...k) k \in \N $$
Un calcul (execution) de cette machine M sur le mot w est une suite r_i de 0 a k, avec k= |w| t.q
$$ \{r_i\}_{i=0}^k $$
$$ 1. r_0 = s \\ 
2.r_i = \delta(r_{i-1}, a_i), i = 1..k $$

definition:
Soit un FSM $M = (Q, \Sigma, \delta, s, F)$

soit $\hat \delta : Q*\Sigma^* \rightarrow Q$
definie par 
$$ \hat \delta(q,\epsilon) = q \\ \hat \delta(q,ua) = \delta(\hat \delta(qu),a) \\ u \in \Sigma^*, a \in \Sigma$$

deinition :
Soit un FSM $M = (Q, \Sigma, \delta, s, F)$
Le langage reconnu (decider) par M est
$$ L(M) = \{w \in \Sigma^*\ |\ \hat \delta(s,w) \in F\} $$

On voudrait prouver que $w \in L(M) ssi\ |w| \equiv 0$
Donc un seul etat ou letat initial est acceptant peu importe le symbole
Donc il faut norlement prouver quil accepte le langage et rien dautre. 
On prouve que
$$ \hat \delta(q_0, w) = q_{|w|mod2} \\ \hat \delta(q_0, \epsilon) = q_0 = q_{0mod2}\\ u \in \Sigma^*, x \in \Sigma \\ \hat \delta (q_0, ux) = \delta(\hat \delta(q_0, u),x) = \delta(q_{|u|mod2}, x) = q_{u+1 mod2} = q_{|ux|mod2}$$

--------

## Cours 3

Retour rapide 
$$  M = (Q, \Sigma, \delta, s, F)\\
\hat \delta : Q * \Sigma^* \rightarrow Q$$
$$ L(M) = \{w \in \Sigma^*|\ \hat \delta (s,w) \in F \} $$

est-ce que cest vrai?
$$ \hat \delta (q, uv) = \hat \delta(\hat \delta(q,u),v)\ \forall u,v \in \Sigma^* $$

Oui pcq cest en gros ce quon a montrer au dernier cours

comment le prouver? Il faut utiliser la definie plus haut, avec la recuresrance
$$ \hat \delta (q, ua) = \hat \delta (\hat \delta (q,u),a)\ a \in \Sigma$$
Recurrence sur |v|. si |v| = 0, v = epsilon et
$$ \hat \delta (q,u\epsilon)  = \hat \delta(\hat \delta*(q,u),\epsilon)= \hat \delta(q,u)$$
si
$$ v = xa\ x \in \Sigma^*, a \in \Sigma $$
on a
$$ \hat \delta (q, uxa) = \delta( \hat \delta (q,ux),a)\\ =  \delta(\hat \delta(\hat \delta(q,u),x),a)\\  = \hat \delta(\hat \delta(q,u),xa)\\ = \hat \delta(\hat \delta(q,u),v) $$
Donc cest une demonstration par recurrence

Definition : 
Un language accepter (decider) par AFD est regulier (ou rationnel). On note la classe de langage reguliers 
$$ L_{reg} $$
exemple de langagues :
$$ \emptyset, \Sigma^*, L_1 \cdot L_2, L_1 \cup L_2, L_1 \cap L_2, L^* $$

Observation : quelque soit lalphabet, empty et sigma* sont des langages reguliers. 
1.
$$ \emptyset \in L_{reg} $$
dessin : trivial, un seul etat sur lui meme avec un \Sigma, aucun etat acceptant
$$ M_{\emptyset} = (\{s\}, \Sigma, \{\hat \delta(s,x)=s,\ \forall x \in \Sigma\}, s, \emptyset)\\
car \ \forall w \in \Sigma^*, \hat \delta(s,w) \notin F, donc w \notin L(M)$$
![emptysetfsm](graphes/emptyset.png)

2. 
$$ \Sigma^* \in L_{reg} \\
\hat \delta(s,w) = s \in F \iff w \in L(M)$$
dessin : meme affaire, mais letat est acceptant pour \Sigma

soit 
$$L \subseteq \Sigma^* L \in L_{reg},\ alors\ \exists\ M = (Q, \Sigma, \delta, s, F),\ tq\ L(M)=L $$
en fait , il existe une infinite de AFD M tq, L(M') = L
pkoi? pcq on on peut ajouter des etats qui ne font rien. 

Def : 
2 automates : 
$$ M_i = (Q_i, \Sigma_i, \delta_i, s_i, F_i) ou i=0,1 $$
sont equivalents  si 
$$ L(M_0) = L(M_1) $$

### Theoreme
Si $L_1, L_2 \in L_{reg},\ L_1 \cup\ L_2 \in L_{reg}$

Preuve : soit
$$ pour \ i = 1,2,\ M_1 = (Q_1, \Sigma_1, \delta_1, s_1, F_1) $$
Soit
$$   M = (Q, \Sigma, \delta, s, F) \\ \delta((q_1, q_2),a) = (\delta_1(q_1,a), \delta_2(q_2,a))$$
$$ F = (F_1 * Q_2) \cup (Q_1 * F_2)
\\ s = (s_1, s_2) $$

Si lalphabet nest pas le meme, il se peut qu'une machine ne fonctionne pas, quest-ce quon fait?
Il faut que lalphabet soit l'union des 2. 

### Lemme
Soit  $M = (Q, \Sigma, \delta, s, F)\ soit \Gamma \ un \ alphabet$
alors il exsite un AFD $M' = (Q', \Sigma \cup \Gamma, \delta', s', F')$
equivalent a M

Preuve : 
Soit
$$ Q' = Q \cup \{P\},\ s'=s\ , \ F'=F\ et \\ \delta'(q, a) = \delta(q,a)\ si\ q \in Q, a\ \in \Sigma \\ p\ si\ q \in Q, a \in \Gamma \setminus \Sigma \\ p\ si q=p, a \in \Sigma \cup \Gamma$$

-----

<!-- M = (Q, \Sigma, \delta, s, F) -->

## Cours 4

concernant le devoir, i et k plus facile sil est fixer mais arbitraire, cest bien dessayer de le faire avec des vrais chiffres, mais ca va etre compliquer en bout de ligne. 
Le prof dit que le langage est regulier, et que cest pas supposer etre compliquer, en abstrait ca devrait etre facile.

Reprenons le lemme du dernier cours!

### Lemme 2

Soit $M = (Q, \Sigma, \delta, s, F)$ un AFD et Gamma un alphabet. Il exist un AFD $M' = (Q', \Sigma \cup \Gamma, \delta', s', F')$
equivalent a M (decide le meme langage)

Preuve : 
On doit defini 
$$ Q' = Q \cup \{P\} \\ s'=s \\ F' = F \\ \hat \delta (q,x) = \delta (q,x),\ x \in \Sigma, \ q \in Q, p\ sinon$$
ou p est etat poubelles

Si on voit une lettre de Gamma qui n'est pas dans Sigma, on lenvoie dans letat poubelle. et une fois quon est dans la poubelle, on y reste. 
$$ poubelle = \Sigma \cup \Gamma $$

M n'est pas un sous-ensemble de M'. 
Dans le fond M' cest la meme affaire que M, mais avec une poubelle qui rejette 
$$ \Gamma \setminus \Sigma $$
Ils vont donc accepter le meme langage

Prouvons que L(M) = L(M') 

qu'est-ce que l'on veut prouver vraiment?
$$ \hat \delta(s,w) \in F \ ssi\ \hat \delta'(s',w) \in F' $$
rappellons nous que s=s' et F=F'.

comment on peut prouver ca?

On a un mot *w*, qui se balade dans *M*, quand on a fini de lire *w*, il va se rammasser dans le meme etat acceptant que *M'*. des quon voit une lettre qui sort de *sigma*, on reste dans *p* de *M'*. En gros cest ca qu'on veut prouver. Asteur faut le faire formellement.

soit $w =a_1 a_2 ... a_n$ soit $k = max\{i\ |\ a_1, a_2, ... a_i \in \Sigma\}$
donc le i peut etre 0

$$ si\ k=n \\ alors\ \hat \delta'(s,w) = \hat \delta(s,w) $$
car si on on n'utilise que
$$ \delta(q,x)\ pour\ q\in Q,\ x \in \Sigma\ (recurrence) $$
$$ sinon\ k < n\ donc\ \\ \hat \delta'(s, a_1...a_k) \in Q\ et\ \delta'(\hat \delta(s, a_1...a_k),x)=p \ car\ x \notin \Sigma \\
donc\ \hat \delta(s,w)=p \notin F$$
cest un exercice sur la manipulation de delta chapeau.

Donc on a prouver qu'on peut avoir un automate, aggrandir l'alphabet et avoir un automate equivalent.

faisons un autre lemme pcq on a du fun
Est-ce quon peut aussi rajouter des etats et avoir un AFD equivalent? oui!

$$  M' = (Q\cup P, \Sigma, \delta', s', F')$$
$$ \hat \delta(q,a) = \delta(q,a),\ q \in Q,\ a \in \Sigma \\ q \ pour\ q\in P\setminus Q$$

### Lemme 3
soit
$$ M_i = (Q_i, \Sigma, \delta_i, s_i, F_i)\ i=0,1 $$
alors il existe des AFD
$$   M_i' = (Q_i', \Sigma, \delta_i', s_i', F_i')\ i=0,1\ tq \\ M_i \equiv M_i' \ et\ Q_0'\cap Q_1' = \emptyset$$
Preuve

Il suffit de remplacer
$$ Q_i\ par\ Q_ix\{i\} $$
et definir
$$ \hat \delta_i'((q,i),a) = (\delta_i(q,a),i) $$
la preuve exacte dans les notes cest le Lemme 5

Le prof dit que cest pas trop trop important et quon va trop s'attarder la dessus, il veut passer a quelque chose de plus important.

### Theoreme
Si
$$  M_i = (Q_i, \Sigma_i, \delta_i, s_i, F_i)\ i=1,2 $$
sont des AFD, alors il existe un AFD
$$  M = (Q, \Sigma, \delta, s, F)$$
tq
$$ L(M) = L(M_1)\cup L(M_2) $$
Preuve:

on definit
$$ Q = Q_1 * Q_2 \\ s=(s_1, s_2) \\ F=(F_1*Q_2)\cup(Q_1 * F_2)\\ \delta((q_1,q_2),a) = (\delta_1(q_1,a),\delta(q_2,a)') $$
et on support par Lemme 2 que 
$$ \Sigma_1 = \Sigma_2 $$

On doit prouver que
$$ \hat \delta((s_1,s_2),w) \in F\ ssi\ \hat \delta_1(s_1, w) \in F_1\ ou \ \hat \delta_2(s_2,w) \in F_2 $$
ceci decoule du fait (exercice, examen?)
$$ \hat \delta((s_1,s_2),w) = (\hat \delta_1(s_1,w), \hat \delta_2(s_2,w)) $$
la preuve est dans les notes

on peut remplacer lunion par lintersection

### Lemme 4
Soit
$$ L_1,\ L_2 \in L_\{reg\} \\ alors\ L_1\cap L_2 \in L_\{reg\} $$
la preuve est exactement pareil que celle plus haut sauf
$$ F'= F_1 * F_2 $$
Dans l'union cest une des 2 qui doit accepter, mais lintersection les 2 doivent accepter

operations :
$$ L_1\cap L_2, L_1\cup L_2 , \bar L, L_1\cdot L_2, L^*$$

Regardont le completement :
### Theoreme
Si
$$ L \in L_{reg} \ alors \bar L \in L_{reg} $$
preuve

soit
$$ M = (Q, \Sigma, \delta, s, F) $$ 
un AFD, tq L(M) = L

soit
$$ \bar M = (Q, \Sigma, \delta, s, Q\setminus F) $$ 
alors

$$ \hat \delta (s,w ) \in F \ ssi\ \bar {\hat \delta}(s,w) \notin F $$
$$ ie \ w \in \bar L \ ssi\ w \notin L \\ Donc\ L(\bar M)=\bar L$$

Soit Sigma un alphabet, est-ce que tout
$$ L \subset \Sigma^* \in L_{reg}?$$
non!

$$ M = (Q, \Sigma, \delta, s, F) \\ w \in \Sigma^*$$
execution de M sur w est une suite des etats
$$ \{r_1\}_{i=0}^{|w|} $$
$$ \delta(r_i, a_i) = r_{i-1} \\ w = a_1 ...a _n $$
supposons que |w| > |Q|, le mot est plus grand que le nombre detat. 

il y forcemenet un etat q qui va se repeter. dont on va revenir en arriere. donc on peut toujours trouver un mot qui va finir par boucler sur les meme q, et le mot dans va rester dans le langage. cest le Lemme du pompiste.
Donc on va se servir de ce lemme pour aider a prouver que langage nest pas regulier

### Theoreme pompage
soit

Sigma un alphabet
$$ L \subset \Sigma^* \\ L \in L_{reg} $$
alors il existe une constante de pompage
$$ p \in \N $$
tq si
$$ w \in L\ |w| \geq p $$
il existe
$$ x,y,z \in \Sigma^* $$
verifiant

1. $w = xyz$
2. $|y| > 0$
3. $|xy| \leq p$
4. $xy^iz \in L\ pour\ i \in \N$

preuve:

Puisque L est regulier, il existe un AFD
$$ M = (Q, \Sigma, \delta, s, F) $$
tq L(M) = L.
Soit
$$ p > |Q| \ p \in \N $$
(on choisit un entier, on veut prouver quil marche)
soit
$$ w = a_1a_2 ... a_n, n \geq p $$
un mot de L et

soit
$$ \{r_i\}_{i=0}^n $$
lexecution de M sur w, puisque  n >= p, les r_i ne peuvent pas tous etres distinct (on va looper)
En fait, il existe un plus petit l tq il existe k < l avec r_k = r_l

On a evidemment
$$ l \leq Q \leq P $$
 et
$$ r_0, r_1,..., r_{l-1} $$
sont distincts

Posons
$$ x = a_1... a_k \\ y = a_{k+1}...a_l \\ z = a_{l+1}...a_n$$
on a alors
$$ r_l = \hat \delta(r_l, y)= \hat \delta(r_l, y^i)\ i \in \N $$
$$ \hat \delta(s,x)=r_k=r_l=\hat \delta(s,xy)\\=\hat \delta (r_k, z)= r_n  \in F$$
donc
$$ \hat \delta(s,xyz) =\\ \hat \delta(\hat \delta(s,x),yz) =\\ \hat \delta(r_l, yz) = \hat \delta(\hat \delta(r_l,y),z) =\\
\hat \delta(r_l,z) \in F\ pour\ i \in \N$$
esti quil est dans son monde... voir les notes pour surment de quoi de plus clair que son ecriture de marde au tableau

exemple
$$ \Sigma= \{a,b,c\} \\ L = \{a^nb^n\ | n \in \N\} $$ 
on veut prouver que le langage n'est pas regulier.
lautomate ne peut pas se rappeler du n, genre autant de a que de b.
on va supposer que cest regulier, et on va trouver une contradiction

Preuve : supposons que L est regulier et p, la constante de pompage, on va prendre un mot qui est
$$ w = a^p b^p $$
clairement w appartient a L et |w|>= p, supposons que
$$ xyz = w\ et\ xyz \in \Sigma^* , |y|>0, |xy| \leq $$
puisque
$$ w = a^pb^p \\ xy = a^k\ pour\ k \leq p$$
mais alors
$$ z = a^{p-k}b^p $$ 
et
$$ xy^iz = a^{ki}a^{p-k}b^p = a^{k(i-1)}a^pb^p \notin L$$
pour i != 1 on a une contradiction et le langage nest pas regulier

------------

## Cours 5 - Des generalites + Cantor

si on a aimer le probleme no1 du devoir essayons celui la

$$ w \in \{0,1,2,...,9\}^* \\ L_{i,k} = \{w\ |\ w \equiv i (\bmod k)\} \\ \forall i,k \in \N \\ 0 \leq i \leq k-1$$
Une autre fois p-e...

$$ M = (Q, \Sigma, \delta, s, F) $$
On va parler de generalite aujourdhui

1. Sigma peut eter coder en binaire
{a,b,c,d} on peut remplacer a,b,c,d en binaire. Si on peut avoir un AFD qui li a,b,c,d, on peut aussi avoir un AFD qui lit les sequences de bits.

$$ |Q| = n \\ |\Sigma| = m $$
$$ q_0, q_1, , ... q_{n-1}\ ou\ q_0 = s$$
$$ \delta(q_i, a_j) = q_k <=> (i,j,k) $$
si on est dans q_i, on li le jieme symbole, on passe dans letat q_k

$$ \{n, m, \{ \delta \}, F \}  \\ \delta : (i_1, j_1, k_1), ... , (i_m, j_m, k_m) \\ q_{i_1}, q_{i_2}, ... q_{i_k}$$
pour chaque letter et chaque etat, on a une condition.

ex d'un nouvelle notation: 
$$ \{2, 3, [(0,a,1), (0,b,1),(1,a,0), (1,b,0),(0,c,1),(1,c,0)],1\} $$
on a 2 etats, 3 symboles et on lit les deltas comme ca. on est a letat 0, un lit a, on va a 1 etc. .... il y a dautre deltas dans sone exemple.
en gros, il explique comment coder en binaire la nouvelle notation plus haut. on peut garder les meme notation pour a,b,c et 0,1,2,3 pcq tout dependent dou on est dans la sequence, ils vont avoir une signification differente. 
Il veugt juste dire quon peut prendre un automate qui lit abcd et on peut le traduire en binaire...

autre exemple
$$ \Sigma = \{0,1\} $$
on peut trouver les mots qui vont avec ca, peut importe le bit. les mots sur {0,1} sont donc denombrables.

`n(w) = # dans la suite`

on peut aussi avoir une fonciton inverse

`n(w)^{-1}(i) = le ieme mot`

### Theoreme Cantor
Soit X un ensemble non vide. Alors 
$$ |X| \lvertneqq |2^x| $$
plus petit, mais pas egal. 
|2^x| est l'ensemble des partie de X
Preuve :

Evidemment:
$$ |X| \leq |2^x| \\ \forall x \in X \\ \{x\} \in 2^x$$
Supposons que :
$$ |X| = |2^x|\ (2^x = \{y \ y \subset X\}) $$
il y a donc une bijection dans les 2 ensembles
$$ b : X\ \rightarrow 2^x $$
soit 
$$ D = \{x \in X\ |\ x \notin b(x)\} $$
x sont image est dans X, mais pas inversement. 

Puisque b est une bijection :
$$ \exists d \in X $$
tq
$$ b(d)=D $$

Est-ce que
$$ d \in D\ ? $$
On voit que
$$ d \in D\ ssi\ d \notin D $$
Cest une contradiction, fin de la preuve du theoreme. 

le d est la diagonalisation de Cantor
$$ |\Sigma^*| > \Sigma^* $$
Donc il y a forcement des langages qui nont pas de machines 

Il y a beaoucp plus de langage qui non pas de machine.

En gros, cest que pour chaque infini, ya forcement un autre plus grand. Il y a un nombre infini d'infini.

------------

## Cours 6

$$ L_1 \cup L_2, L_1 \cap L_2, L_1 \cdot L_2, L^*, \bar L $$

si l1 et l2 sont regulier, lintersection et la jointure vont etre regulier aussi.

pour un mot :
$$ a_1 a_2 ... a_i a_{i+1} ... a_k a_{k+1}....a_n $$

disont que 
$$ a_1...a_i \in L_1 \\ a_1...a_k \in L_1 \notin L_2 $$

comemnt on fait pour savoir quel mot appartient a quel langage.

on a 2 automates M1 et M2, on aimerait passer de letat acceptant de M1 et aller dans M2. si jamais M1 a plusieurs etats acecptants, comment on fait pour passer de M1 a m2?

On va definir lacceptation du mot dans M1. on va il dire il existe un chemin qui satisfait la condition. 

$$ \delta(q, a) = Q_{q,a} \subseteq Q  \\ \delta(q,a) = \{q_1,q_2,q_3\}$$

on commence dans un etat initial s et apres on a plusieurs possibilite si on lit "a", a chacun de ses etats la, on a ausis plusieurs possibilite pour a_2, et ainsi de suite

il y a un etat acceptant, il existe donc un chemin de la racine vers letat acceptant. ce n'est pas un automate deterministe

tout nous dit pas comment passer dun automate a lautre, ca va nous prendre une certaine transition. notons que les ensemble detats entre les 2 sont disjoints

epislon est le mot magique

$$ \delta(q, \epsilon) = s_2 $$

### Definition

Un automate fini non-deterministe (AFND)
$$ M = (Q, \Sigma, \delta, s, F) $$
est defini par
 
- Q = un ensemble fini detats

- Sigma = un alphabet

- s = etat initial
$$ s \in Q $$
- f un ensemble detats acceptant
$$ F \subseteq Q $$
- delta :
$$ \delta : Q * (\Sigma \cup \{\epsilon\}) \rightarrow 2^Q $$

On ecrit parfois
$$ \Sigma_{\epsilon}\ pour\ \Sigma \cup \{\epsilon\}$$ 
pour que ce soit plus court

le prof dessin un automate qui accepte les mots qui commences par "aa" ou "aba"
$$ aa\Sigma^*\ ||\ abb^*a\Sigma^* \ ||\ b^*a\Sigma^*\ || aa^*bb^*a\Sigma^*  $$
$$ \delta(s,a) = q_1,\ q_2 $$

lidee cest quon quon peveut plusieurs parcours pour le meme debut de mot.

$$ L=\{aa, aba, acab\} $$

on peut faire un trajet different pour les 3 mots qui commence tous par un "a". si on prend aba, on peut trouver un chemin de letat initial a un des etats acceptants. il en existe au moins une. 

$$ L=\{w_1...w_k\} \\ L(M_i) \{w_i\} $$
on peut faire un automate par mots accepter par le langage. un etat initial qui lit un mot vide epsilon et ensuite chaque nouveau trajet devient un automate deterministe. 

comment peut-on definir le langage decider par un AFND

### Definition
soit
$$ M = (Q, \Sigma, \delta, s, F)  $$
un AFND

pour
$$ q \in Q $$
on defini E(q), lensemble des etats accessibles a partir de q par des suites de transitions spontanees

un transition spontanee cest quand un lit un epsilon et on va a plein dautres etats possibles. 

$$ E_0(q) = \{q\} $$
donc en 0 etapes de transition spontanees. et

$$ E_{k+1}(q) = E_k(q)\cup(\Cup\delta(p,\epsilon))  \\ p \in E_k(q)$$

donc la transition e_i+1 va inclure toutes les transitions avant elle. dans le fond cest un ensemble de cardinalite un de plus. (au moins un de plus)

$$ E(q) = E_{|Q|-1}(q) = \Cup_{k=0}^{|Q|-1} E_k(q)$$

ce que ca dit, cest que a partide q, on lit epsilon et ca va un a ensmble detats et ca comprend lui meme.

pour
$$ X \subset Q $$
mettons
$$ E(x) = \Cup E(q) \\ q \in X $$

E(q) cest tous les etats quon peut arriver a partir des etats de  x avec des transitions spontannees. 

### Remarque important
$$ E(E(X)) = E(X) $$

### Definition
soit
$$ M = (Q, \Sigma, \delta, s, F)  $$
un AFND

on definit
$$ \hat \delta: Q*\Sigma^* \rightarrow 2^Q $$

petit rappel(pour un ensemble X)
$$ 2^x = P(x) =\{y|y \subseteq X\} $$ 
(cest une notation ensembliste bref)

par

un etat, un mot et un ensemble detat dans lequel on peut arriver

$$ \hat \delta(q,\epsilon) = E(q) $$
bref on lit rien et on arrive quelque part
$$  \hat \delta(q, wa) = \Cup_{p \in \hat \delta(q,w)} \Cup \hat \delta(v,\epsilon)_{v \in \hat \delta(p,a)} = \Cup_{p \in \hat \delta(q,w)} \Cup E(v)_{v \in \hat \delta(p,a)} $$

### definition

le langage decider par un AFND
$$ M = (Q, \Sigma, \delta, s, F)  $$
est
$$ L(M)=\{w \in \Sigma^* |\ \hat \delta(s,w) \cup F \neq \emptyset \} $$

une suite de choix menent a letat acceptant

### theoreme

Si  M est un AFD, il existe un M', un AFND equivalent. 

comment prouver ca?

soit
$$ M = (Q, \Sigma, \delta, s, F)  $$
un AFD

definissons
$$ M' = (Q', \Sigma, \delta', s', F')  $$
$$ Q' = Q \\ \Sigma \\ s'=s \\ F' = F \\ \delta'(q,a) = \{\delta(q,a)\} \\ \delta'(q,\epsilon)=\{q\}$$

Il faudrait prouver
$$ \hat \delta '(s,w)\cap F' \neq \emptyset\ ssi\ \delta(s,w) \in F $$
on veut arriver la (voir les notes pcq on a assez souffert)
$$ \delta'(q,w)  = \{\hat \delta(q,w)\}$$

### theoreme

Si  M est un AFND, il existe un M', un AFD equivalent. 

on va definir
$$ \delta'(X,a) = Y' $$

on va supposer quon a prouver le theorem pcq on a pas le temps
$$ M_1 = (Q_1, \Sigma_1, \delta_1, s_1, F_1)   \\ M_2 = (Q_2, \Sigma_2, \delta_2, s_2, F_2)$$

deux afd pour L_1 et L_2, on veut un AFN M tq
$$ L(M) = L_1 \cdot L_2 $$
on peut supposerr sans perte de generalite que 
$$ \Sigma_1 = \Sigma_2 = \Sigma \\ Q_1 \cap Q_2 $$
$$   M = (Q, \Sigma, \delta, s, F) \\ Q = Q_1 \cup Q_2 \\ \Sigma = \Sigma_1 = \Sigma_2 \\ s = s_1 \\ F=F_2 \\

\delta(q,a)
\begin{cases}
\{\delta_1(q,x)\}\ x\in \Sigma,\ q\in Q_1 \\
\{\delta_2(q,x)\}  \ x\in \Sigma,\ q\in Q_2 \\
{s_2}\ x=\epsilon \ q \in F_1
\end{cases}
$$

-----------

## Cours 7

### Theoreme
Pour tout AFN
$$ M = (Q, \Sigma, \delta, s, F)  $$
il exist un AFD
$$ M' = (Q', \Sigma, \delta', s', F')  $$
equivalent

Preuve : on definit

$$ Q' = \{E(X)\ |\ x \in 2^X \} \\ s' = E(s) \\ F' = \{x \in Q'\ |\ F \cap X \neq \emptyset\} \\ \delta': Q' \times \Sigma \rightarrow Q' \\ 
\delta'(x,a) =\Cup_{q \in X} \hat \delta(q,a) $$

Ce que ca decrit, cest le fonctionnement de cette machine. Si on a un AFND, on veut le simuler par un AFD. 

Il faut montrer que

$$ \hat \delta(s,w) \in F\ ssi\ \\ \hat \delta'(s',w) \in F' $$

on a que
$$ \hat \delta'(E(X), \epsilon)= E(X) $$
et
$$ \hat \delta(x,ua) = \delta'(\hat \delta'(X,u),a) $$
On veut que
$$ \hat \delta(x,w) = \Cup_{q \in X} \hat \delta(q,w) $$
car alors
$$ \hat \delta'(s',w) = \hat \delta(s,w) $$
et donc
$$ \hat \delta'(s',w) \in F'\ ssi\ \\ \hat \delta(s,w) \cap F \neq \emptyset $$

Voir les notes pour plus de details (page 6 en haut)

### Theoreme

$$ L_{AFND} = L_{AFD} = L_{reg} $$

### Lemme

Si
$$ L_1,\ L_2 \in L_{reg} $$
alors
$$ L_1 \cdot L_2 \in  L_{reg}$$
si
$$ L \in L_{reg} $$
L* l'est.

On va montrer que les langages reguliers = L_{e,r}, la classe de langages decrits par des expression regulieres.

### Definition - Expression reguliere
Soit Sigma, un alphabet. Un RegEx sur sigma est definit recurisvement : 

1. \EmptySet est un RegEx
2. \epsilon est un RegEx
3. a est un RegEz pour a \in \Sigma
4. si R_1 et R_2 sont RegEx, alors (R_1 \cdot R_2) est RegEx
5. si R_1 et R_2 sont RegEx, alors R_1 + R_2 est ReGex
5. Si R est regex, (R^*) l'est aussi.


### Definition
Le langage L(R) decrit par un RegEx R sur un alhpabet sigma est definit recursivement.
1. Si $R = \emptyset$, $L(R) = \emptyset$
2. Si $R = \epsilon$, $L(R) = {\epsilon}$
3. si $R = a$, $L(R)= {a}$
4. Si $R = (R_1 \cdot R_2)$ , $L(R) = L(R1) \cdot L(R2)$
5. Si $R = (R1 + R2)$, $L(R) = L(R1) + L(R2) = L(R1) \cup L(R2)$
6. Si $R = (R1^*)$, $L1(R1) = (L(R1))$

Exemple
$$ \Sigma = \{a,b,c\} $$
Regex: 
$$ \emptyset, \epsilon, a,b,c, (\emptyset,a) = \emptyset, (\epsilon * a) = (a * \epsilon ) = a, (a*b), ((a*b) c),... $$

Notons qun regex represente un langage mais ne decide rien.

-----------

## Cours 8 - RegEx suite


$$ \Sigma \\ \emptyset \\ \epsilon \\ a \in \epsilon \\ (R_1 \cdot R_2) \\  (R_1 + R_2) \\ (R^*) $$

Si regex est 

$$ (R_1 \cdot R_2) $$
alors
$$ L(M_1) = L(R_1) \\ L(M_2) = L(R_2) \\ \Rightarrow L(R_1) \cdot L(R_2) \in L_{reg}$$

on va avoir besoin des transitions epsilons pour passer de M1 a M2. 

### Theoreme

Si 
$$ L \subseteq \Sigma^* $$
est un langage regulier, il existe un regex R sur Sigma tq L(R) = L (dont le langage est le meme)

Preuve:
supposons :
$$ q_0, .. q_n $$
on va regarder ce qui se passe de qi a qj, on va se dire quon ne va permettre aucun etat intermediaire, puis 1, puis 2 , etc... on fait du bootstrap...

On va definir un regexp
$$ R_{ij}^k $$
qui correspond au mot permettant daller de qi a qj en ne passant que par les etats q0, q1, ... qk-1

donc on veut aller de qi a qj en evitant les qk et plus,
quand on a k, on a aussi R_kk^k, donc on retourne sur k lui meme.

$$ R_{ij}^{k+1} = R_{ij}^{k} + R_{ik}^{k} \cdot (R_{kk}^{k})^* \cdot R_{kj}^{k} $$

ex : 

$$ L= \{w \in \{a,b\}\ : |w| \equiv 2 \bmod 3 \}  \\ L(R_{ij}^k) = \{w \in \{a,b\}\ : \hat \delta(q_i,k)= q_j \}  $$
mais aucun etat intermediare est numerote > k

ca nous fait une recurrence 
$$ R_{ij}^{k+1} = R_{ij}^{k} + R_{i(k+)}^{k} \cdot (R_{(k+1)}^{k})^* \cdot R_{(k+1)j}^{k} $$


$$ R_{00}^{-1} \rightarrow \epsilon \\  R_{01}^{-1} \rightarrow (a+b) \\ R_{10}^{-1} \rightarrow \emptyset \\ R_{02}^{-1} \rightarrow \emptyset \\ R_{20}^{-1}  \rightarrow (a+b)\\ R_{12}^{-1} \rightarrow (a+b) \\ R_{21}^{-1} \rightarrow \emptyset \\ R_{11}^{-1} \rightarrow \epsilon \\ R_{22}^{-1} \rightarrow \epsilon   $$


$$ R_{00}^0 \rightarrow \epsilon+\epsilon \cdot \epsilon^* \cdot \epsilon = \epsilon \\ 
R_{01}^0 \rightarrow (a+b)+\epsilon \cdot \epsilon^* \cdot (a+b) = (a+b) \\ 
R_{10}^0 \rightarrow \emptyset + \emptyset \cdot \emptyset^* \cdot \emptyset = \emptyset \\ 
R_{20}^0 \rightarrow  (a+b)+(a+b)(\epsilon)^* \cdot \epsilon = a+b \\ 
R_{02}^0 \rightarrow \emptyset + \epsilon \cdot \epsilon^* \cdot \emptyset = \emptyset \\ 
R_{12}^0 \rightarrow (a+b) + \emptyset \cdot \epsilon^* \cdot (a+b) = (a+b)
\\ R_{21}^0 \rightarrow (a+b)^2 \\ R_{11}^0 \rightarrow  = \epsilon \\ R_{22}^0 \rightarrow  = \epsilon$$

le prof est dans son monde en esti....

dans le fond on regarde le premier indice vers lexposant, ou bient lexposant vers lexposant, ou lexposant vers le 2e indice. il faut voir ca comme un triangle...

rappellons le theoreme

$$ L \subseteq \Sigma^* $$
est un langage regulier, il existe un regex R sur Sigma tq L(R) = L (dont le langage est le meme)

Preuve:

Puisque 
$$ L \in L_{reg} $$
Il existe un AFD
$$ M = (Q, \Sigma, \delta, s, F)  $$
tq L(M) = L

Soit 
$$ Q = \{q_0, ... , q_{n}\} $$
spdg avec s = q_0

On va construire des regex
$$ R_{ij}^k $$
tq

$$ L(R_{ij}^k) = \{w=a_1a_2...a_m\ |\ a_i \in \Sigma, \hat \delta(q_i, w) = q_j \} \\ \hat \delta(q_i, a_1...a_t) \notin \{q_{k+1}, ..., q_{n} \}\\ pour\ t=1,...,m-1 $$

On observe dabord que 
$$ R_{ij}^{-1}$$
a comme langage lensemble des mots tq
$$ \hat \delta(q_i, w) = q_j\ et\ \hat \delta(q_i, a_1...a_t) \notin \{q_0,..., q_n \} \\ R_{ij} = + \{a|\ \delta(q_i,a) = q_j \} $$

ie
$$  L(R_{ij}^{-1}) = + \{a|\ \delta(q_i,a) = q_j \} $$

On observe egalement que
$$ R_{ij}^{k+1} = R_{ij}^{k}  + R_{i(k+1)}^{k}(R_{(k+1)(k+1)}^{k})^*R_{(k+1)j}^{k}  $$
Lemme 11 dans les notes

$$ LHR : L(R_{ij}^k) = \{w\ |\ \hat \delta(q_i,w) = q_j\} $$
sans passerr par q_k+1 to q_n

mais alosr
$$ L(R_{ij}^{k+1}) = L(R_{ij}^k) + L(R_{i(k+1)}^k)(L(R_{(k+1)(k+1)}^k) )^*L(R_{(k+1j}^k)    $$


------

definition 16 :
Une relation binaire R sur X est une partie de X × X. On ´ecrit souvent xRy (parfois x ∼R y)
pour (x, y) ∈ R.
$$ R \subseteq X \times X $$
cest le produit cartesien de deux ensemble

Une relation binaire R sur X est une relation d’´equivalence si elle est

- reflexive : xRx pour tout x ∈ X;
- symetrique : si xRy alors yRx;
- transitive : si xRy et yRz alors xRz.

une classe dequivalence $[x]_R$ cest quon ramasse tout ce qui est equivalent ensemble

Une partition de X est une famille Π = {Xi}i∈I de parties de X, I un ensemble d’indices, telle
que ∪i∈IXi = X et Xi ∩ Xj = ∅ pour i, j ∈ I, i 6= j.

les classes dequivalences sont disjointes.

Definition 17 Soit X un ensemble et soient R, E deux relations d’´equivalence sur X. On dit que E
raffine R si pour tout x ∈ X, [x]E ⊆ [x]R

## Cours 9

Soit $L \in L_{reg}$ sur $\Sigma$

soit $M = (Q, \Sigma, \delta, s, F)$ tq L(M) = L
$$ \hat \delta (s,u) = \hat \delta(s,w) = \hat \delta(s,u) = q \\ \forall z \in \Sigma^*, \hat \delta(s,wz) = \hat \delta(s,vz) = \hat \delta(s,uz) $$

ici u, v et w amene tous a q.

Alors on definir une relation dequivalence sur $\Sigma^*$ par
$$ uR_Mv\ si\ \hat \delta(s,u) = \hat \delta(s,v)$$

Cette relation dequivalence est invariante a droite. c-a-d si $uR_mv$ alors $\forall z \in \Sigma^*,\ uzR_Mvz$ 

car
$$ \hat \delta(s,uz)= \hat \delta(\hat \delta(s,u),z) = \hat \delta(\hat \delta(s,v),z) = \hat \delta(s,vz) $$

Soit
$$ L \in \Sigma^* $$
Definissons une relation dequivalence $R_L$ sur $\Sigma^*$, par $uR_Lv$ si
$$ par\ uR_Lv\ si\\ \forall z \in \Sigma^*,\ uz \in L\ ssi\ vz \in L$$

On observe que R_L est egalement invariante a droite car
$$ uR_Lv\ alors\ uz \in L\ ssi\ uz \in L,\ \forall z \in \Sigma^* $$
et donc
$$ uxz \in L\ ssi\ uxz \in L\ \forall x,z \in \Sigma^*\ ie.\ (ux)z \in L\ ssi\ (ux)z \in L\\ donc\ uxR_Lvx $$

Exemples
soit
$$ \Sigma = \{a,b,c,d\} $$
1. $$ L=\{w \in \Sigma^*:\ w=au,\ u \in \Sigma^* \} \\ R_L = auR_Lav\ \forall u,v \in \Sigma^* \\ xuR_Lxv\ \forall u,v \in \Sigma^*,\ x \neq a$$
Cest donc une relation des mots qui commence par a et des mots qui ne commence pas par a. 

2. $L= \{w\ :\ |w| \equiv 2 \bmod 3\}$
On pretend que uR_Lv ssi $|u| \equiv |v| \bmod 3$
$$ \epsilon R_L aaabbb $$
En effet, si $|u| \equiv |v| \bmod 3$ alors $|ux| \equiv |vx| \bmod 3\ et\ w \in L\ ssi\ vx \in L$
Ici ca decoupe lespace de sigma* en 3 (0 mod3, 1 mod3, 2 mod)

3. $L = \{w \in \Sigma^*:\ |w|_a \equiv |w|_b \}$

ICi ca va diviser lespace k fois en de 0 a k nombre de a. il a y un nombre infini de classe dequivalence, alors que ce netait pas le cas dans les 2 autres exemples.

### Theoreme Myhill - Nerode

Soit Sigma un alphat, L un langage dans sigma^*, alors les enonces suivant sont equivalents. 

1. Le langage est regulier.
2. L est la reunion  de certaines classe dequivalence dune relation dequivalence invariante a droite avec un nombre fini de classe dequivalences. 
3. la relation dequivalence $R_L$ a un nombre fini de classes dequivalences

Preuve :

1 => 2

Notons que nous avons au plus autant de relations dequivalences que de nombres detats (attention aux etats inatteignables)

Puisque le langage est regulier, il un AFD M = blbabla, tq L(M) = L . la Relation R_L est invariante a droite et a un nombre de classes dequivalences fini. En effet, le nombre de classe dequivalence est au plus |Q|. Pour le voir, on note uRv ssi

$$ \hat \delta(s,u) = \hat \delta(s,v) $$
et on peut donc appeler 
$$ [u] = [v] = \bar q = \hat \delta(s,u) $$
Clairement
$$ L = \Cup_{q \in F} \bar q  = \Cup_{q \in F} [u] = \Cup_{\hat \delta(s,u) \in F} [u]$$

## Cours 10 Poussons notre crayon encore plus

### Theoreme Myhill - Nerode
 on la definie un peu plus haut

(1) =⇒ (2)
On a prouv´e ceci plus haut: soit M un AFD tel que L(M) = L. Alors la relation RM est
invariante `a droite, d’index fini, et on a que L = ∪x∈L[x] = ∪q∈F Cq.

sur Sigma^* definie par

$$ uR_Mv \ si\ \hat \delta(s,u) = \hat \delta(s,v) $$
quelque soit le mot x quon rajoute, on va toujours passer par q et aller au meme etat.

elle est invariante a droite

$$ \forall x \in \Sigma^*,\ \hat \delta(s,ux) = \hat \delta(\hat \delta*(s,u),x) = \hat \delta(\hat \delta*(s,v),x) = \hat \delta (s,vx)$$

$$ Si\ uR_Mv\ et\ donc\ uxR_Lvx $$

Evidemment :
$$ L = \Cup_{\hat \delta(s,u) \in F} [u] $$
[u] = classe dequivalence du mot u



• (2) =⇒ (3)
Nous prouvons que toute relation E verifiant (2) raffine RL. Puisque le nombre de classes
d’equivalence (l’index) de E est fini, le nombre de clases d’equivalence de RL l’est aussi (E a au
moins autant de clases d’equivalence que RL). Soit donc E un relation d’equivalence verifiant
(2) et soit x, y ∈ Σ^*

tels que xEy. Pour tout z ∈ Σ^*
xzEyz (car E est invariante adroite),
c’est-a-dire,xz et yz appartiennent a la meme classe. Donc xz ∈ L si et seulement si yz ∈ L,
c’est-a-dire, (x, y) ∈ RL.

Rappel; 

1. deux mots sont equivalents si peu importe le mot quon ajoute a droite, on arrive ou non dans L.

$$ uR_Lv\ si \forall z \in \Sigma^*,\ uz \in L <=> vz \in L $$

2. E raffine R_L veut dire  que 

$$ uEv => uR_Lv $$


Soit donc u et v dans sigma*, tq uEV

on sait que : 

$$ \forall x \in \Sigma^*,\ uxEvx $$

mais cela nous dit que
$$ ux \in L\ ssi\ vx \in L \\ ie = uR_Lv $$

Puisque le nombre de classe dequivalence de R_L est au plus (car E raffine R_L) celui de E, et puisque ce dernier est **fini,**
RL a un nombre **fini** de classe dequivelence. 

• (3) =⇒ (1)
Nous allons definir un automate fini deterministe ML tel que L(ML) = L. Pour x ∈ Σ^*
mettons
[x] = [x]RL
Soit ML = {Q, Σ, δ, s, F} defini par
Q = {[x] : x ∈ Σ^*}
.
s = [ε]
F = {[x] : x ∈ L}
δ([x], a) = [xa] \forall x \in Sigma^*, \forall a \in Sigma

soit L compris dans Sigma^*, tq le nombre de classe dequivalence de RL, on va construire un AFD qui decide ce langage.

La definition de deltaL est coherente, si
$$ v \in [u] $$
alors
$$ uR_Lv $$
est donc
$$ vazR_Luaz\ \forall a \in \Sigma, \forall z \in \Sigma^* $$
RL est invariante a droite.

Donc [ua] = [va]

Regardons
$$ \hat \delta_L([x], y)\ x,y \in \Sigma^* $$

On a
$$ \hat \delta_L ([x], y) = [xy] $$

exercice recurrence... (avec la longueur de Y, si cest epsilon, on reste sur place)

Donc
$$ \hat \delta_L([\epsilon], u) = [u] $$
et
$$ u \in L\ ssi\ \hat \delta_L ([\epsilon], u) \in F$$

faisons un exemple

$$ \Sigma = \{a,b\} \\ P = \{w : w=w^R\} $$

palindrome

le prof ecrit
$$ w = a_1...a_k \in \Sigma^* : a_i = a_{k-i} $$

prenons
$$ a^ib,\ i \in \N$$ 
des mots.
on a  qye
$$ abR_Pa^ib\ si\ i \neq j$$
le relation nest pas valide
car
$$ a^iba^i \in P \\ a^jba^i \notin P $$

donc par le theoreme R_P na pas un nombre fini de classe dequivalence, donc n;est pas regulier. 

ie [ab] != [a^jb] si i!=j

autre aexemple
$$  L = \{w \in \Sigma^* :\ |w| \equiv \bmod 17\} $$

soit 
$$ [a^i]\ i=0...16 $$
des classes dequivalences de R_L. On pretend que ce sont toutes les classes dequivalences de R_L

on doit prouver que chaque mot, appartient a une de ces classes.

En effet
$$ a_1, a_2,..., a_k\ \in \Sigma^* $$

Alors
$$ |w| = k\ et\ wR_La^k, k \in [17] $$

car
$$ |a_1a_2...a_kx| \equiv 3 \bmod 17\ ssi\ k \equiv 3-[x] \bmod 17\ ssi\ |a^kx| \equiv 3 \bmod 17 $$

ici
$$ wR_La^k $$

### Corollaire

L'automate M_l a le nombre minimum d'etats parmis tous les automates M tq L(M) = L

Deux AFD
$$ M_i = (Q_i, \Sigma, \delta_i, s_i, F_i)\ i=0,1 $$

sont isomorphes sil existe une bijection
$$ Q: Q_0 \rightarrow Q_i $$
$$ \varphi (s_0) = s_1 \\ \varphi(F_0) = \{\varphi(q):\ q \in F_0 \}  = F_1 \\ \varphi(\delta_0(q,a)) = \delta_1(\varphi(q),a) $$

### Theoreme

AFD ML est unique a lisomorphe pres. 

preuve : on doit prouver que si M= blablabla est un AFD avec |Q| = |Q_L| et tq L(M) = L(M_L) = L

alors M et M_L sont isomorphes

SOit M un automate pour L, avec |Q| minimum. on a donc que
$$ |Q| \leq |Q_L| $$

puisque R_M raffine R_L
$$ |Q| \geq |Q_L| $$
donc
$$ |Q| = |Q_L| $$

Puisque M est minimum, pour tous q \in Q, il existe w_q \in \Sigma^* tql
$$ \hat \delta(s,w_q) = q $$
poru chaque etat un a mot

On va determiner une bijection varphi
$$ \varphi Q \rightarrow Q_L $$

par
$$ \varphi (q) = [w_q] $$

On a un automate que
$$ \varphi(s) = [s] $$

varphi est donc une bijection

notons que 
$$ a \in \Sigma $$
on a
$$ \delta_L (\varphi(q),a) = \delta_L([w_q],a) =[w_qa] = \varphi(\delta(s, w_qa))  $$

et que si
$$ q \in F  \\ \varphi (q) = [w_q] \in F_L$$
Donc M = M_L

## Cours 11

Examen intra N615 !!

va etre a 16h-17h.

On fait un exemple de minimisation

|       |   a   |   b   |   c   |
| :---: | :---: | :---: | :---: |
|   0   |   1   |   1   |   5   |
|   1   |   2   |   6   |   6   |
|   2   |   3   |   7   |   3   |
|   3   |   4   |   8   |   0   |
|   4   |   5   |   1   |   1   |
|   5   |   6   |   2   |   6   |
|   6   |   7   |   3   |   3   |
|   7   |   8   |   0   |   4   |
|   8   |   5   |   1   |   5   |

les etats acceptant sont 0-4-8

$$ \delta(q_i, a_i)  = q_r \\ \delta(q_j, a_i) = q_s$$
qr pourrait mener a un etat acceptant et qs non.
on essaie de trouver le mot z qui separe les etats

ne pas oublier que ce tableau devrait etre symetrique, la partie en haut nest pas necessaire◊
|   x   |  (0)  |      1      |      2      |      3      |     (4)     |      5      |      6      |      7      |     (8)     |
| :---: | :---: | :---------: | :---------: | :---------: | :---------: | :---------: | :---------: | :---------: | :---------: |
|  (8)  |       | (0,epsilon) | (0,epsilon) | (0,epsilon) |             | (0,epsilon) | (0,epsilon) | (0,epsilon) |      x      |
|   7   |       |             |             |             | (0,epsilon) |             |             |      x      | (0,epsilon) |
|   6   |       |             |             |             | (0,epsilon) |             |      x      |    (1,a)    | (0,epsilon) |
|   5   |       |             |             |             | (0,epsilon) |      x      |    (2,a)    |    (1,a)    | (0,epsilon) |
|  (4)  |       | (0,epsilon) | (0,epsilon) | (0,epsilon) |      x      | (0,epsilon) | (0,epsilon) | (0,epsilon) |             |
|   3   |       |             |             |      x      | (0,epsilon) |    (1,a)    |    (1,a)    |             | (0,epsilon) |
|   2   |       |             |      x      |    (1,a)    | (0,epsilon) |    (2,a)    |             |    (1,a)    | (0,epsilon) |
|   1   |       |      x      |    (2,a)    |    (1,a)    | (0,epsilon) |             |    (2,a)    |    (1,a)    | (0,epsilon) |
|  (0)  |   x   | (0,epsilon) | (0,epsilon) | (0,epsilon) |             | (0,epsilon) | (0,epsilon) | (0,epsilon) |             |

On voit que 0-4-8 sont le meme etat
1-5 sont le meme etat, aussi 2-6 et 3-7

|       |   a   |   b   |   c   |
| :---: | :---: | :---: | :---: |
| 0-4-8 |  q1   |  q1   |  q1   |
|  1-5  |  q2   |  q2   |  q2   |
|  2-6  |  q3   |  q3   |  q3   |
|  3-7  |  q0   |  q0   |  q0   |

les chiffres dans le tableau ne font que correspondre au plus petit dans dans la colonne a gauche

donc est un automate qui reconnait les langages de 0 mod 4
$$ \hat \delta(q_2, aaa) \neq \hat \delta(q_5, aaa)$$

cest impossible de le faire pour un non deterministe.

**Fin de la matiere pour lintra**

### Grammaire

### Definition grammaire hors-contexte 

Une ghc est un quadruplet
$$ G = (V, \Sigma, R, S) $$

ou

V est un alphabet de variables
\Sigma est un alphabet (de terminaux) 
$$ V \cap \Sigma = \emptyset $$
S in V est le symbole de depart (initial)
R est une ensemble fini de regles (production)
$$ R \subset \{ A \rightarrow \alpha | A \in V, \alpha\in(V\cup\Sigma)^*\} $$
ex:
$$ V = \{A,B,C,D\} \\ \Sigma =\{a,b\} \\$$
$$ R: S \rightarrow aAa | \\ A \rightarrow aAa | B | \epsilon \\ B \rightarrow bAb | A | \epsilon $$
$$ s \rightarrow aAa \rightarrow aaAaa \rightarrow aaBaa \rightarrow aabAbaa \rightarrow aabbaa$$
produit des palindromes de qui commence par a et de longueur paires

cetait quoi la difference enter delta et hat delta
$$ \delta: Q \times \Sigma (lettre) \\ \hat \delta Q \times \Sigma^* \ (mot) $$

On dit hors contexte par on peut remplacer les variables dans les regles ou qu'elles soient.

### Definition :
Etant donner une ghc(V, Sigma, R,S), on definit u donne v en k etapes
$$ u,v \in (Vu\Sigma)^* $$
recursivement :

1. $$ u \Rightarrow^0 u$$
2. $$ u \Rightarrow^k v $$
sil exist
$$ u_1, u_2,w \in (Vu\Sigma)^* $$
et
$$ A \in V\ tq\ u \Rightarrow^k u_1Au_2 \\ A \rightarrow w \in R\ et\ \\ v = u_1wu_2 \\ pour k>0$$

on peut arrive de u a v en faisant des remplacements par les regles
$$ s \rightarrow aAa \rightarrow aaAaa \rightarrow aaBaa \rightarrow aabAbaa \rightarrow aabbaa$$
cest 5 changements
$$ s \Rightarrow ^5 aabbaa$$
On ecrit 
$$ u \Rightarrow^* v $$
u donne v sil exist un k
$$ u \Rightarrow^k v $$
On definit le langage de la grammaire G comme
$$ L(G) = \{w \in \Sigma^*:s \Rightarrow^*w\} $$

ex:
$$ G = (\{S\}, \{a,b\}, R, S) $$
R:
$$ S \rightarrow aSa | bSb| a | b | \epsilon $$

Test :
$$  S \rightarrow aSa \rightarrow abSba \rightarrow abbSbba \rightarrow abbbaSabba \rightarrow abbababba$$
des palindromes pairs ou impair

On pretend que
$$ L(G) = L = \{w \in \Sigma^*: w = w^R\} $$

1. $$ |w|=0\ ie\ w = \epsilon \\ s \rightarrow \epsilon \in R\ donc\\ S \Rightarrow^1 \epsilon$$
donc w in L implies w \in L(G) pour |w|=0

2. $$ |w|=1,\ w \in \{a,b\}\\ S \rightarrow a, S \rightarrow b \in R \\ S \Rightarrow^1 w+ \\ w \in L(G)$$

3. $$ |w|=k>1 $$
alors il exist
$$ u \in \Sigma^*\ et\ w \in \{a,b\} \\tq\ w = uxu $$
si on suppose que (HR) tq
$$ \forall u \in L,\ |u|<k $$
$$ S \Rightarrow^* u $$
on peut utiliser cette derivation pour donner w : on remplace cette derivation par
$$ S \rightarrow xSx \Rightarrow^* xux = w $$

Supposons maintenant que w \in L(G) On va prouver  si 
$$ S \Rightarrow^k u\, u\in (Vu\Sigma)^* $$
alors il existe
$$ v \in \Sigma^*, x \in \{s,a,b,\epsilon\}\ tq\ u = v^xv^R $$
si k=0
$$ S \Rightarrow^*S\ bon\ avec\ v=\epsilon $$
si k=1
$$ S \Rightarrow^1 \{aSa,bSb, a, b, \epsilon\} $$
qui sont de la bonne forme

si k>1
$$ S \Rightarrow^1 xSx \Rightarrow^{k-1} xux\ avec x \in \{a,b\}$$
et par HR on obtient
$$ S \Rightarrow^{k-1} u \ et\ u = v^xv^R $$
donc
$$ S \Rightarrow^k xvXv^rx $$
qui est dela bonne forme, on conclut que L(E) = L

## cours 12

Exemple

$$ \Sigma = \{(,)\} \\ L= \{w \in \Sigma^* | w\ est\ un\ bon\ parenthesage\}
$$

x = (
y = )

(le prof nutilse pas x ou y, cest pcq ca fuck mon latex)

D1. w est bon si 
$$ w = a_1... a_{2k} \\ \forall i=1,...,2k, |a_1...a_i|_{x} \leq |a_1...a_i|_{y}\ et\ |w|_{x}= |w|_{y}$$
le nombre de parenthese ouvrante sur le ieme symbole doit etre egale au nombre de parenthese fermantes sur le ie symboles.

(())()(())

D2. Sigma est bon
- si u et v sont bons alors (u) et uv sont bons

$$ (((\epsilon))(\epsilon)((\epsilon))) $$
Exercice, prouvez que D1=D2
le nombre de bon parenthesage est le nombre de Catalan.

On aiemrait prouver que ce langages est hors contexte

x = (
y = )

(le prof nutilse pas x ou y, cest pcq ca fuck mon latex)

$$ G_{()} = (V, \Sigma, R, S) \\ V = \{s\} \\ \Sigma = \{x, y\}\\ R = S \rightarrow \epsilon | SS | (S)$$

CLAIM
$$ L(G) = L $$
1. 
$$ L \subseteq  L(G)\\ ie\ w \in L \Rightarrow w\ \in L(G)$$

$$ si\ w \in \epsilon, S \rightarrow \epsilon\\ si\ w =uv, S \rightarrow SS \Rightarrow^* uv\ par\ HR \\ si\ w= (u),\ S \rightarrow (S) \Rightarrow^* (u)\ par\ HR $$
2. 
$$ L(G) \subseteq L $$

Soit $w \in L(G)$ On fait une reccurence sur le longueures des derivations

Si k=1
$$ S \rightarrow \epsilon\ et\ \epsilon \in L$$

Si > K_1, supposons ok $\forall u  \in L(G)$
$$ S \Rightarrow^l u, l < k \\ Si\ S \Rightarrow^k w, alors\\
soit\ S \rightarrow SS \Rightarrow^{k-1} w\\
soit\ S \rightarrow (S) \Rightarrow^{k-1} w \\
donc\ soit\ w =uv,\ uv \in L(G)\\
soit\ w=(u)\ u \in L(G)

$$

Donc par HR, $uv \in L$ dans le premier cas et $u \in L$ dans le 2e et on a que $uv \in L$ et $(u) \in L$ par defintion. Donc $w \in L$ dans tous les cas.

### Lemme

Soit G, une ghc,soit
$$ \alpha \in (V \cup \Sigma)^*  $$
si
$$ w = u \alpha v $$
avec
$$ u_Lv \in (V \cup \Sigma)^* $$
si
$$ \alpha \Rightarrow^* \beta, \beta \in (V \cup \Sigma)^* alors\\ w \Rightarrow^* u\beta v$$

On fait une recurrence sur la longueur k de la derivation. 

Si $\alpha \Rightarrow^0 \beta$, $\beta = \alpha$ et $u\alpha v \Rightarrow^0 u\alpha v$

Si $\alpha \Rightarrow^k \beta$, $k>0$, il existe $A \in V$, une regle $A \rightarrow y$ et $x_1,x_2 \in (V \cup \Sigma)^*$

tq $\alpha \Rightarrow^{k-1} x_1Ax_2 \Rightarrow^1 x_1yx_2$ par definition avec $\beta = x_1γux_2$

ALors $w  \Rightarrow^{k-1} ux_1Ax_2v$

par HR, et donc $w \Rightarrow^{k} ux_1γx_2v$ en applicant la regle A −→ γ et
$ux_1γx_2v = u\beta v$. 

en gros on prouve quon peut avoir de quoi comme ca
$$ (s)\rightarrow (ss) \rightarrow ((s)S)\rightarrow((s)(s)) $$

### pompiste avec ca

Soit
$$ L \in \Sigma^* $$
un langage hors contexte, alors il existe
$$ p \in \N $$
constante de pompage tq
$$ \forall w \in L, |w| \geq p, \exists u,v,x,y,z \in \Sigma^* $$

1. 
$$ w  = uvxyz$$
2. 
$$ |vxy| \leq p $$
3. 
$$ |vy| >0 $$
4. 
$$ uv^ixy^iz \in L, \forall i \in \N $$

faisons un exemple
$$ L = \{a^nb^nc^n: u \in N \} $$
Soit p, la constante de pompage, soit
$$ a^pb^pc^p \in L $$
supposons
$$ \exists u,v,x,y,z $$
qui verifient 1, 2 et 3.

ie:
$$ w = uvxyz\\ |vy|>0, \\ |vxy| \leq p$$

(pas sur que cest bon ici)
on observe que vxy ne peut-etre que
$$ vxy=a^?, v= a^r, x = a^s, y= a^t, r+s+t \leq p \\ vxy \in a^pb^p, v=a^r, x=a^sb^t, y=b^n\\ v=a^ra^s\ x=b^t\\ v= a^ra^s,\ x=a^tb^n$$

en general
$$ v = a^ra^sb^t \\ x = a^{r'}b^{s'}b^{t'}\\ y = a^{r'}b^{s'}b^{t'} $$

on observe que vxy ne peut-etre que
$$ a^s, soit\ b^s,\ soit\ c^s \\ ou\ bien\ a^sb^t, b^sc^t $$

Dans tous les cas,$v^ixy^i$ aura changer le nombre d'au plus deux des trois lettres a,b,c, et donc $uv^ixy^iv \notin L$
sauf si i=1.
ce langage nest pas hors contexte

### preparation de la preuve
$$ A \Rightarrow^* w\\ (w \in (V \cup \Sigma)^*) $$
$$ A \rightarrow BC \rightarrow aBaC \rightarrow aaBaaC \rightarrow aaBaaCDEb $$

### definition
Soit
$$ G=(V, \Sigma, R, S) $$
une ghc

un arbre de derivation (ADD ou arbre danalyse) 
$$ T_G(A) $$
associe a une grammaire hors contexte G et une variable A \in V et est defini par:

1. Un seul sommet etiqueter par A, est un ADD. T_0(A). On definit son mot w(T_0(A)) = A]
2. Supposons que $T_i(A)$ est un ADD avec les feuilles $L_1, ..., L_n$ etiquetees par $e(L_k) = Y_k, k =1, ..., n$ soit $Y_j = B \in V$ et soit $Y_1, ... y_j+1, y_j+1, ... y_n \in U \cup \Sigma$
soit
$$ B \rightarrow x_1, ... x_l $$
une regle de G
l'arborescence ordonnee obtenue en ajoutant des nouvelles feuilles
$$ f_1....f_l $$
(une pour chaque lettre x)
comme enfants du sommets Lj etiquettes B est un ADD 
$$ T_{i+1}(A) $$
dont le mot est defini comme
$$ w(T_{i+1}(A)) = Y_1... Y_{j-1}F_1, ... F_lY_{j+1}...Y_n $$

------------

## Cours 13

Continuation de la preuve du pompiste GHC

### Theoreme

Soit Sigma un alphabet
$$ L \subseteq \Sigma^* $$
un langage hors contexte.

Il existe une constante de pompage
$$ p \in \N $$
tq si
$$ w \in L$$
est de longueure an moins p, alors il existe
$$ u,v,x,y,z \in \Sigma^* $$
verifiant
1. $$ w = uvxyz $$
2. $$ |vxy| \leq p $$
3. $$ |vy| > 0 $$
4. $$ \forall i \in \N, uv^ixy^iz \in L $$

Preuve:
Soit
$$ \Sigma, L $$
comme dans lenonce. 

soit
$$ G=(V, \Sigma, R, S) $$
un ghc

avec L(G) = L

le prof fait un dessin dans son monde. en gros onv eut prouver
$$ S \Rightarrow^* \alpha A \beta \Rightarrow^* \alpha vAy\beta \Rightarrow ... $$
l'arbre de derivation donne un arbre b-aire qui va nous donner
$$ b^p $$
de feuilles ou p est la profondeur.

donc un arbre b-aire avec m feuille sera de profondeur

$$ p \geq \log_bm $$

revenons;;

Soit
$$ b = max \{|\alpha|A \rightarrow \alpha \in \R\} \\ w \in L$$
tq
$$ |w| \geq b^{|v|+1} +1 = p$$

Soit 
$$ T_G(s) = T $$
un arbre de derivation dont le mot est w(t) = w, le moins profond. La profondeur de T est >= |v| + 2.

Soit C un plus long chemin de S (racine) vers une feuille, il est de longueur >= |v| + 2 et contient >= |v| +1 sommets internes etiquettes par des variables. Il y en a une qui se repete

Soit X, le sommet de C etiquette par la 2e occurence de cette variable, disons A. Soit Y, le sommet de c etiquette par la premiere occurence (toujours en partant du bas), Il existe $uvyz \in \Sigma^*$
tq
$$ w = w(T) = uw(T(X,A))z = uvw(T(Y,A))yz $$

**rappel T(X,A) = sous arbre de X, etiquette par A. **

On met $x = w(T(Y,A))$

et on a que $w = uvxyz$

La partie de C contenue dans T(X,A) contient au plus |v| + 2 sommets et donc
$$ |vxy| =| wT(X,A)| \leq B^{|v|+1} \leq p $$

Si |vy| = 0 (il ny a que des variables sur c). les sommets de c entre la feuille et X n'aurait chacun qu"un enfant. (juste des variables bref). Dans ce cas, l'arbre obtenu en remplacant T(X,A) par T(Y,A) aurait le meme mot mait serait moins profond. 

*Il existe donc au moins un chemin c tq |vy| > 0 pour les v,y correspondants*

Finalement T' obtenu en remplace T(X,A) par T(Y,A) mene a
$$ uv^0xy^0z $$
tandis que si $T = T_1$ on obtient $T_{i+1}$
recursivement de T_i en remplacant T(Y,A) par T(X,A) dans T_i

## Cours 14
manquer le cours...

## Cours 15

Forme normal de Chomksy

But : Si $G=(V, \Sigma, R, S)$

est ghc alors il exsite un ghc $L(G') = L(G \setminus \epsilon)$
$$ G'=(V', \Sigma, R', S') $$

Aucune regle n'est de la forme $A \rightarrow \epsilon\\ A \rightarrow B \\ A,B \in V$

Toute regle est de la forme 
$$ A \rightarrow BC \\ A \rightarrow a \\ A,B,C \in V' \\  B \neq S'\neq C \\ a \in \Sigma $$
toute variable est utile

Recette : 

1. Trouver $G_1$ ou $A \Rightarrow^* w , w \in \Sigma^*, A \in V_1$

2. Trouver $G_2$ tq $\forall A \in V_2$, $\exists \alpha \in (V \cup \Sigma)^*$ tq $S \Rightarrow^* \alpha A \beta$

3. Trouver $G_3$ tq aucune regle nest $A \rightarrow \epsilon$, $L(G_3) = L(G)\setminus \epsilon$

4. Trouver $G_4$ tq $A \rightarrow B$, $A,B \in V_4$ nest pas une regle et $L(G_4) = L(G)\setminus \epsilon$

5. Puisque $G_4$ a des regles uniquement de la forme $A \rightarrow \alpha$ ou $|\alpha| \geq 2$, $\alpha \in (V_4 \cup \Sigma)^*$ sauf si $\alpha \in \Sigma$, trouver $G_5$ ou chaque regle est de la forme $A \rightarrow BC$, $A,B,C \in V_5$ ou $A \rightarrow a, a \in \Sigma$ et tq $L(G_5)= L(G)\setminus \epsilon$

DE plus on a que $G_5$ tq S N'apparait pas a droite dans une regle

6. Si $\epsilon \in L(G)$, on ajoute $S\rightarrow \epsilon$ aux regles de $G_5$
$
faisons un exemple:

$$ S \rightarrow AB\ | a \\ A \rightarrow A$$

Alors 
$$ A \rightarrow a \\ S \rightarrow a $$

donc $V=\{A,S\}$ et $R_1 = \{S\rightarrow a, A \rightarrow a \}$

$G_1 = (\{S\}, \{a\}, \{S\rightarrow a\}, S)$

Autre exemple : 

$$ S \rightarrow \neg S | (S \Rightarrow S) | p | q \\ V = \{S\}, \Sigma=\{\neg, \Rightarrow, p, q, (,)\}$$

ca va faire 
$$ S \rightarrow X_{\neq}S  \\ X_{\neg} \rightarrow \neg \\ S \rightarrow X_{(}, S, X_{\Rightarrow} S,X_{)} \\ X_p \rightarrow p \\ X_q \rightarrow q$$
et un pour chaque variable

ca va nous donner
$$ S \rightarrow X_{(} Y_s  \\ Y_s  \rightarrow S Y_{\Rightarrow} \\ Y_{\Rightarrow} \rightarrow X_{\Rightarrow} Y_s \\ ...  \\ S \rightarrow p \\ S \rightarrow q$$

voir les notes pcq le prof est tout perdu encore

## Cours 16

Forme normale de Chomsky

$G=(V, \Sigma, R, S)$ tq $(A \in V)$

1. Si $A \rightarrow \alpha \in R$ alors $\alpha \rightarrow BC, B,C \in V$ ou $\alpha = a \in \Sigma$ sauf si $S \rightarrow \epsilon$ et $\epsilon \in L(G)$
2. si $A \rightarrow BC \in R$, $B \neq S \neq C$
3. Toute variable est utile, ie $\forall A \in V$, $\exists w \in L(G)$ tq $S \Rightarrow^* \alpha A\beta \Rightarrow^* w$ tq $\alpha, \beta \in (V \cup \Sigma)^*$

Avec cela on a un arbre de grammaire binaire partout, pas repeition de variables. donc ca nous permet de reprouver le lemme du pompiste bcp plus facilement.

Mais cela aussi par exemple si on a 

$L \in \Sigma^* \\ w \in \Sigma^* \\ w \in^? L$

Avec la FNC on peut tester toutes les derivations possibles de la longueur de w. 

Recette : 

Data : $G=(V, \Sigma, R, S)$

But : $\hat G=(\hat V, \Sigma, \hat R, \hat S)$

1. Trouver $G_1 = (V_1, \Sigma, R_1, S_1)$ equivalente sans variables inutiles

  a) garder dans $V_1'$ les variables tq $A \Rightarrow^* w, w \in \Sigma^*$

  b) Garder dans $V_1$ les varaibles de $V_1'$ tq $\exists \alpha, \beta \in (V \cup \Sigma)^*$ avec $S \Rightarrow^* \alpha A \beta$

2. Trouver $G_2 = (V_2, \Sigma, R_2, S_2)$ equivalente sans variables inutiles et sans regles $A \rightarrow \epsilon$

  a) Enlever $A \Rightarrow^* \epsilon$ quelque chsoe comme $A \Rightarrow BC \Rightarrow BCDX \rightarrow \epsilon$ (un ensemble de variable qui mene a $\epsilon$) en ajoutant de nouvelles regles

  si $D \rightarrow A_1...A_k$ avec $A_1 \Rightarrow^* \epsilon$, il ne faut pas tout remplacer les occurences. 

exemple :

$A,B,C \Rightarrow^* \epsilon$ on enleve les regles $X \rightarrow \epsilon$ et on ajoute 

si $D \rightarrow aAbbAcBcCCdA$ ici on nimporte quelle variable peut donner $\epsilon$

il faut donc ajouter $D \rightarrow abbAcBcCCdA | aAbbcBcCCdA | aAbbAccCCdA | abbcBcCCda | abbAccCdA | ...$

3. Trouver $G_3 = (V_3, \Sigma, R_3, S_3)$ sans variables inutiles, sans $A \rightarrow \epsilon$ sans $A \rightarrow B$

Si on a quelque chose comme $X \rightarrow aAbcBa \\ X \rightarrow aBbcBa$ va donner $X \rightarrow A \Rightarrow A \rightarrow B$

4. Trouver $G_4= (V_4, \Sigma, R_4, S_4)$ sans variables inutiles, regles unitaires, regles $\epsilon$, et tq $A \rightarrow BC$ ou $A\rightarrow a$ sous la regle

$A \rightarrow X_1X_2 ... X_l, k \geq  2, x \in V_3 \cup \Sigma$ Pour chaque $a \in \Sigma$ qui apparait dans une regle ajouter une nouvelle variable $X_a$ et une regle $X_a \rightarrow a$, en changeant $A \rightarrow X_1, ... X_k$ en $A \rightarrow Y_1...YK$ et Y_i = x_a si x=a \in \Sigma et x_i sinon

Il ne me reste que 
$A \rightarrow a$ $A \rightarrow x_1, ..., x_k, x_i \in V_2$
si trouver $\hat G=(\hat V, \Sigma, \hat R, \hat S)$ en FNC, il suffit de remplacer les regles $A \rightarrow X_1,... X_k, k \geq 3$ par une serie de regles de la forme $A \rightarrow BC$ on remplace le A par $A\rightarrow x, y_1, ... Y_{k-2} \rightarrow X_{k-1}X_k \\ Y_1 \rightarrow x_2y_2$

faisons un exemple

$\hat S$ nouveau

$\hat S \rightarrow \epsilon$ si $\epsilon \in L(G)$

$\hat S \rightarrow \alpha$ tq $S \rightarrow \alpha$

il fait l'exemple 1 dans les notes

http://www-labs.iro.umontreal.ca/~hahn/ift2105/Notes/Chomsky.pdf

il y a quand meme des erreures dans ses notes, il va les corriger. 

le 2e exemple, il montre comment on peut se tromper. si on enleve les variables inutiles au debut, on peut se ramasser avec des variables inutiles quon ne peut plus changer dans notre iterations comme $C \rightarrow CC$ on doit donc recommencer. cest donc un risque de decider si on enleve les varialbes inutiles a la fin ou au debut. 

## Cours 17

Rappel : 

Si $L \in L_{reg}$, alors on peut decider si $w \in L$

Si $L \in L_{hc}$ alors on peut decider si $w \in L$, car si G est une ghc en FNC pour L, tout mot $w \in L$ est derivable en $2|w|-1$ applications des regles

### Machines de Turing

Definition formelle : Une Machien de Turing (MT) est definie par 

Q = un ensemble finis detats (tout est fini ici)

$\Gamma$ = alphabet du ruban

$\Sigma \subset \Gamma$ = alphabet dinput

$B \in \Sigma \setminus \Gamma$ = le symbole blanc ou vide

$s \in Q$ = etat initial

$q_A \in Q$ etat acceptant

$q_R \in Q$ etat refusant

$\delta : Q \times \Gamma \rightarrow Q \times \Gamma \times \{D,G\}$ = une function de transition

On ecrit donc $M = (Q, \Sigma, \Gamma, \delta, S, q_a, q_R)$

Definition : Soit $M = (Q, \Sigma, \Gamma, \delta, S, q_a, q_R)$ Une configuration de M est triplet (u,q,v) tq le ruban contient uv sur les premieres cases et la tete est sur le premier symbole de v

les cases sont donc $|u_1 | u_2 | ... | u_m | v_1 | .... | v_n$ et la tete est sur $v_1$

Definition : Soit M une MT, soit $u = u_1...u_m$, $V = v_1, ..., v_n$ $v_i, u_j \in \Gamma$, $i \leq j \leq m$

1. Une configutraiton (u,q,v) donnc (u,q,v) en 0 etapes $(u,q,v) \vdash^0 (u,q,v)$
2. Une configuration (u,q,v) donne (u', q', v') en 1 etapes $(u,q,v) \vdash^1 (u',q',v')$
  - soit $\delta(q, v_1) = (q', c, D)$ et $u' = u_1...u_mc$ et $v'= v_2...v_n$
  - soit $\delta(q, v_1 = (q', c, G)$ et $u' = u_1...u_{m-1}$ et $v'= u_mcv_2...v_n$

  si $u = \epsilon$, on aura dans le 2e cas $u=\epsilon$, $v'=cv_2,...v_n$

3. Une configuration $(u,q,v) \vdash^k (u', q', v')$ donne en k etapes sil existe une configuration $(\alpha, p, \beta)$ de M tq $(u,q,v)\vdash^1(\alpha, p, \beta)\vdash^{k-1}(u',q',v')$
4. Une configuration  (u,q,v) donne (u', q', v') sil existe un cas tq $(u,q,v) \vdash^k (u', q', v')$, on ecrit  $(u,q,v) \vdash^* (u', q', v')$

Def : Soit $M = (Q, \Sigma, \Gamma, \delta, S, q_a, q_R)$, le langage de M, $L(M)=\{w \in \Sigma^*: \exists u,v \in \Gamma^*\ tq\ (\epsilon, s, w)\vdash^*(u,q_A, v)\}$

Une MT reconnait son langage : elle accepte tous les mots de L(M), si, en plus, M refuse les mots qui ne sont pas dans L(M), on dit qu'elle decide son langage. Decider si $w \notin L(M)$, $\exists u',v' \in \Gamma^*$ tq $(\epsilon, s, w)\vdash^*(u', q_R, v)$

## Cours 18

Une machine de Turing  $M = (Q, \Sigma, \Gamma, \delta, S, q_A, q_R)$

une configuration $(u,q,v) \vdash^* (u',q'v')$ avec le vdash, cest quil existe une suite de transition qui permet de passer de u q v a la version prime. 

----------

On va faire un exemple

data : $a,b \in \N$

but : $f(a,b) = a+b$

ecrivons a et b en unaire.

$\Sigma = \{0,1,diese \}$, $\Gamma = \Sigma \cup\ Blanc$, $Q = \{q_0, q_A, q_R\}$

$$\delta(q_0, x) = 
\begin{cases}
(q_0, 1, D)\ si\ x=1\\
(q_R, 1, D)\ sinon
\end{cases}
$$

$$\delta(q_1, x) = 
\begin{cases}
(q_1, 1, D)\ si\ x=1\\
(q_2, 1, D)\ si\ x=diese \\
(q_R, 1, D)\ si\ x=0\ ou\ blanc
\end{cases}
$$

$$\delta(q_2, x) = 
\begin{cases}
(q_2, 1, D)\ si\ x=1\\
(q_3, blanc, G)\ si\ x=blanc \\
(q_R, 1, D)\ si\ x=0\ ou\ diese
\end{cases}
$$

$$\delta(q_3, x) = 
\begin{cases}
(q_4, blanc, G)\ si\ x=1\\
(q_R, x, D)\ si\ x=0\ ou\ diese
\end{cases}
$$

$$\delta(q_4, x) = 
\begin{cases}
(q_A, blanc, G) \\
(q_R, x, D)\ si\ x=0\ ou\ diese
\end{cases}
$$


sur le ruban ca va ressemble a ca : `111.....111 # 11...11`, la pemiere partie avant le de # est le $a$ et lautre cest le $b$.

quand on voit le diese, on lenleve et on decale les auters a gauche. 

pour implementer laddition

nous avons un mot en binaire de longueure k $a_1, a_2, ..., a_k$#$b_1,b_2,....b_l$ ## (marqueur de fin de mot)

une iteration : $a_1, a_2, ...,$##$b_1,b_2,....$###$(a_k+b_l \bmod 2)$ On doit se rappeler si cest 1+1 pour la retenue

a la fin : # k fois, # l fois, plus le resultat (a lenvers) quon va aller les remettre dans lordre au premiere caractere du ruban. (Cest juste un exemple)

-------

Certain problemes peuvent etre plus facile a representer si la machine a plusieurs ruban qui ont chacun leur tete, mais qui bougent en meme temps.


1. $a_1, a_2, ..., a_k$#$b_1,b_2,....b_l$ est linput
2. $a.....a$
3. $b.....b$
4. la somme resultante a lenvers

quand on a la somme va pourra recoopier a partir du ruban 4 sur le ruband la somme resultante dans le bon ordre. 

evident, si on a une seule tete (ou plusieurs qui bougent en meme temps), on peut tout representer sur un seul ruban, on va avoir besoin de mettre des marqueurs qui indique la fin d'un ruban, etc. 

on pourrait ecrire une transition comme ca $\delta((q'_{i_1}, q_{i_2}, ... q_{i_k}), (a_1, a_2,...,a_k))=((q'_{i_1}, .... q'_{i_k}), (b_1,...,b_k),(d_1, d_2,....d_k))$

$\approx (\delta_1(q_1,a_1), \delta_2(q_2, a_2)....,\delta_k(q_k, a_k) )$

supposons $a^nb^nc^n \in \{a,b,c\}^*,\ n \in \N$

pour se rappeler du nombre quon a compter, on peut ecrire la memeoire directement sur le ruban, soit mettre un separateur sur notre seul ruban, ou utilsier un autre ruban. il semble y avoir plusieurs methodes pour sauvegarder lla memoire (pile, compter carrement, etc. )

autre exemple : $n \cdot m$ tq $n,m \in \N$

------

On peut representer un ruban infini des 2 cotes comme un ruban infini dun seul cote.

`...,-3,-2,-1,0,1,2,3,...` deviendrait `0,-1,1,-2,2,-3,3,-4,4,...`

donc maintenant on peut se souvenir de letat des choses, du nombres des variables, on peut utilsier des rubans multiples, infinis dans les 2 sens.

notons qu'un ruband a plusieurs tetes est aussi equivalent. 

autre exemple

on veut faire n+m $\Sigma=\{0,1\}$, $\Gamma=\{0,1,Blanc\}$

on va sentendre des mots de tailles 4 `0000,0001,...` ya 16 possibilites

on va donc avoir besoin de sentendre sur une suite de bits qui vont etre nos separateurs, exemple `0000` est un separateur et `0001=0`, tout depend du codage qu'on sentend a la base. 

## Cours 18 La fin approche!

Machine Turing non-deterministe $M=(Q, \Sigma, \Gamma, \delta, s, q_A, q_R)$

pour deterministe c'est $\delta: Q \times \Gamma \rightarrow Q \times \Gamma \times \{D,G\}$

tout ce qui change c'est la fonction $\delta:Q \times \Gamma \rightarrow 2^{Q \times \Gamma \times \{D,G\}}$

en gros si on fait le graphe dexecueion, pour la deterministe, cest une chaine bien precise. pour la non deterministe, cela fait presque un arbre ou on ne sait psa quelle decision a pris la machine. 

la difference entre reconnait et decide?

une machine de turing decide son langage si elle s'arrete sur toute entree , dans le fond avec nimporte quelle input on arrive dans letat acceptant ou refusant.

elle reconnait son langage si elle sarrete en etant $q_A$ si $w \in L(M)$

soit $L_{dec} = L_{r}$ la classe des langages decidables et $K_{rec}=L_{r.e}$

r = recursive et r.e = recusivement enumerable.

notons que $L_{dec} \subseteq L_{rec}$

$L \in L_{dec}$ une MT M tq L(M)=L

Il faut parler de codage

$MT = (Q, \Sigma, \Gamma, \delta, s, q_A, q_R)$

sqdg 
$$ Q =\{q_0, q_1, q_2, ... q_n\} \\ \Sigma = \{a_1,...a_m\} \\ \Gamma = \{b_1, ... b_l\}\cup \Sigma \\ q_0 = s \\ q_1 = q_A \\ q_2 =q_R\\ q_{m+1}=B$$

$(n, m, l, (i,j, s, b, d),(...), (...),...,(...)) \rightarrow <M>$ code de M sur {0,1}, on peut aussi coder <G> une ghc

$\delta(q_i, q_j, q_s, q_t, d)$ avec $d \in \{D, E\}$

donc a chaque MT, correspondra un mot coder en binaire....


Soit M une MT et soit $w \in \Sigma^*$

<M,w> code de M, <M> avec le code du mot w <w>. 

$A_{tm} = \{<M,w> | M\ est\ MT,\ w\ un\ mot\, w \in L(M)\}$ ou par exemple $\{<G> | G\ est\ ghc, L(G) \neq \emptyset\}$ou par exemple $\{<M> |M\ est\ AFD, L(M) \neq \emptyset\}$

$\{<M,w> | M\ est\ un\ AFD\, w \in L(M)\}$ decidable? oui. 

On aimerais savoir si cest possible que pour 2 afds, A, B, $L(A)=L(B)$

soit $L=(L(A) \cap \bar L(B))\cup (L(B) \cap \bar L(A))$

$L = \emptyset$ ssi $L(A)=L(B)$

soit C tq $L(C)=L$

on fait une matrice avec tous les mots possible et toute les machines et on essaie de trouver $w_j \in L(M_i)$

$L_d = \{<M,w> | w \notin L(M)\}$ on contedris le theoreme, cest la diagonalisation de Cantor. donc ce ne est pas possible de savoir si un mot nest pas dans le langage. donc $L_d$ nest pas un langage reconnaissable, ni decidable. $L_d \notin L_{rec}$


### THM
$L \in L_{dec}$ alors $\bar L \in L_{dec}$

### THM
$L \in L_{rec}$ alors $\bar L \in L_{rec}$ $\implies$ $L \in L_{dec}$


| decidable | reconnaissable | complement reconnaissable |
| :-------: | :------------: | :-----------------------: |
|     V     |       V        |             V             |
|     X     |       X        |             V             |


$\bar L = \{<M_i, w_i> | w_i \in L(M_i)\}$
si le mot appartient au langage, on va le trouver, sinon il se ne passera rien. 


$L = \{<M_i, w_i> | w_i \in L(M_i)\}$

## Cours 19

Reconnaissanble vs decidable

|               | decidable | reconnaissable |
| :-----------: | :-------: | :------------: |
|     $L_d$     |     X     |       X        |
|  $\bar L_D$   |     V     |       V        |
| $A_{TM}= L_u$ |     F     |       V        |
|     HALT      |     F     |       ?        |

tout depend de ce quon appel une reduction, soit reduire un probleme a un autre probleme

soit deux $L_1$, $L_2$ deux languages. $L_1$ se reduit (Turing) a $L_2$ si on donne $L_1$ a une MT et on utilise $m(L_2)$ pour decider $L_1$

Prouvons $A_{TM}= L_u = \{<M,w> | w \in L(M) \}$ n'est pas decidable. 

$L_u \in L_{rec}$ car etant donne <M, w>, on peut simuler M sur w. (il fait un dessin de machine on lui donne <M,w>, sur $M_{L_u}$ et la machine fait passer w sur M. si M donne non, alors on output non, si M donne oui, alors on output oui.)

$L_u \notin L_{rec}$, on fait le meme dessin comme avant. On donne <M, w> a la machine et cette machine devrait decider le langage diagonale, mais cest pas possible par ce quon a vu. 

Deuxieme prueve que $L_u \notin L_{dec}$. Soit $M_u$ une une decide $L_u$

On passe a cette machine, le code de la machine m, <D>. Elle va utilise $T \rightarrow <M,<M>>$ et on lui donne a $M_u$ a linterieur. Si $M_u$ donne oui, on output non, et si elle dit non, on output oui. On va appeler cette machine D. 

donc

$<D> \in L(D)$ ssi $<D> \notin L(D)$ cest le langage diagonal, qui nest pas decidable en theorie. 

Donc ca ressemble au problme quon a deja vu HALT = {<M,w> | M s'arrete sur w}

supporons $M_{HALT}$ decide HALT. On lui domme <M,w>, cela passe en premier dans $M_{halt}$. si elle dit oui, alors on dit non, si elle dit oui, on passe w dans M, et on ouput la reponse de M (oui=oui, non=non).

appelons de langage $\hat M$, alors $L(\hat M) = L_u = A_{TM}$ donc si on peut faire ca, on peut faire une machine pour decider le langage universel,et encore une fois, on sait que ce nest pas decidable. on ne sait pas si cest reconnaissable. 

$L_{\emptyset} = \{<M> | L(M) = \emptyset\} \notin L_{dec}$

$\bar L_{\emptyset} = \{<M> | L(M) \neq \emptyset\}$

Supposons $M_{\emptyset}$ decidable $M_{\bar \emptyset}$ etant donne <M,w>, soit $M_{<M,w>}$ la mt suivante. 

on fait la boit, on lui donne x (un mot sous un alphabt), elle va demarrer M sur w, si elle dit oui, on dit oui. 

$$ L(M_{<m,w>})=
 \begin{cases}
\bar \emptyset\ si\ w \notin L(M) \\ 
\Sigma^* si\ w \in L(M)
\end{cases}
$$

une autre machine, on lui donne <M,w>. dans le transformateur de la machine, elle va transformer en $<M_{<M,w>}>$ et donne ca a la machine $M_{\bar \emptyset}$, si elle dit oui ,on output non, elle dit non, on outpur oui. cette machine decide donc $M_u$ tq
$L(M_u)=L_u = A_{tm}$ donc ce nes tpas decidable. 

Comment generer des paires d'entiers? On va simuler M sur $w_i$ pendant j transitions...

## Cours 20

Suppose qu'un languages est decidable ou reconnaissable, si cest vrai, par $M_L$

On construit une MT auxilaire $\hat M$ dont le language est L ssi quelque chose. 

On construit une MT qui decide ou reconnait un language connu comme indecidable ou non reconnaissable a laide de $M_L$

Donc avec son dessin de machine quil fait tout le temps, ML est dans la machine principale. le transformateur nous donne le code de $<\hat M>$, la machine au complet est $M^*$

|          L           |  dec  | recon |
| :------------------: | :---: | :---: |
|        $L_D$         |   X   |   X   |
|      $\bar L_D$      |   X   |  oui  |
|        $L_U$         |   X   |  oui  |
|      $\bar L_U$      |   X   |   X   |
|         HALT         |   X   |  oui  |
|    $\bar {HALT}$     |   X   |   X   |
|   $L_{\emptyset}$    |   X   |   X   |
| $\bar L_{\emptyset}$ |   X   |  oui  |
|      $L_{dec}$       |       |   X   |
|    $\bar L_{dec}$    |       |   X   |

Rappel:
$L_{HALT} =\{<M,w> | M\ decide\ w\}$

$L_{\emptyset} = \{<M> | L(M) = \emptyset\} \notin L_{dec}$

$\bar L_{\emptyset} = \{<M> | L(M) \neq \emptyset\}$

donc

1. $L_{dec}= \{<M>|\ L(M) \in L_{dec}\}$

2. $\bar L_{dec}= \{<M>|\ L(M) \notin L_{dec}\}$

le in l_dec, cest la grande classe des languages decidables. 

1. Supposons $L_{dec} \in L_{rec}$ disons par $M_{dec}$

on construit la machine $\hat M$, etant donne une machine et un mot $<M,w>$

(on lui donne x), dans la machine on donne w a M, si elle dit oui, on part $M_u$ avec x, si elle dit oui, on dit oui.

$L(\hat M)= \begin{cases}\\ L_u\ si\ w \in L(M) \\ \emptyset\ si\ w \notin L(M) \end{cases}$

ie si $w \in L(M)$, $\hat M$ accepte x ssi $x=<M',w'>$

Soit $M^*$: on donne $<M,w>$ a la machine, le transformateur nous donne $\hat M$, et lui donne ca a la machine $M_{dec}$. Si $M_{dec}$ dit oui, 

donc $L(M^*)= \bar L_u$ contredisant $\bar L_u \notin L_{rec}$

2. Supposons que $\bar L_{dec}$ soit reconnaissable, par $\bar M_{dec}$ 

On donne a la machine $<M,w>$, le transformateur nous donne $\hat M$ quon donne a $\bar M_{dec}$, si elle dit oui, on dit oui. 
cette machine reconnait si le langage nest pas decidable. 

pour la machine $\hat M$, on lui donne x. quon envoie dans $M_u$, dans la mahcine on donne w a M, si M et $M_u$ disent oui, alors on dit oui. 

donc $L(\hat M)= \begin{cases} \Sigma^*\ si\ w \in L(M) \\ L_u\ si\ w \notin L(M) \end{cases}$

donc $L_(\hat M)$ est decidable ssi $w \in L(M)$

donc $L(\bar M_u)$ est non decidable ssi $w \notin L(M)$ 

donc si $\bar M_dec$ exisant, on pourrait reconnaitre le complement du langage universel. 

le prof a lair de comparer $M_u$ comme un ordinateur regulier. toute forme de cpu. 

### Remarque sur le devoir
on a pas vu ca en demo, (no shit). il y a plus de choses que ca...

Soit X un ensemble, une **propriete** P de X est une partie de X. La propriete P est triviale si $P=X\ ||\ P=\emptyset$

Soit $L_{dec}$ la classe de langages decidables. 

Soit $S \subseteq L_{dec}$. On peut definir $L_S = \{<M> | L(M) \in S\}$

### Theoreme (Rice)

Toute propriete non-triviale des langages decidables est indecidables. 

Soit $S$ une propriete non triviale des langages decidables, le langage $L_S= \{<M>\ |\ L(M) \in S\}$ est indecidable. 

notons que $L(M) \in L_{dec}$

Preuve :

Soit $S$ tq $\emptyset \notin S$ une propriete non-triviale de $L_{dec}$