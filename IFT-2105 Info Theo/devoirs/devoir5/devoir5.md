# Devoir 5
## par Marc Laliberté matricule : 20085533 et Imane El Alaoui Najib matricule : 0917827

### No1
Utilisez le lemme du pompiste pour prouver que
$$ L = \{ww: w \in \{a,b,c,d\}^*\} \notin L_{reg}$$
Montrons par contraposee que 
$$ L \notin L_{reg} $$
Soit p, la constante de pompage (lemme du pompiste) ou
$$ p > 0 $$
soit
$$ w= a^pba^pb\ w \in L\ |w| \geq p \\ w = xyz\\ x =a^m,\ y = a^n,\ z =a^lba^pb \\ l = p-m-n \\ m+n \leq p\ et\ n>0  $$
pour
$$ i=0,\ xy^iz=\ xy^0z \\ = a^mba^pb\\ m \neq p\ car\ n>0\\ w \notin L \\L \notin L_{reg} $$

------

### No2
Donnez une RegEx pour le langage
$$ \{w \in \{a,b\}: |w|_{ab} = |w|_{ba}\} $$
ou w commence et fini par la meme lettre.

Soit
$$ \Sigma = (a+b) $$
RegEx:
$$ (a\Sigma^* a)+(b\Sigma^*b)+(a+b+\epsilon) $$

--------

### No3
Prouvez que le langage L
$$ \{w \in \{a,b,c,d,e\}^*: |w|_{ab} = |w|_{ba}\} \notin L_{reg}$$
Utilison Myhill - Nerode que le langage a un nombre infini de classe d'equivalences.

$$ [(ab)^i] $$
est une classe d'equivalence de R_L
$$ \forall i \in \N $$

Prenons
$$ w = (abe)^i \\ w \in L $$

Pour toute répétition i, w reste dans le langage, mais
$$ i \in \N $$
et N est infini et nous avons une infinité de classe d'équivalences.

alors
$$ L \notin L_{reg} $$

Par Myhill - Nerode

<!-- Prenons
$$ w= (ab)^i\Sigma^*(ba)^i\\ i \in \N\\ w \in L $$

mais si
$$ j \neq i \\ (ab)^i\Sigma^*(ba)^j \\ w \notin L \\ [(ab)^i] \neq [(ba)^j]$$

car pour
$$ z = (ba)^i,\ (ab)^i\Sigma^*z \in L\ mais\\ (ab)^j\Sigma^*z \notin L\\ (ab)^i\Sigma^*(ba)^i \in L\\ [(ab)^i] \neq [(ba)^j] $$ -->

