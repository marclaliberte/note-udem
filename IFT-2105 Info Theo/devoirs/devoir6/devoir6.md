# Devoir 6
## par Marc Laliberté matricule : 20085533 et Imane El Alaoui Najib matricule : 0917827

Utilisez Myhill-Nerode pour prouver que

---------

### No1
$$ \{a^nb^m:\ pgcd(n,m)=1\}\subseteq \{a,b,c,d\}^*\notin L_{reg} $$

soient
$$ i != j\\ i,j \in \N\\ i>1,\ j>1 \\ pgcd(i,j)=1$$

Prenons
$$ a^i,\ a^ib^i \notin L\\ car\ pgcd(i,i)=i$$
Mais si
$$ i != j\ \\ a^ib^j \in L\\ car\ pgcd(i,j)=1$$

Soit
$$ R_L $$
la relation d'equivalence sur
$$ \Sigma^* $$
telle que
$$ xR_Ly\ ssi\ \forall z \in \Sigma^*,\ xz \in L,\ ssi\ yz \in L $$

Alors
$$ [a^i] != [a^j] $$

car pour 
$$ z = b^i,\ a^iz \notin L\ mais\\ a^jz \in L\\ a^jb^i \in L\\ [a^i] != [a^j] $$

Il y a une infinité de paires (i,j) dans N et N est infini, donc infini de classes, Donc L n'est pas un langage regulier par M-N.

---------

### No2
$$ \{w \in \{0,1\}:\ w \equiv i \bmod k\} \in L_{reg} \\ pour\ 2 \leq k \in \N\\ i \in [k] $$

Soit
$$ R_L $$
la relation d'equivalence sur
$$ \Sigma^* $$
telle que
$$ xR_Ly\ ssi\ \forall z \in \Sigma^*,\ xz \in L,\ ssi\ yz \in L $$

Nos classes d'equivalences
$$ [\epsilon],\ [0],\ [1], [...], [k-1]$$

Soit
$$ w \in \Sigma^* $$
si
$$ w = \epsilon,\ w \in [\epsilon] $$
si
$$ w \equiv 0 \bmod k, w \in [0] $$
si
$$ w \equiv 1 \bmod k, w \in [1] $$
...

si
$$ w \equiv k-1 \bmod k, w \in [k-1] $$

soit
$$ z \in \Sigma^* $$

si 
$$ z=0 \\ w \in [i]$$
alors

$$ wz\ vaut\ 2w  \\ donc\   w \in  [2i \bmod k] \\$$

si 
$$ z=1 \\ w \in [i]$$
alors

$$ donc\  wz\ vaut\ 2w+1  \\ w \in  [2i+1 \bmod k] \\ $$

$$ 2i \bmod k \equiv 2(nk+i) \bmod k $$

w est dans une des classes dequivalences

donc mes classes partionnement \sigma^* et raffine R_L, donc R_L a un nombre fini de classes dequivalences et L est regulier