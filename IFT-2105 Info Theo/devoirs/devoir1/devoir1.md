
# Devoir 1
## par Marc Laliberté matricule : 20085533
------

### No 1
**Donner un exemple d'un langage fini et un exemple d'un langage infini. N'oubliez pas de spécifier l'alphabet.**
- Language fini est un ensemble *L* a cardinalité finie
  $$ |L|< \infty$$
ex : 
$$ \Sigma = \{a,b\} \\
L = \{w \in \Sigma |\ |w|=2 \}\\
L = \{aa,\ ab,\ bb,\ ba\}
$$
- Language infini est un ensemble *L* a cardinalité infinie
$$ |L| = \infty $$
Ex: 
$$
\Sigma = \{a\} \\
L = \{w \in \Sigma |\ |w| = a^k où\ k>1\} \\ 
L = \{a,\ aa,\ aaa,\ ...\ ,\ a^k \}
$$

---------- 

### No 2

**Prouvez que**
$$ L^* = (L^*)^* $$
**Est-ce que cette dernière dépend de l'alphabet?**

Preuve:

Soit
$$  w \in (L^*)^*$$
Alors
$$  w = w_1\ ...\ w_k$$
Donc
$$ w \in L^* $$
Aussi
$$  \forall i, w_{i,1}\ ...\ w_{i,k_i}\ où\ w_{ij} \in L$$
Donc, si on rajoute des caracteres à gauche et à droite: 
$$  w = w_{1,1}\ ...\ w_{1,k_1}\ ...\ w_{k,1}\ ...\ w_{k,k_k} \in L^*$$
Cela démontre que 
$$  (L^*)^* \subseteq L^* \\ L^* = (L^*)^*$$
et ce, peu importe l'alphabet.