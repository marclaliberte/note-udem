# Devoir 10
## par Marc Laliberté matricule : 20085533 et Imane El Alaoui Najib matricule : 0917827

Soit une Machine de Turing $M=(Q, \Sigma, \Gamma, \delta, s, q_A, q_R)$

$$ \Sigma = \{0,1,\$ \} \\ \Gamma = \Sigma $$

Algorithme (on considere que l'input est correct, et pas d'espaces blanc dans les a ou b)

1. De l'etat initial $s$, lire a droite sans modifier le ruban
2. A la lecture d'un espace (apres b), aller a gauche, aller a $l$ (pour lecture)
3. Aller a letat $y$ si on lit 1, aller a letat $x$, si on lit 0 et ecrire 'blanc' a la place du symbole lu et a aller a gauche.

    A la lecture de $:
    passer a letat f et allar a gauche

    si on lit 0 ou 1, aller a $q_A$

    si on lit x, ecrire 0, rester dans $f$, aller a gauche,

    si on lit y, ecrire , rester dans $f$, aller a gauche.

4. A la lecture de $\$$ (on revient dans a) passer de letat $x$ a $x_2$ ou $y$ a $y_2$ et aller a gauche
5. A lecture de 0 :
    passe de $x_2$ a $s$, ecrire $x$ et aller a droite

    passer de $y_2$ a $s$ ecrire $y$ et aller a droite
   
   A la lecture de 1 : 
    passer de $x_2$ a $s$ ecrire $y$ et aller a droite

    passer de $y_2$ a $r$ pour retenue, ecrire $x$ et aller a gauche


6. De $r$, a la recherche de:
    
   0, passer de $r$ a $s$ et ecrire 1, aller a droite

   1, rester dans $r$, ecrire $0$, aller a gauche

   Si on est rendu dans la case 0 du ruban (on ne peut plus aller gauche), decaler le tout de 1 case. (inserer algo de decalage ici) et rajouter 1 a gauche

Algo de decalage en pseudo-code

1. Aller a droite jusqu'a blanc et aller a gauche:
   
   Si on lit 0 : aller a letat 0, aller a droite, ecrire 0, retourner en mode lecture en allant a gauche.

    si on lit 1, aller a letat 1, aller a droite, ecrire 1

    si on lit $, aller a letat $, aller a droirte et ecrire $.

Formellement :

$(s, X) \rightarrow (s, X D)$ tq $X \neq \ blanc$

$(s,\ blanc) \rightarrow (l,\ blanc, G)$

$(l, 1) \rightarrow (y,\ blanc, G)$

$(l, 0) \rightarrow (x,\ blanc, G)$

$(l, \$) \rightarrow (f,\ blanc, G)$

$(x, X) \rightarrow  (x, X, G)$ tq $X \neq \$$

$(x,\$) \rightarrow  (x_2, \$, G)$

$(y,X) \rightarrow (y,X, G)$

$(y,\$) \rightarrow (y_2, \$, G)$

$(f,0) \rightarrow (q_A, 0, R)$ tq R = reste, bouge pas, fini

$(f,1) \rightarrow (q_A, 1, R)$

$(f,x) \rightarrow (f,0,G)$

$(f,y) \rightarrow (f,1,G)$

$(f,X) \rightarrow  q_A$

$(x_2, 0) \rightarrow (s,x,D)$

$(y_2, 0) \rightarrow (s,y, D)$

$(x_2, 1) \rightarrow (s, y, D)$

$(y_2, 1) \rightarrow (r, x, G)$

$(r,0) \rightarrow (s, 1, D)$

$(r,1) \rightarrow (r, 0, G)$

$(r, X) \rightarrow decalage$