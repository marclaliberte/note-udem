# Devoir 8
## par Marc Laliberté matricule : 20085533 et Imane El Alaoui Najib matricule : 0917827

Pour chacun des langages suivants dites s’il est ou n’est pas hors-contexte. Prouver vos reponses soit en donnant une grammaire hors-contexte qui genere le langage, soit en prouvant qu’il n’est pas hors-contexte

- $$ L_1 = \{ w \in \{a,b,c,d\}^*: |w|_a = |w|_b\} $$
Est hors contexte, voici la grammaire : 

$$ S \rightarrow OBOAO\ |\ OAOBO\ |\ O  \\ O \rightarrow Sc\ |\ Sd\ |\ S\ |\ \epsilon \\
A \rightarrow a\\ B \rightarrow b
$$


- $$ L_2 = \{ w \in \{a,b,c,d\}^*: |w|_a\ \neq |w|_b\} $$
Est hors contexte, voici la grammaire : 

$$ S \rightarrow OBOBOAO\ |\ OAOAOBO\ |\ O  \\ O \rightarrow Sc\ |\ Sd\ |\ S\ |\ \epsilon \\
A \rightarrow a\\ B \rightarrow b
$$


- $$ L_3 = \{ w : |w|_a = |w|_b = |w|_c \} $$

Est hors contexte, voici la grammaine : 

$$ S \rightarrow \epsilon | aSbSc | aScSb | bSaSc | bScSa | cSaSb | cSbSa $$


- $$ L_4 = \{a^ib^jc^k  : i\ \neq j\ ||\ j\ \neq  k\} $$
N'est pas hors contexte,

Alors, il existe une constante de pompage
$$ p \in \N $$
tq si
$$ w \in L$$
est de longueure an moins p, alors il existe
$$ u,v,x,y,z \in \Sigma^* $$
verifiant
1. $$ w = uvxyz $$
2. $$ |vxy| \leq p $$
3. $$ |vy| > 0 $$
4. $$ \forall i \in \N, uv^ixy^iz \in L $$

