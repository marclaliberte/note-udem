# Devoir 4
## par Marc Laliberté matricule : 20085533 et Imane El Alaoui Najib matricule : 0917827

## No 1

![no1](no1.svg)

## No 2

$$ ((0)^*(0011)^*(10)^*)^*.(1|0)+((0)^*(0011)^*(10)^*)^* $$
![no2](no2.svg)
