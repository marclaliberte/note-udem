# Devoir 3
## par Marc Laliberté matricule : 20085533 et Imane El Alaoui Najib matricule : 0917827

Pour chacun des langages suivantes, dites s'il est regulier ou pas.

### No 1
$$ L_1 = \{w \in \{a,b,c,d,e\}^* :\ |w|_a \equiv 3 (\bmod 6)\ ou\ |w|_b \equiv 1(\bmod 2)\} $$

$$ L_1 \in L_{reg} $$
car il existe un AFD

$$ M_1 = (Q_1, \Sigma_1, \delta_1, s_1, F_1)   \\ Q_1=\{q_0,...,q_{11}\}\\ s_1 = q_0 \\ F_1 = \{q_3,\ q_6,\ q_7,\ q_8\, q_9,\ q_{10},\ q_{11}\} \\
\{q_j\}\delta_1 \ j=0,...,5:
\begin{cases}
(q_j\ \times a) \rightarrow q_{j+1 \bmod 6} \\
(q_j \times b) \rightarrow q_{j+5} \\
(q_j \times c|d|e) \rightarrow q_j
\end{cases} \\

\{q_k\}\delta_1\ k=6,...,11\ : 
\begin{cases}
(q_k\ \times a) \rightarrow q_{k-5 \bmod 6} \\
(q_k \times b) \rightarrow q_{k-6} \\
(q_k \times c|d|e) \rightarrow q_k
\end{cases}
$$
![m1](l1.svg)

Tous les mots de L_1 peuveut etre generer par M_1
$$ L_1 \subseteq L(M_1) $$
$$ w \in \Sigma^* \\ \forall w \in \Sigma^*, \hat \delta(q_0, w) = q_{|w|_a \equiv 3 (\bmod 6)}\ ou\ q_{|w|_b \equiv 1(\bmod 2)} $$

Tous les mots de L(M_1) sont dans L_1 
$$ L(M_1) \subseteq L_1$$
car ses mots sont soit
$$ |w|_b \equiv 1(\bmod 2) \rightarrow q_6,...,q_{11}$$
ou 
$$ |w|_a \equiv 3 (\bmod 6) \rightarrow q_3$$

En conclusion:
$$ L(M_1) = L_1 \\ L_1 \in L_{reg} $$

-------

### No 2
$$ L_2 = \{w \in \{a,b,c,d,e\}^*:\ |w|_a \equiv |w|_b\ (\bmod 4) \} $$
Oui car on pourrait avoir un afd

$$ M_2 = (Q_2, \Sigma_2, \delta_2, s_2, F_2) \\ Q_2 = \{q_0,\ ...,\ q_{15}, q_p\} \\ s_2 =\{q_0\} \\ F=\{q_0, q_5, q_{10},q_{15}\}\\\delta_2(q_i, w) =
\begin{cases}
(q_i\ \times a) \rightarrow q_{i+1}\ \forall i \setminus \{3,7,11,15\} \\
(q_i\ \times a) \rightarrow q_{i-3}\ si\ i=3,7,11,15 \\
(q_i\ \times b) \rightarrow  q_{i+4}  \forall i \setminus \{12,13,14,15\} \\ 
(q_i\ \times b) \rightarrow  q_p\ si\ i=12,13,14,15 \\
(q_i \times c|d|e) \rightarrow q_i
(q_p)\times \Sigma_2) \rightarrow q_p
\end{cases}
$$
![no2](no2.svg)


Tous les mots de L_2 peuveut etre generer par M_2
$$ L_2 \subseteq L(M_2) $$
<!-- $$ w \in \Sigma^* \\ \forall w \in \Sigma^*, \hat \delta(q_0, w) = q_{|w|_a \equiv 3 (\bmod 6)}\ ou\ q_{|w|_b \equiv 1(\bmod 2)} $$ -->

Tous les mots de L(M_2) sont dans L_2
$$ L(M_2) \subseteq L_2$$
<!-- car ses mots sont soit
$$ |w|_b \equiv 1(\bmod 2) \rightarrow q_6,...,q_{11}$$
ou 
$$ |w|_a \equiv 3 (\bmod 6) \rightarrow q_3$$ -->
En conclusion:
$$L(M_2) = L_2 \\  L_2 \in L_{reg} $$

----------

### No 3
$$ L_3 = \{w \in \{a,b\}^* :\ |w|_{ab} = |w|_{ba}\} $$

Si on considere que le mot
$$ w = aba \notin L_3 $$

Montrons par contraposee que 
$$ L_3 \notin L_{reg} $$
Soit p, la constante de pompage (lemme du pompiste) ou
$$ p > 0 $$
soit
$$ w=(ab)^p(ba)^p,\ w \in L,\ |w| \geq p \\ w = xyz : x=(ab)^{m},\ y=(ab)^{n},\ z=(ab)^{p-m-n}(ba)^p \\ m+n \leq p\ et\ n>0$$
pour
$$ i=0,\ xy^iz=\ xy^0z \\ = (ab)^{p-n}(ba)^p \\ p-n<p\ car\ n>0\\ w \notin L_3$$

*notons que les parentheses ne sont presentes que pour aider a la lecture*

donc en conclusion :
$$ L_3 \notin L_{reg} $$

Si on considere que le mot
$$ w = aba \in L_3 $$
Il un existe un automate
![no3AFD](no3AFD.svg)

$$ M_3 = (Q_3, \Sigma_3, \delta_3, s_3, F_3)   \\ Q_3=\{q_0,...,q_6\} \\ s_3 = q_0 \\ F_3 = \{ q_3,\ q_6 \} \\ 
\delta_3(q_i, w) =
\begin{cases}
(q_i\ \times a) \rightarrow q_{i+1}\ si\ i=0,2,4 \\
(q_i\ \times a) \rightarrow  q_{i}\ si\ i=1,3,5 \\ 
(q_i\ \times a) \rightarrow  q_{i-1}\ si\ i=6 \\
(q_i\ \times b) \rightarrow q_{i+1}\ si\ i=1,5 \\
(q_i\ \times b) \rightarrow  q_{i+4}\ si\ i=0 \\ 
(q_i\ \times b) \rightarrow  q_{i}\ si\ i=2,4,6 \\
(q_i\ \times b) \rightarrow  q_{i-1}\ si\ i=3 \\
\end{cases}
$$


-------

### No 4
$$ L_4 = \{a^nb^n:\ 1 \leq n \leq 10^{10}\} $$

Etant donne que 
$$  1 \leq n \leq 10^{10} $$

L_4 est un langage fini donc regulier par definition. 


----


### No5

Montrons par contraposee que 
$$ L_5 \notin L_{reg} $$
Soit p, la constante de pompage (lemme du pompiste) ou
$$ p > 0 $$
soit
$$ w=(abc)^p(ba)^p,\ w \in L,\ |w| \geq p \\ w = xyz : x=(abc)^{m},\ y=(abc)^{n},\ z=(abc)^{p-m-n}(ba)^p \\ m+n \leq p\ et\ n>0$$
pour
$$ i=0,\ xy^iz=\ xy^0z \\ = (abc)^{p-n}(ba)^p \\ p-n<p\ car\ n>0\\ w \notin L_5$$