# Devoir 7+
## par Marc Laliberté matricule : 20085533 et Imane El Alaoui Najib matricule : 0917827

Prouvez que tout langage regulier est hors-contexte.

Soit un AFD
$$ M = (Q, \Sigma, \delta, s, F) $$

Alors, on peut ecrire une ghc
$$ G=(V, \Sigma, R, S) $$
Telle que
$$ L(M) = L(G) $$
Avec
$$  \Sigma = \Sigma \\ V = \{Q\} \\ R\ =\ A \rightarrow xB\ \  A, B \in Q,\ x \in \Sigma\ tq\ \delta(A,x) = B \\
     \cup A \rightarrow \epsilon\ si\ A \in F\\
S= q_0
$$