# Devoir 9
## par Marc Laliberté matricule : 20085533 et Imane El Alaoui Najib matricule : 0917827

### 1. 
Soit $G=(V, \Sigma, R, S)$ une grammaire hors-contexte en forme normale de Chomsky et telle que $\epsilon \notin L$. Prouvez en details que $\forall w \in L(G)$ est derive par G en exactement $2|w|-1$ etapes.

Faisons une preuve par induction sur la longueure de w

**Base:**

Si $|w|=1$, alors w contient exactement un seul caractere $c$. Alors toutes les derivations de $w$ a partir d'une variable $A$ va avoir une seule etape, soit $A \rightarrow c$

**Induction:**

On suppose par induction que c'est vrai pour tous les mots $|w| < k$. Donc, soit $w$ et $|w| = k$ et $w$ est la derivation de la variable A. 

Prenons la premiere etape de la derivation. Si cette etape a une regle de la forme $A \rightarrow c$, alors $w$ ne contient qu'un seul caractere et nous sommes de retour a l'etape de base.

Considerons le cas ou cette contient une regle de la forme $A \rightarrow BC$

Nous pouvons deriver X comme suit $A \rightarrow B \rightarrow c'$ ou $c' \neq \epsilon$ car $\epsilon \notin L$

et $A \rightarrow C \rightarrow c''$ ou $c'' \neq \epsilon$ car $\epsilon \notin L$

Supposons maintenant que $|c'| = i$ et $|c''|=j$. Comme nous savons que $w =c'c''$ donc $i+j=k$

Comme nous savons que G est sous la forme normale de Chomsky, ni B ni C n'est un symbole de depart, donc ne peuvent devenir $\epsilon$

donc $i>0$ et $j>0$ et $|c'|=i< i +j =k$ et similairement $|c''|=j< i +j =k$ 

Nous pouvons donc appliquer notre hypothese d'induction sur $c'$ et $c''$

Comme $|c'|=i$, la derivation $B \Rightarrow^* c'$ dont prendre exactement $2i-1$ etape et similairement $C \Rightarrow^* c''$ dont prendre exactement $2j-1$ etapes.

Donc la derivation de $w$ a partir de $A$ prend $1+(2i-1) +(2j-i) = 2(i+j)-1 = 2k-1 etapes$

comme nous savons que $|w|=k$ alors nous savons que $\forall w \in L(G)$ est derive par G en exactement $2|w|-1$ etapes.

----------


### 2. Donnez la forme normale de Chomsky de la grammaire suivante, etape par etape.

$S \rightarrow aBBBCD | BC | A| BAB$

$A \rightarrow aAa | aba | Cbb$

$B \rightarrow BB|b|\epsilon$

$C \rightarrow S | BABAB$

$E \rightarrow a|b|bb|aa$

1. Epsilon dans B

On enleve $B \rightarrow \epsilon$

On ajoute

$B \rightarrow B$

$S \rightarrow C$

$S \rightarrow BA$

$S \rightarrow AB$

$S \rightarrow aBBCD$

$S \rightarrow aBCD$

$S \rightarrow aCD$

$C \rightarrow BABA$

$C \rightarrow ABAB$

$C \rightarrow BAAB$

$C \rightarrow BAA$

$C \rightarrow AAB$

$C \rightarrow ABA$

$C \rightarrow AA$

Nos regles a jour

---------

$S \rightarrow aBBBCD | BC | A| BAB | C | BA | AB | aBBCD | aCD$

$A \rightarrow aAa | aba | Cbb$

$B \rightarrow BB|b|B|$

$C \rightarrow S | BABAB | BABA | ABAB | BAAB | BAA | AAB | ABA | AA$

$E \rightarrow a|b|bb|aa$

-----------

2. Unitaire $S \rightarrow A$, $S \rightarrow C$, $B \rightarrow B$, $C \rightarrow s$

On ajoute

$S \rightarrow aAa | aba | Cbb$

$S \rightarrow BABAB | BABA | ABAB | BAAB | BAA | AAB | ABA | AA$

$C \rightarrow aBBBCD | BC | A| BAB | BA | AB | aBBCD | aCD | BABAB | BABA | ABAB | BAAB | BAA | AAB | ABA | AA$

On enleve 

$S \rightarrow A$, $S \rightarrow C$, $B \rightarrow B$, $C \rightarrow s$

Nos regles a jour

--------

$S \rightarrow aBBBCD | BC | A| BAB | BA | AB | aBBCD | aCD | aAa | aba | Cbb | BABAB | BABA | ABAB | BAAB | BAA | AAB | ABA | AA$

$A \rightarrow aAa | aba | Cbb$

$B \rightarrow BB|b|$

$C \rightarrow BABAB | BABA | ABAB | BAAB | BAA | AAB | ABA | AA | | aBBBCD | BC | A| BAB | BA | AB | aBBCD | aCD | BABAB | BABA | ABAB | BAAB | BAA | AAB | ABA | AA$

$E \rightarrow a|b|bb|aa $

--------------

3. Les finaux

On ajoute
$X_a \rightarrow a$

$X_B \rightarrow b$

Nos regles a jour

----

$S \rightarrow aBBBCD | BC | A| BAB | BA | AB | X_aBBCD | X_aCD | X_aAX_a | X_abX_a | CX_bX_b | BABAB | BABA | ABAB | BAAB | BAA | AAB | ABA | AA$

$A \rightarrow X_aAX_a | X_aX_bX_a | CX_bX_b$

$B \rightarrow BB|X_b$

$C \rightarrow BABAB | BABA | ABAB | BAAB | BAA | AAB | ABA | AA | aBBBCD | BC | A| BAB | BA | AB | aBBCD | aCD | BABAB | BABA | ABAB | BAAB | BAA | AAB | ABA | AA$

$E \rightarrow X_a|X_b|X_bX_b|X_aX_a$

$X_a \rightarrow a$

$X_B \rightarrow b$

-----

4. Decomposer les variacles trop longues

On ajoute

$F \rightarrow CD$

$G \rightarrow BA$

$H \rightarrow AB$

$I \rightarrow BB$

$J \rightarrow IF$

$K \rightarrow BJ$

$L \rightarrow X_aX_b$

$M \rightarrow X_bX_b$

$N \rightarrow GG$

Nos regles a jour

----

$S \rightarrow X_aK | BC | A| GB | BA | AB | X_aJ | X_aF | OX_a | LX_a | CM | NB | GG | HH | GH | GA | AH | AG | AA$

$A \rightarrow OX_a | LX_a | CM$

$B \rightarrow BB|b$

$C \rightarrow NB | GG | HH | GH | GA | AH | AG | AA | X_aK | BC | A| GB | BA | AB | X_aK | X_aF | NB | GG | HH | GH | GA | AH | AG | AA$

$E \rightarrow a|b|X_bX_b|X_aX_a |$

$X_a \rightarrow a$

$X_B \rightarrow b$

$F \rightarrow CD$

$G \rightarrow BA$

$H \rightarrow AB$

$I \rightarrow BB$

$J \rightarrow IF$

$K \rightarrow BJ$

$L \rightarrow X_aX_b$

$M \rightarrow X_bX_b$

$N \rightarrow GG$

$O \rightarrow X_aA$

-----

5. ??

6. On enleve les variables inutiles

$S \rightarrow X_aK | BC | A| GB | BA | AB | X_aJ | X_aF | OX_a | LX_a | CM | NB | GG | HH | GH | GA | AH | AG | AA$

$A \rightarrow OX_a | LX_a | CM$

$B \rightarrow BB|b$

$C \rightarrow NB | GG | HH | GH | GA | AH | AG | AA | X_aK | BC | A| GB | BA | AB | X_aF$

$E \rightarrow a|b|X_bX_b|X_aX_a |$

$X_a \rightarrow a$

$X_B \rightarrow b$

$F \rightarrow CD$

$G \rightarrow BA$

$H \rightarrow AB$

$I \rightarrow BB$

$J \rightarrow IF$

$K \rightarrow BJ$

$L \rightarrow X_aX_b$

$M \rightarrow X_bX_b$

$N \rightarrow GG$

On est sous la forme normale de Chomsky