# Devoir 2
## par Marc Laliberté matricule : 20085533 et Imane El Alaoui Najib matricule : 0917827

### No 1
Soit
$$ \Sigma = \{0,1\} $$
$$ 2 \leq k \in \N, i \in [k] \\ [k] = \{0,1,2,..., k-1\}$$
$$ L_{i,k} = \{w \in \Sigma^* : w \equiv i \bmod k\} $$
$$ L \in L_{reg} ? $$
Oui.
On peut construire un AFD

Preuve:
$$ M = (Q, \Sigma, \delta, s, F) \\ Q = \{q_0,\ q_1,\ ...,\ q_{k-1}\ \} \\ \Sigma : \{0,1\} \\ s : q_0 \\ F:\{q_i\}\\
 \delta :
\begin{cases}
   (q_n \times\ 0) \rightarrow q_{2n\ (\bmod k)} \\
   (q_n \times\ 1) \rightarrow q_{2n+1\ (\bmod k)} 
\end{cases} 

0 \leq n \leq k-1
$$


Donc, comme nous pouvous avoir un AFD qui reconnait le langage, il est regulier. 
$$ L_{i,k} \in L_{reg} $$

------

### No 2
Soit
$$ L = \{w \in \{a,b,c\}^* :\ |w|_a \equiv 2\bmod 3\ et\ |w|_b \equiv 2\bmod 4\} $$
voici le AFD

![no2](fsm_no2C.png)
$$ M = (Q, \Sigma, \delta, s, F) \\ Q = \{q_1,\ q_2,\ ... ,\ q_{12}\} \\ \Sigma = \{a,b,c\}\\ s=q_1 \\ F =\{q_{11}\} \\
\delta: (voir\ table\ plus\ bas)
$$

|           | a   | b   | c   |
|-----------|-----|-----|-----|
| q1(0a0b)  | q5  | q2  | q1  |
| q2(0a1b)  | q6  | q3  | q2  |
| q3(0a2b)  | q7  | q4  | q3  |
| q4(0a3b)  | q8  | q1  | q4  |
| q5(1a0b)  | q9  | q6  | q5  |
| q6(1a1b)  | q10 | q7  | q6  |
| q7(1a2b)  | q11 | q8  | q7  |
| q8(1a3b)  | q12 | q5  | q8  |
| q9(2a0b)  | q1  | q10 | q9  |
| q10(2a1b) | q2  | q11 | q10 |
| q11(2a2b) | q3  | q12 | q11 |
| q12(2a3b) | q4  | q9  | q12 |

Notons ici que pour alleger le tableau
$$ 0a0b \Rightarrow |w|_a = 0\bmod 3\ et\ |w|_b= 0\bmod 4$$
et ainsi de suite pour tous les *q*

Preuve formelle :

Prouvons que : 
$$ L \subseteq L(M) $$

$$ w \in L \\ |w|_a \equiv 2(\bmod 3)\ et\ |w|_b \equiv 2(\bmod 4)\ \\$$

Posons
$$ w = ux,\ u \in \Sigma^*,\ x\ \in \Sigma $$
$$ \hat \delta(s,w)= \hat \delta(s, ux) = \delta(\hat \delta (s,u),x) = q_{11} $$
Alors:
$$ i=\{1,2,3\}$$
$$ si\ x=c\ \rightarrow\ u \in L \\ et\ \delta_1(q_{11},c) = q_{11}\ \\ q_{11} \in F$$
$$si\ x=a \\ |u|_a \equiv 1(\bmod 3)\ |u|_b \equiv 2(\bmod 4) \\ \delta_2 (q,a) = q_{11} \Rightarrow q= q_7$$
$$si\ x=b \\  |u|_a \equiv 2(mod3)\ |u|_b \equiv 1(mod4) \\ \delta_3 (q,b) = q_{11} \Rightarrow q= q_{10} $$


Prouvons que
$$ L(M) \subseteq L  $$
$$ w \in L(M) \Rightarrow \exists q \in F \ tq \ \hat \delta(s,w) = q= q_{11} \in F\\ ssi\ |w|_a \equiv 2(\bmod3)\ |w|_b \equiv 2(\bmod4)$$

On a que 
$$ w \in L(M) \Rightarrow w \in L \\ w \in L \Rightarrow w \in L(M) $$
En conclusion : 
$$ L(M) \subseteq L \ et \ L \subseteq L(M) \Rightarrow L= L(M) $$ 




<!-- $$ L \subseteq L(M) $$
Si on a un mot dans L, ce mot la est aussi dans M
$$ w \in L \\ w= ua, u \in \Sigma \\ \hat \delta (s,w) = \hat \delta (s, ua) \\ 
\delta (\hat \delta(s,u),a)$$
aussi, de maniere analogue:
$$ w \in L \\ w= ub, u \in \Sigma \\ \hat \delta (s,w) = \hat \delta (s, ub) \\ 
\delta (\hat \delta(s,u),b)$$
De plus
$$ w \in L \\ w= uc, u \in \Sigma \\ |u|_a \equiv 2\bmod 3\ et\ |u|_b \equiv 2\bmod 4\ \\ \hat \delta (s,w) = \hat \delta (s, uc) \\ 
\delta (\hat \delta(s,u),c) =\hat \delta (s,w)$$ -->

<!-- Donc, si on a *c*, on ne change pas d'etat.

Prouvons que:
$$ L(M) \subseteq L $$  
$$ w \in L(M) \Rightarrow \exists q \in F \ tq \ \hat \delta(s,w) = q= q_{11}$$
posons
$$ w = ux,\ u \in \Sigma^*,\ x \in \Sigma $$

$$ q_{11} = \hat \delta(s,w) = \hat \delta(s,ux) = \delta (\hat \delta(s,u),x)  = q_{11}$$
par observation des deltas

$$ \delta_i(q,x) ,\ i=1,2,3\ :\ q_{11} \Rightarrow 
\begin{cases}
\delta_1(q,x) \Rightarrow x=b\ si\ |u|_a \equiv 2\bmod 3\ et\ |u|_b \equiv 1\bmod 4\ et\ q=\{q_{10}\}\\ 
\delta_2(q,x) \Rightarrow x = a\ si\ |u|_a \equiv 1\bmod 3\ et\ |u|_b \equiv 2\bmod 4\ et\ q=\{q_{7}\}\\ 
\delta_3(q,x) \Rightarrow x = c\ si\ |u|_a \equiv 2\bmod 3\ et\ |u|_b \equiv 2\bmod 4\ et\ q=\{q_{11}\}
\end{cases} $$
On a que 
$$ w \in L(M) \Rightarrow w \in L \\ w \in L \Rightarrow w \in L(M) $$
En conclusion : 
$$ L(M) \subseteq L \ et \ L \subseteq L(M) \Rightarrow L= L(M) $$ -->
