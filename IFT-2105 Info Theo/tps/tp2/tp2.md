# tp2
## devoir2
## no1
Soit
$$ \Sigma = \{0,1\} $$
$$ 2 \leq k \in \N, i \in [k] \\ [k] = \{0,1,2,..., k-1\}$$
$$ L_{i,k} = \{w \in \Sigma^* : w \equiv i \bmod k\} $$
$$ L \in L_{reg} ? $$

On va creer M_{i,k} tq L(Mik) = L_ik

$$ M_{i,k}  = (Q_{i,k} , \Sigma_{i,k} , \delta_{i,k} , s_{i,k} , F_{i,k} ) \\ Q_{i,k} =\{q_0, q_1, ... q_{k-1}\} 
\\ s_{i,k}  = q_0 \\ F=\{q_i\}
 \delta :
\begin{cases}
   (q_n \times\ 0) \rightarrow q_{2n\ (\bmod k)} \\
   (q_n \times\ 1) \rightarrow q_{2n+1\ (\bmod k)} 
\end{cases} 
$$
On veut 
$$ \{w \in \Sigma^*, \hat \delta_{ik}(q_o, w) = q_i\} = \{w \in \Sigma^*\ w \equiv i (modK) \} $$

Je prouve
$$ \forall w \in \Sigma^*,\ \hat \delta(q_0, w,) = q_{w mod k} $$
par induction

cas de base 
$$ w  = \epsilon \\ \hat \delta_{ik}(q_0,\epsilon) $$
epsilon vaut 0 en binaire 0(mod k)= 0

Supposons pour
$$ n \in \N, \forall w \in \Sigma^*\ ou |w|= n, \hat \delta (q_0, w) = q_{[w\ mod\ k]} $$

posons
$$ w = qk+r \\ q \leq q,r, r<k$$
$$ \hat \delta_{ik} (q_o, w) = \delta (\hat \delta(q_0, w),0) = q_{[2((w modk)modk)]} $$
donc w0 vaut 2w

$$ 2w (mod k) = 2(qk+r)mod\ k  = 2r\ modk \\ 2r(modk)=2(w(modk))(mod\ k)$$

propriete des mods
$$ a+b \bmod k = a (\bmod k) + b(\bmod k) = b \bmod k $$

autre etape

$$ \hat \delta_{ik} (q_0, w1) = \delta_{ik}(\hat \delta_{ik}(q_0, w), 1) = q_{[2(w(modk)+1(modk))]} $$
w1 vaut 2w+1

$$ 2w+1 (mod k) = 2(qk+r)+1(modk) = 2+2r+1(modk) - 2(w(modk))+1(modk) $$
si
$$ |w| = n+1 $$
ca fonctionne

donc si 
$$ w \in L <=> w(modk) = i <=> \hat \delta_{ik}(q_0, w) = q_i $$
en conclusion
$$ w \in L(M_{ik}) \\ donc\ L_{ik} \in L_{reg} $$

### No2
AFD on etait good

elle ecrit ses delta de maniere cool
$$  Q = \{ij |\ i \in [3], j \in [4] \} $$
$$ \delta(ij, a) = (i+1(mod3)j) \\ \delta(ij,b) = i(j+1(mod4)) \\ \delta(ij, c) = ij $$

la preuve va etre similaire au no1. 

------

# Lemme du pompiste

Soit un langage qui est regulier, alors
$$ \exists p >0 $$
tq
$$ \forall w \in L, |w| \geq p $$
$$ \exists x,y,z \in \Sigma^* $$
tq
$$ 1.\ xyz=w \\ 2.\ |xy| \leq p \\ 3.\ |y| > 0 \\ 4.\ \forall i \in \N, xyz \in L $$

cest les conditions du lemmme du pompiste.

le y cest le shit quon peut pomper tout en gardant le langage acceptant.

------

### L1

$$  L_1 = \{w \in \{a,b\}^*\ |\ |w|_a = 3 \bmod 4 \} $$

on peut prendre p =4 (on prend un p = nombre detats en general, si le langage est regulier)

prenons w_1 = aaa, |w_1| < p. 
tous les mots qui ont moins que p lettres, on peut les ignorer. 

prenons w_2 = aaba
ici x = aa, y = b , z = a. si y^1000, on reste dans le langage. 

prenons w_3 = aaaaaaa \in L
au moins p lettres, 
x = epsilon, y = a^4 (on fait boucler au complet) z= a^3

cest pas une genre de question dexamen, cest pour aider a comprendre lol

### L2
$$ L_2 = \{\{bab\} \in \{a,b\}\} $$

on cherche le mot bab

prenons p=5,
$$ w \in L \\ w = bab, \\ |bab|<5 $$
Donc par defaut, le lemme du pompiste est respecter

Est-ce quon peut se servir de ca pour prouver que nimporte quel langage est regulier. 

Regulier => Pompiste

mais

Pompise implique pas Regulier

si on veut la negation de Regulier => Pompiste

Donc Non Pompiste => Non Regulier. cest une contraposee. 

-------

### Contraposee pompiste
soit
$$ L \in \Sigma^* $$
tq
$$  \forall p>0 $$
$$ \exists w \in L, |w| \geq p,\ tq\ \forall x,y,z \in \Sigma^* $$
et
$$ 1.\ xyz=w \\ 2.\ |xy| \leq p \\ 3.\ |y| > 0 \\ 4.\ \forall i \in \N, xyz \in L $$
alors
$$ L \notin L_{reg} $$

Algorithme preuve : 
$$
a.\ poser\ p\ inconnu \\
b.\ choisir\ w \in L, |w| >= p \\
c.\ Considerer\ toutes\ les\ subdivisions\ w = xyz \\
d.\ Choisir\ un i \in \N, rq, xy^{i}z \notin L
$$

------

### L3
$$ L_3 = \{0^n1^{2n}\ |\ n \in N\} $$
MQ que L_3 not in L_reg
$$
a)\ poser\ p>0 \\
b)\ poser\ w = 0^p 1^{2p} \in L,\ |w| >= p \\
c)\ w = xyz\ |\ x= 0^m ,\ y=0^l,\ 0<l,\ m+l \leq p,\ z = 0^{p-l-m}1^{2p} \\
d)\ i=2: \ xy^iz = xy^2z = 0^m0^{2l}0^{p-l-m}1^{2p} = 0^{p+l}1{2p} \notin L\\
2(p+l) \neq 2p\ car\ l>0
$$
$$ L_3 \notin L_{reg} $$

------

### L4
$$ L_4 =\{w \in \{a,b\}^*: w\ est\ un\ palindrome\ (w=w^R)\} $$
prouvons que L_4 \notin L_reg
posons p>0
prenons 
<!-- $$ w = a^p \in L, |w| \geq p,\ (p \geq p) $$
Prenons xyz = w
$$ x=a^l,\ y=a^m,\ z=a^c,\ l+m \leq p, n >0\\ \forall i \in N,\ xy^iz = a^{p+(i-1)m}\in L $$ -->
$$ w = a^pba^p,\\ x=a^l,\ y=a^m,\ z=a^c,\ l+m \leq p, n >0\\ $$

pour
$$ i=0,\ xy^iz = xz = a^{p-m}ba^p \notin L \\ xz^R = a^pba^{p-m} \neq xz$$
donc
$$ L_4 \notin L_{reg} $$

-----

### L5
$$ L_5 = \{a^mb^n\ |\ m \leq n\} $$
mq l_5 notin l_reg

Supposons que L in Lreg

poson p>0, la constante de pompage

posons w= a^pb^2p
soit
$$ xyz \in \Sigma^*, |xy| \leq p,\ et\ |y|>0 \\ x=a^l,\ y = a^m,\ l+m \leq p, m>0,\ z = a^{p-l-m}b^{2p} $$

posons 
$$
i = 2p+1 \\ xy^iz = a^la^{m(2p+1)}a^{p-l-m}b^{2p}  =a^{p-m+2pm+m}b^{2p} = a^{p+2pm}b^{2p}\\
2p \leq 2pm \Rightarrow 2p<2pm+p \\
donc\ a^{p+2pm}b^{2p} \notin L
$$
Ici il fallait prendre un i assez grand sinon ca aurait pas marcher

-----
### L6
$$  L_6 =\{K^{2^n}\ |\ n \in \N \} $$
le nombre de lettres est une puissance de 2

mq L6 notin L_reg

Supposons que L in Lreg

Alors il exist p>0, la constante de pompage

Posons
$$ w = k^{2^p} \in L \\ |w| \geq p <=> 2^p \geq p $$

sous-lemme par induction
$$ base:\ 2^0 = 1 > 0  \\ supp\ 2^n > n \geq 1 \\ 2^{n+1} = 2*2^n = 2^n + 2^n > n+n \geq n+1 \\ 2^{n+1} > n+1$$

supposons que w = xyz et |y| = m et m <= p et m>0

posons i=2

$$  xy^iz = K^{2^p +m} \notin L  \\ 2^p + m \neq 2^q \forall q \notin \N $$
si
$$ q \leq p : 2^p +m > 2^p \geq 2^q ,\ m>0  $$
si 

$$ q \geq p+1 : 2^p +m  \leq 2^p +p < 2^p + 2^p = 2*2^p = 2^{p+1} \\ 2^p +m\ pas\ une\ puissance\ de\ 2$$
$$ K^{2^p+m} \notin L \Rightarrow L \not L_{reg} $$

