# tp5

## Algorithme pour trouver les automates minimaux (minimisation des automates)

Soit ADF
$$ M = (Q, \Sigma, \delta, s, F) $$

Deux etats q_1 et q_2 de M sont distinct si (un est acceptant etre laurte non)
$$ \exists z \in \Sigma^*\ tq \\ \hat \delta(q_1, z) \in F \\ \hat \delta(q_2, x) \notin F $$
Par recurrence q_1 et q_2 distincts si
$$ q_1 \in F\ mais\\ q_2 \notin F $$
sans perte de generalite

cas z = epsilon
$$ q_1\ et\ q_2\ disincts\\si\ \exists q \in \Sigma : \delta(q_1, a)\ et\ \delta(q_2, a)\ distincts $$

Les etats de Q qui ne sont pas distincts peuvent etre combiner. (on en choisi un des deux et on l'enleve)

Algorithme de minimisation:

On initialise un tableau vide 
$$ T_{|Q|\times |Q|}\\ Q = q_1, q_2, ... q_n $$

T : 
|       |  qn   | qn-1  |  ...  |  q1   |
| :---: | :---: | :---: | :---: | :---: |
|  q1   |       |       |       |       |
|  q2   |       |       |       |       |
|  ...  |       |       |       |       |
|  qn   |       |       |       |       |

1. passer par chaqu case (qi,qi) de T. Si qi in F mais qj not in F (ou vice versa), ecrire 0,Epsilon, dans la case (qi, qi)
2. Tant que quelque chose change (on ajotue de linformation) : passer par chaque case (qi, qj) vide
$$ \forall a \in \Sigma,\ si\ (\delta(q_i, a), \delta(q_j, a))= k,u \\ ecrire\ k+1,a\ dans\ la\ case\ (q_i, q_j)$$

----

Exemple :

![afd1](afd1.svg)

$$ \Sigma = \{a,b\} $$

T : 
|       |     8     |     7     |     6     |     5     |     4     |   3   |   2   |   1   |
| :---: | :-------: | :-------: | :-------: | :-------: | :-------: | :---: | :---: | :---: |
|   1   |    2,a    | 0,epsilon |    2,a    |    1,b    | 0,epsilon |  2,a  |  1,a  |       |
|   2   |    1,a    | 0,epsilon |    1,a    |    1,a    | 0,epsilon |  1,a  |       |       |
|   3   |           | 0,epsilon |    2,a    |    1,b    | 0,epsilon |       |       |
|   4   | 0,epsilon |    1,a    | 0,epsilon | 0,epsilon |           |       |       |
|   5   |    1,b    | 0,epsilon |    1,b    |           |           |       |       |
|   6   |    2,a    | 0,epsilon |           |           |           |       |       |
|   7   | 0,epsilon |           |           |           |           |       |       |
|   8   |           |           |           |           |           |       |       |

On pourra jamais remplir le (3,8), cest donc le meme etat (poubelle en loccurence), on na jamais beosin de deux etats poubelle.

![afd2](afd2.svg)

---------

$$ L \in L_{reg} <=>\ R_L\ a\ un\ nombre\ fini\ de\ classes $$

$$ L = \{w\ commence\ par\ 1\ et\ fini\ par\ 0 \} \subseteq \{0,1\}^*$$
est-ce que L est regulier?

Preuve :

Soient les classes
$$ [\Epsilon], [1], [0], [10] $$

Soit
$$ w \in \Sigma^* $$

$$ a)\ si\ w = \epsilon,\ w \in [\epsilon],\ \epsilon R_L \epsilon\\ 
b)\ w= 0x,\ x \in \Sigma^*,\ donc w \in [0] \\◊
soit\ z \in \Sigma^*,\ wz = 0a_1a_2,.... \notin L,\ 0z = 0.... \notin L\ wR_Lz0\ w \in [0]\\
c)\ w= 1x, x \in \Sigma^* \\ i)\ w\ finit\ par\ 0\ donc\ w \in [10]\\
soit\ z \in \Sigma^*,\\ 
si\ z = \epsilon : wz= w= 1...0 \in L,\ 10z = 10 \in L \\
si\ z\ finit\ par\ 0: wz = 1...0..0 \in L,\ 10z = 10...0 \in L \\
si z\ finit\ par\ 1 : wz = 1...0..1 \notin L,\ 10z = 10..1 \notin L \\
donc\ wR_L10\ et\ w \in [10]\\
ii)\ w\ finit\ par\ 1,\ donc\ w \in [1],\ soit z \in \Sigma^* \\
si\ z=\epsilon: wz = 1..1 \notin L,\ 1z = 1 \notin L \\
ai\ z\ finit\ par\ 0:\ wz:1...1..0 \in L\ et\ 1z \in L \\ 
si\ z\ fini\ par\ 1:\ wz: 1....1 \notin L\ et\ 1z: 1...1 \notin  L\\
donc\ wR_L1\ et\ w \in [1] 
 $$

$$ w \in \Sigma^* <=>w\ fait\ partie\ d'une\ des\ 4\ classe\ de\ R_L \Rightarrow L \in L_{reg}\ par\ M-N$$

-------
## Correction devoir 5

### No1
Par le pompiste, MQ
$$ L= \{ww: w \in \{a,b,c,d\}^*\} \notin L_{reg} $$

Supposons que L est regulier, Alors, il existe p>0 tq L respecte le lemme du pompiste.

Soit
$$ w \in L,\ |w|>p:\ a^pca^pc=w $$

soient
$$ x,y,x \in \Sigma^* $$
tels que
$$ w= xyz,\ |xy| \leq p,\ |y| >0 $$
Posons k et l , tq
$$ a = a^k,\ a^l,\ z = a^{p-k-l}ca^pc \\ et\ k+l \leq p,\ l>0$$

Soit i =3
$$ xy^iz = xy^3z = a^ka^{3l}a^{p-k-l}ca^pc = a^{p+2l}ca^pc \notin L$$

car si 
$$ xy^iz = uv\ |u|=|v|, u \neq v $$
car u n'as pas de c
donc L ne respece pas le lemme du pompiste, n'est donc pas regulier

### no2
Donnez une RegEx pour le langage
$$ \{w \in \{a,b\}: |w|_{ab} = |w|_{ba}\} $$
ou w commence et fini par la meme lettre.
Soit
$$ \Sigma = (a+b) $$

on etait good yahoo

$$ (a\Sigma^* a)+(b\Sigma^*b)+(a+b+\epsilon) $$

un autre
$$ \epsilon + aa^*(bb^*aa^*)^* +  bb^*(aa^*bb^*)^*$$

### No3

Prouvez que le langage L
$$ \{w \in \{a,b,c,d,e\}^*: |w|_{ab} = |w|_{ba}\} \notin L_{reg}$$

soit R_L la relation dequivalence sur sigma^* tq
$$ xR_Ly\ ssi\ \forall z \in \Sigma^*,\ xz \in L,\ ssi\ yz \in L $$

Soient
$$ i \neq j \in \N\ et\ [(abc)^i]_{R_L}\ et\ [(abc)^j]_{R_L}  $$

Soit
$$ z=(bac)^i \\(abc^i)z = (abc)^i(bac)^i \in L,\ (abc)^iz \notin L\ (abc)^j(bac)^i \notin L\ car\ i \neq j$$

donc pour tout k \in N [(abc)^k] est distincte (differentes des autres), donc il y a un infinite de classes, Par M-H, le langage nest pas regulier

-------------

## Sujets a lexamen
1. AFD -> difference entre delta et hat delta, minimisation
2. AFN -> delta, hat delta, E(q)
3. L_reg -> fermeture (union, intersection, concatenation, kleene^*, inverse)
4. AFN -> afd (elle est pas sur)
5. RegEx et AFD vers regex
6. Myhill-Nerode (pour regulier ou non) reletion dequivalences, raffinement
7. Pompiste

--------
### AFN -> afd 

![afd3](afd3.png)

on a 3 etats ici

$$ AFD= 2^3 etats= 8 \\ \{0,1\}, \{0,2\},\ \{1,2\},\ \{0,1,2\},\ \emptyset,\ \{0\},\ \{1\},\ \{2\} $$
$$ E(0) =\{0,1\}=s  $$

Tous les etats qui ont dedans vont etre acceptants

![afd4](afd4.png)

pour minimiser : on eleve tout dans lequel on arrive jamais

![afd5](afd5.png)

### afd -> regex

![afd6](afd6.png)
$$ R_{ij}^{k+1}= R_{ij}^{k}+ R_{ik+1}^{k}(R_{k+1}^{kk})^*R_{k=1j}^{k} $$

k=-1

|  ij   |   R_ij^k    |
| :---: | :---------: |
|  00   |   epsilon   |
|  01   |     a+b     |
|  10   |      b      |
|  11   | epsilon + a |


k=0
|  ij   |                 R_ij^0                  |
| :---: | :-------------------------------------: |
|  00   | R_00^-1 + R_00^-1(R_00^-1)*R^-1_00 = x1 |
|  01   |                   x2                    |
|  10   |                   x3                    |
|  11   |                   x4                    |

$$ x1 = \epsilon + \epsilon \cdot \epsilon^* \cdot \epsilon = \epsilon $$
x1 est le regex recherche dans le probleme
$$ x2 = a+b+\epsilon \cdot \epsilon^* \cdot (a+b) = (a+b) $$
$$ x3 = b +b\epsilon = b $$
$$ R^{-1}_{11}+ R_{10}^{-1}(R_{00}^{-1})^*R_{01}^{-1} = \epsilon + a + b \cdot \epsilon(a+b) = \epsilon + a +b(a+b) $$

k+1 = 1
|  ij   | R_ij^0 |
| :---: | :----: |
|  00   |   y1   |
|  01   |   y2   |
|  10   |   y3   |
|  11   |   y4   |
$$ y1 = R_{00}^0 + R_{01}^0(R_{11}^0)^*R_{10}^0 = \epsilon + (a+b)(\epsilon + a + b(a+b))^* $$
$$ y2 = R_{01}^0 + R_{01}^1(R_{11}^0)^*R_{11}^0 = (a+b)+(a+b)(\epsilon + a + b(a+b))^*(\epsilon + a + b (a+b)) $$


--------

### Revision myhill-nerode
MQ
$$ L = \{w \in \{0,1\}: w \neq 11\ et\ w \neq 111\} \in L_{reg}$$

nos classes dequivalences
$$ [\epsilon ,\ [0],\ [11],\ [1],\ [111] $$
epsilon nest pas en relation avec 0 donc
$$ [\epsilon] \neq [0] $$

Je veux que 
$$ w \in \Sigma^* \Rightarrow wR_L\epsilon\ ou\ wR_L1\ ou\ wR_L0 $$
Soit
$$ w \in \Sigma^* $$
si
$$ w = \epsilon,\ w \in [\epsilon] $$
si
$$ w = 1, w \in [1] $$
si
$$ w = 11, w \in [11] $$
si
$$ w = 111, w \in [111] $$
Sinon, alors soit
$$ |w|>3 $$
soit
$$ |w|_0 >0 $$
Je dis que $w \in [0]$

soit
$$ z \in \Sigma^*\\ |wz|>3\ ou |wz|_0 >0 $$
donc wz in L car
$$ 11 \neq wz \neq 111\ donc \\ 0z \neq 11,\ 0z \neq 111$$
donc
$$ wz \in L <=> 0z \in L $$
donc
$$ wR_L0\ et\ w \in [0] $$
Donc
$$ w \in \Sigma^* \Rightarrow $$ 
w est dans une des classes dequivalences

donc mes classes partionnement \sigma^* et raffine R_L, donc R_L a un nombre fini de classes dequivalences et L est regulier

rappel si R' raffine R, #classes dans R' >= #R