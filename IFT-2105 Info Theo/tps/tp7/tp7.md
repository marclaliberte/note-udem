# Correction Dev 6

Utilisez Myhill-Nerode pour prouver que

---------

### No1
$$ \{a^nb^m:\ pgcd(n,m)=1\}\subseteq \{a,b,c,d\}^*\notin L_{reg} $$

Soit $R_L$, la relation d'equivalence sur $\Sigma^*$ telle que
$$ xR_Ly\ ssi\ \forall z \in \Sigma^*,\ xz \in L,\ ssi\ yz \in L $$

soit
$$ p_1, p_2... (\{p_n\}_{n \in \N}) $$
une suite infini de nombre premiers strictement croissante.

Soient pi pj pour i et j dans les N et i != j

Soient
$$ [a^{p_i}]\ et\ [a^{p_j}] $$

je veux quelles ne soient pas en relation entre elles. 

Soit 
$$ z = b^{p_i}  \\ a^{p_i}z =a^{p_i}b^{p_i} \notin L $$

car pgcd(pi, pi) = pi

$$ a^{p_j}b^{p_i} \in L$$
par pgcd(pi,pj) car pi,pj sont des premiers distincts. Donc. a^pi nest pas en relation avec a^pj
donc
$$ [a^{p_j}] \neq [a^{p_i}] $$
Donc RL est infini car infinites de nombres premiers. DOnc le langages nest pas regulier.

-----------------

### No2
$$ \{w \in \{0,1\}:\ w \equiv i \bmod k\} \in L_{reg} \\ pour\ 2 \leq k \in \N\\ i \in [k] $$

Soient [0], [1], [10], ..., [k-1]

Soit
$$ w \in \Sigma^* $$

si |w| = 0, w = epsilon. w mod k = 0. $w \in [0]$
Soit $z \in \Sigma^*$ ,si z mod k = j, alors 0z mod k = j, donc
$$ \epsilon z \in L \Rightarrow 0z \in L \\ \epsilon \in [0] $$

Supposons que pour |u| = n,
$$  u \in [u mod k = j] $$

soit w = u0
$$ uR_Lj \Rightarrow u0 R_L j0 $$

donc je veux que j0 soit dans la classe [j0 mod k]. Donc je peux voir que  [2j mod k] existe, je nen ai que k a verifier. 

Idem pour w = u1. 

En conclusion, 
$$  w \in \Sigma^* \Rightarrow w\ est\ dans\ une\ de\ mes\ classes \Rightarrow $$
RL est fini (k) donc le langage est regulier.

# Devoir 7

Prouvez que tout langage regulier est hors-contexte.

Soit L un langage regulier. 

Soit un AFD
$$ M = (Q, \Sigma, \delta, s, F) $$
qui reconnait L(M) = L

Soit
$$ G=(V, \Sigma, R, S) $$
une ghc

posons
$$ V = \{A_q; q \in Q\}  \\ S = A_s\\ \Sigma = \Sigma \\ $$

Soit
$$ \delta(q,a) = p,\ a \in \Sigma, q,p \in Q $$
On ajoue a R, la regle
$$ A_q \rightarrow qAp $$

Si
$$ p \in F $$

on ajoute
$$ A_p \rightarrow \epsilon $$

Maintenant la preuve

on veut L(M) = L(G) = L

Commencons par
$$ L(M) \subseteq L(G) $$

Soit
$$ w \in L(M) $$
donc
$$ \hat \delta(s, w) = p \in F $$

$$ w = w_1, w_2, ... w_n $$
Donc on a 
$$ s, a_1, a_2,..., a_{n-1}, p $$
where
$$ \delta(s,w_1) = a_1 \\ \delta(a_1, w_2) = a_2 \\ ... \\ \delta(q_{n-1}, w_n)= p $$

Donc R contient
$$ S \rightarrow w_1Aa_1 \\ A_{q_1} \rightarrow w_2AQ_2 \\ ... \\ A_{q_{n-1}} \rightarrow w_nA_p,\ et\ A_p \rightarrow \epsilon $$

On veut que 
$$ S \Rightarrow^* w $$

$$ S \Rightarrow w_1Aq_1 \Rightarrow w_1w_2Aq_2 \Rightarrow^*... \Rightarrow wA_p \Rightarrow w\epsilon = w $$

Donc $w \in L(G)$

Ensuite
$$ L(G) \subseteq L(M) :\ S \Rightarrow^* w $$
Dans 2-3 semaines lol


--------

LE vrai tp wohoo

GHC
$$ G=(V, \Sigma, R, S) $$

V : variables
$$ V \cap \Sigma = \emptyset $$
Sigma : lettres
$$ S \in V $$
$$ R \subseteq V \times (V\cup \Sigma)^* \\ v \rightarrow \alpha \in (V \in \Sigma)^*$$

$$ L(G)= \{w:S, w\ in \Sigma^*  \Rightarrow^* w \} $$

ex: 
g1
$$ S \rightarrow A | Ab | \epsilon \\ A \rightarrow A | a $$
donc
$$ S \Rightarrow Ab \Rightarrow Ab \Rightarrow ab $$

des exemples plus pertinents
$$ L_2= \{w \in \{a,b\}^*: |w|\equiv 1\bmod 2\} $$

$$ G_2=(V, \Sigma, R, S) $$

R=
$$ S \rightarrow A \\ A \rightarrow a | b | AAA $$
$$ V = \{S,A\} $$

mettons que
$$ |w| = 2k+1 \\ S \Rightarrow A \Rightarrow AAA \Rightarrow .... \Rightarrow AA...A \\ S \Rightarrow^{k+1} AAA...A \Rightarrow^{2k+1}w $$
Donc 
$$ S \Rightarrow^{3k+2} w $$

------

$$ L_3 = \{w \in \{a,b,c\}^*: w = w^R\ et\ aa|bb|cc \notin w\} $$

R=
$$ S \rightarrow aAa| bBb | cCc | \epsilon | a | b | c \\ A \rightarrow bBb | cCc | b | c \\ B \rightarrow aAa| bBb | a | c \\ C \rightarrow aAa| bBb | a | b$$

$$ V = \{S,A,B, C\}  \\ \Sigma = \{a,b,c\} \\ S=S$$

------

$$ L_4 =\{w \in \{a,b\}: |w|_{ab} = |w|_{ba}\} $$
R = 
$$ S \rightarrow aAa | bAb | a| b | \epsilon \\ A \rightarrow aA | bA | \epsilon $$



-------

$$ L_5 = \{w \in \{a,b\}: |w|_a = |w|_b\} $$

quand le langage est pas regulier, cest toff faire la grammaire. 
R = 
$$ S \rightarrow \epsilon | aSb | bSa| abS| baS | Sab | Sba | SS $$

cetait un essaie en classe, cest pas facile comme on voit, voici la vrai reponse

$$ S \rightarrow \epsilon | aSbS | bSaS $$

alizee dit cest pas mal important cette grammaire quand on veut autant quelque chsoe que quelque chose

--------

$$ Francais=(V, \Sigma, R, S) \\ \Sigma =\{mots\ du\ francais, ponctuation\}$$

R = 
$$ S = phrase\\ phrase \rightarrow phrase\ conjonction\ phrase. | groupeSujet\ groupeVerbe. \\ groupeSujet \rightarrow pronom| determinant\ _ \ nom | prenom\\
groupeVerbe \rightarrow verbe_adverbe \\ adverbe \rightarrow \epsilon | beaucoup
 $$
 $$ phrase \rightarrow phrase\ conjonction\ phrase $$
 cette partie fait en sorte que cest ambigu