# Tp4
## Correction dev4
### No1
on avait oublier quelques transitions. 
### No2
$$ L = \{w \in \{0,1\}\} $$
tout sous mot 00 apparait avant 11

$$ 0^*(100^*)^* 1^*(01 1^*)^*(0 + \epsilon) $$

### retour sur dev3 no3

$$ \{w \in \{a,b\}: |w|_{ab} \equiv |w|_{ba}\} $$
w commence et fini par la meme lettre.

| $\delta$ |   a   |   b   |
| :----: | :---: | :---: |
|   0    |   1   |   3   |
|   1    |   1   |   2   |
|   2    |   1   |   2   |
|   3    |   4   |   3   |
|   4    |   4   |   3   |


$$ \hat \delta(0,w) \in F <=> w \in L(M) $$
w commence par a :
$$ \hat \delta (0,w) \in \{1,2\} $$
w commence par b
$$ \hat \delta(0,w) \in \{3,4\} $$
$$ \hat \delta (0,w) = 0 <=> w = \epsilon $$
$$ \hat \delta (0,w) \in \{1,4\} <=>w\ termine\ par\ a \\  \hat \delta (0,w) \in \{2,3\} <=>w\ termine\ par\ b \\  $$
$$ \hat \delta (0,w) = 1 <=> w\ commence\ et\ fini\ par\ a\\ \hat \delta (0,w) = 3 <=> w\ commence\ et\ fini\ par\ b \\ w \in L \\ L(M) \subseteq L$$

------------

Montrer que si L est regulier, alors  (L)^* est regulier
$$ L \in L_{reg} \Rightarrow \exists AFD\ M $$
tq L(M) = L et m = blablaba

Soit M', on peut que L(M') = L^*

On fait un M random avec des etats acceptants, pour M', on rajouter un etat de base s' (et il est acceptant) a M et une transition epsilon vers letat acceptant de M. et de tous les etats acceptants de M, on a une transition epsilon vers letat acceptant de M. fak M' cest tous les etats de M plus s'
$$ L^* = \Cup_{n \in \N} L^n $$
$$ w \in L^* \Rightarrow \exists n \in \N,\ w \in L^n $$
$$ M' = (Q \cup \{s'\}, \Sigma, \delta', s', F \cup \{s'\}) $$

cest un lemme quon peut utilsier en examen....

-------------

Myhill - Nerode :
$$  L \in L_{reg} <=> $$
L est la reunion finie de certaines classes dequivalences de R_l

soit 
$$ x,y \in \Sigma^* \\ xR_Ly <=> \forall z \in \Sigma^*,\ (xz \in L\ ssi\ yz\ \in L) $$

si on veut prouver que 2 elements ont la meme relation dequivalence, on doit prouver que peu importe le mot quon leur rajouter, les 2 elements se comportent de la meme maniere, ie vont dans le meme etat ou mieux encore parte du meme etat et les 2 vont vers le meme etat.

disons :
$$ z = \epsilon :\ xR_Ly <=> (x \in L\ ssi\ y \in L) $$

--------

MQ avec Myhill - Nerode 
$$ L = \{0^m 1^n: n \neq m\} \notin L_{reg} $$
prenons
$$ 0^i, i \in N,\ 0^i1^i \notin L,$$
mais si j != i
$$ 0^i1^j \in L $$
$[0^i]_{R_l}$ = est une classe dequivalence differente de $R_L \forall i \in \N$
Si 
$$ i \neq j \\ [0^i] \neq [0^j] $$
car pour 
$$ z = 1^i,\ 0^iz \notin L\ mais\\ 0^jz \in L\\ 0^j1^i \in L\\ [0^i] \neq [0^j] $$
Il y une classe par i dans N et N est infini, donc infini de classes, Donc L n'est pas un langage regulier par M-N.

-------------

Trouver les expression regulieres
$$ \Sigma = \{0,1\} $$
- L1 = w commence par 1 et fini par 0
- L2 = la 3e lettre de w est 0
- L3 = w commence par 1 ou 001
- L4 = w est de longueure paire et commence par 1
- L5 = le nombre de a = 1 mod3
- L6 = |w|_1 > 3
- L7 = w != 01 et w != 1
- L8 = w ne contient pas le sous-mot 001

Dans le cours on a le droit a
1. emptyset
2. epsilon
3. a \in \Sigma
4. +
5. .
6. *

on peut pas faire ?, mais on peut faire (epsilon + R1)
pour R1^+ = (R1)(R1)^*

disons quon a
$$ 00^*(1+\epsilon)\emptyset $$
ca donne
$$ \emptyset $$

posons
$$ \Sigma = (0+1) $$

$$ L_1 = 1(1+0)^*0  \\ 1 \Sigma^* 0$$
$$ L_2 = \Sigma \Sigma 0 \Sigma^*  $$
$$ L_3 = (1+001) \Sigma^* = (1\Sigma^*) + (001\Sigma^*) $$
les 2 sont equivalents ici
$$ L_4 = 1\Sigma^{2n+1} = 1 \Sigma(\Sigma\Sigma)^* $$
ici on peut pas vraiment faire 2n+1
$$ L_5 = b^* ab^*(ab^*ab^*ab^*)^*$$
$$ L_6 = 0^*10^*10^*1\Sigma^* $$
$$ L_7 =  \epsilon + 0 + 1 + 11 + 00 + 10 + \Sigma\Sigma\Sigma\Sigma^*$$

POur L8 faut faire lautomate (3 etats assez basic)

$$ s \rightarrow s: (1^* + 1^*+ 01)^* = (1+01)^* \Rightarrow \hat \delta(s,w) = s $$
$$ s \rightarrow a: (1+01)^* 0 $$
$$ s \rightarrow b: (1+01)^* 000^* $$
Le regex au complet cest lunion des 3
$$  (1+01)^*(\epsilon + 0 + 000^*)$$