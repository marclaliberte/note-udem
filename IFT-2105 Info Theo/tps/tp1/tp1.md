# TP1

Alizee
email : alizee.gagnon@umontreal.ca
jeudi : 16h-18h
local a cote de lasso AA-3189

## correction devoir 1

1. Donner 2 langages, un fini et un infini

$$ \Sigma = \{a,b,c\} \\ L_f=\{a, ab, \epsilon, bbbb, c\} \subseteq \Sigma^*$$

$$ L_i = \{a^n, n \in \N\} \subseteq \Sigma^*$$
pour justifier que cest infini, prendre un ensemble quon sait qui est infini et montrer quils ont la meme longueure.

2. Montrer que
   $$ L^* = (L^*)^* $$
   et est-ce que ca depend de l'alphabet
   $$ L^* \subseteq (L^*)^*  \\ (L^*)^* = \cup (L^*)^k = (L^*)^0 \cup (L^*)^1 \cup (L^*)^2 \equiv L^*$$
   $$ (L^*)^* \subseteq L^* \\ MQ\ : w \in (L^*)^* \Rightarrow w \in L^* $$
   soit

$$
 w = w_{1,1}\ ...\ w_{1,k_1}\ ...\ w_{k,1}\ ...\ w_{k,k_k} \in L^* \\
 v_{ij} \in L \\
m = \sum_{j=i}^k m_j \\
w \in L^m \subseteq L^*
$$

et ce peu importe l'alphabet.

## Des fois, les devoirs peuvent sa rammasser a lexamen.

alphabet : un ensemble fini non vide de symbole
un langage est un sous-semble de sigma*
\$\$ L \subseteq \Sigma^* \$\$

---

UN ADF
$$ M = (Q, \Sigma, \delta, s, F)  $$

- Q est un ensemble fini d'etats
- Sigma est l'alphabet
- delta est une fonction de transition
  $$ \delta(q,x) = p \\ \delta:Qx\Sigma \rightarrow Q $$
- s est l'etat initial
- F est une ensemble d'etat aceptants

---

$$ L_1 = \{w \in \{0,1\}^* : w\ se \ termine \ par 0 \} \\ M_1\ tq\ L(M) = L_1$$

**notons que F est l'ensemble de etats acceptants**

M_1

![L1](../graphes/L1_tp1.png)

$$
 \delta_1 (s,0) = q_0$$

|delta1 |  0  | 1 | 
| ----- |  -- |---|
|s      | q_0 | s |
|q_0    | q_0 | s |

$$ L(M) = \{w \Sigma^* : \hat \delta(s,w) \in F \} $$
Il faut montrer que la machine comprend le bon langage donc que L_1 = langage de M_1

$$ L_1 \subseteq L(M_1) $$
Si on a un mot dans L_1, ce mot la est aussi dans M_1
$$ w \in L_1 \Rightarrow w= u0, u \in \Sigma^* \\ \hat \delta (s,w) = \hat \delta (s, u0) \\ 
\delta (\hat \delta(s,u),0) = q_0 \in F$$
dans le dernnier delta chaepeau, cela inclus tous les etats dans Q
$$ \Rightarrow w \in L(M_1) $$
premiere partie de faite, faut faire lautre
$$ L(M_1) \subseteq L_1 \\ w \in L(M_1) \Rightarrow \exists q \in F \ tq \ \hat \delta(s,w) = q= q_0$$
posons
$$ w = ux,\ u \in \Sigma^*,\ x \in \Sigma $$
notons que 
$$ w \ncong \epsilon 
\ car\ \hat \delta (s,\epsilon) = s \notin F$$
donc a le droit disont un lettre a la fin
$$ q_0 = \hat \delta(s,w) = \hat \delta(s,ux) = \delta (\hat \delta(s,u),x)  = q_0$$
par observation de delta_1
$$ \delta_1 (q,x) = q_0 \Rightarrow x = 0 \\ w = ux = u0 \Rightarrow w \in L_1$$
On a que 
$$ w \in L(M_1) \Rightarrow w \in L_1 \\ w \in L_1 \Rightarrow w \in L(M_1) $$
En conclusion : 
$$ L(M_1) \subseteq L_1 \ et \ L_1 \subseteq L(M_1) \Rightarrow L_1 = L(M_1) $$

le plus important 
$$ \hat \delta (q, uv) = \hat \delta(\hat \delta (q,u), v) $$

----------

soit M_2 = 

![l2](../graphes/L2_tp1.png)

Quest-ce que L(M)?
cest les mots de longueures impaires avec des etats de plus pour nous meler. 
$$ L(M_2) = \{ w \in \{a,b\}^* |\ |w|mod2 =1\} $$
-----

$$ L_3 = \{etoile, coeur, etoile, smiley\} \\ \Sigma=\{coeur, smiley, fleur, etoile\}$$
p est un etat poubelle
FSM : 
![L3](../graphes/L3_tp1.png)

------
$$ L_4= \{w \in \{a,b,c\}: w\ se\ termine\ par\ aba\} $$

FSM:
![L4](../graphes/L4_tp1.png)

-----

$$ L_5=\{(bba)^n : n \in \N\} \\ \epsilon \in L_5$$
![L5](../graphes/L5_tp1.png)

------

$$ L_6 = \{w \in \{a,b,c\} : |w|\geq 5\} $$
![L6](../graphes/L6_tp1.png)

------

$$ L_5 \cap L_6 = \{(bba)^n : n \geq 2\} $$
![l5l6](../graphes/l5*l6_tp1.png)

-----

$$ L_7 = \{w \in \{a,b,c\}: w\ ne\ contient\ pas\ ccc\} $$
on commence par trouver le complement de L7 et on inverse tous les etats
$$ \Sigma^* \setminus L : F^c = Q \setminus F $$

![L7](../graphes/L7_tp1.png)