#TP 9 FNC une fois pour toute

Passer de G une, grammaire vers G' telle que L(G) = L(G') et G' 
- n'a pas de variables inutiles
- pas de regles $A \rightarrow \epsilon$ sauf $S' \rightarrow \epsilon$
- a des regles de la forme $A \rightarrow BC$ ou $A \rightarrow a$ avec $A,B,C \in V', a \in \Sigma$ et $S' \neq B,C$


## Algo de Hopcroft-Ullman

1. Epsilon Trouver $E = \{A \in V, A \Rightarrow \epsilon\}$

$E_0 = \{A \rightarrow \epsilon\}$, $E_1 =\{B \rightarrow A, A \in E_0\} \cup E_0, E = E_n, n \rightarrow \infty$

$\forall A \in E$ si on a une regle $B \rightarrow \alpha$ ou $A \in \alpha$ (retirer A de alpha de toutes le facons possible pour trouver de nouvelles regles).

 On peut ensuite enlever les regles epsilon $C \rightarrow \epsilon, C \in V$

2. Unitaire : Enlever toutes les regles unitaires. Pour toutes regles $A \rightarrow B$, creer $V_B=\{C \in V : C \Rightarrow^* B\}$ la series de regle de C vers b sont toutes des regles unitaires. Pour $C \in V_B$, pour toutes regles $B \rightarrow \alpha$, ajouter la regle $C \rightarrow \alpha$

3. Lettres Pour chaque $a \in \Sigma$, ajouter $X_a$ aux variables, ajouter la regle $X_a \rightarrow a$. Remplacer a par $X_a$ dans les regles, sauf si la regle est de forme $A\rightarrow a$

4. Decomposer les regles trop longues en ajoutant des $Y_i$, $A \rightarrow BCD$ de vient $A \rightarrow BY_1, Y_1 \rightarrow CD$

5. Ajoute rune nouvelle variable initiale $\hat S$ et les regles $\hat S \rightarrow \alpha$ si $S \rightarrow \alpha$, Si $\epsilon \in L(G)$ ajourter $\hat S \rightarrow \epsilon$

6. Enlever les variables inutiles. 

  6.1 on veut $A \Rightarrow^* w, w \in \Sigma^*$, on prend $U_0 = \{\epsilon\}\cup \Sigma$ $U =\{A \in \hat V : A \rightarrow \alpha, \alpha \in U_0^*\}$ $U_{n+1} = \{A \in \hat V: A \rightarrow \alpha, \alpha, \in U_n^*\}\cup U_n$ $U_n \cap V, n \rightarrow \infty$ est utile (wtf...)

  6.2 $S \Rightarrow^* A$ $U_0 = \{S\}$, U, $\{A \in \hat V : S \rightarrow \alpha, A \in \alpha\}\cup U_0$, $U_{n+1} = \{A \in \hat V, \exists B \in U_n, B \rightarrow \alpha, A \in \alpha \}\cup U_n$ $U_{n+1}$ est utile

voir les notes esti calisse

Exemple
R=

$S \rightarrow ABA | AC | BB | a$

$A \rightarrow B | Cb | \epsilon$

$B \rightarrow aBa | bCb$

$C \rightarrow Sba | abS$


1. $E_0 = \{A\}$, $E_1 = \{A\} = E$. On enleve $A \rightarrow \epsilon$, on ajoute $S \rightarrow  BA | AB | B | C$
2. UNitaire $A \rightarrow B, S \rightarrow  B, S \rightarrow  C$, $V_B =\{A\}$, on ajoute $A \rightarrow aba | bCb, S\rightarrow aBa | bcB | Sba | abS$ on enleve $A \rightarrow  B$
3. les regles presentement

$S \rightarrow  ABA | BA | AB | AC | BB | a | X_aBX_a | X_bCX_b | SX_BX_a | X_aX_bS$

$A \rightarrow  CX_b | X_aBX_a | X_bCX_b$

$B \rightarrow X_aBX_a | X_bCX_b$

$C \rightarrow SX_bX_a | X_aX_bS$

$X_a \rightarrow  a$

$X_b \rightarrow b$

4. J'enleve
   
   $S \rightarrow  ABA | X_aBX_a | X_bCX_b | SX_bX_a | X_aX_bS$

   $A \rightarrow  X_aBX_a | X_bCX_b$

   $B \rightarrow  X_aBX_a | X_bCX_b$

   $C \rightarrow SX_bX_a | X_aX_bS$
    on ajoute

    $S \rightarrow A Y_1| X_aY_2 | X_bY_3 | SY_4 | X_aY_5$
    $Y_1 \rightarrow BA$

    $Y_2 \rightarrow BX_a$

    $Y_3 \rightarrow CX_b$

    $Y_4\rightarrow X_bX_a$

    $Y_5 \rightarrow X_bS$

    $A \rightarrow  X_aY_2 | X_bY_3$

    $B \rightarrow X_aY_2 | X_bY_3$

    $C \rightarrow SY_4 | X_aY_5$


-------------

## Revision de l'intra

Cest recapitulatif (25-30% sur les langages reguliers) + hors contextes

Des languages, montrer qui sont regulier ou non, hors contexte ou non. 

(mechante revision)

Si $L_{hc}$ sont fermer sous complement, alors $L \in L_{hc} \implies L^c \in L_{hc}$

$L = \{w \in \{a,b,c\}: |w|_a \neq |w|_b ou |w|_b \neq |w|_c ou |w|_a \neq |w|_c\}$

le complement, cest quils sont tous egaux (on la deja montrer au tp8) cest pas HC

L est une union de 3 langages, chacun est hors contexte, HC est fermer pour lunion. donc le HC nest pas fermer sur le complemenet. 

$L_A = \{a^n : n \in \N\}$ $L_B\{b^n : n \in \N\}$ les 2 sont reguliers, mais si on fait la concatenation

$$ L_A \cdot L_B = \{a^nb^m: n,m \in \N\} $$

HC sont fermer sous concatenation

HC ne sont pas fermer sous intersection

----

$L = a^nb^m : n \neq m \notin L_{reg}$

on fait avec MH, donc infinite de classe dequivalence. 

prendre $[a^i]$ comme classes. Si $i \neq j$ $a^ib^i \notin L$ mais $a^jb^i \in L$

(ebauche de preuve la)

avec pompiste $a^pb^q$

si $q = p! + p$ ca va marcher, si $|y| =a^k$ prenons $i = p! /  k$ alors $xy^iz = a^qb^q \notin L$ donc ne respecte pas le pompiste



mq que cest hc

cest lunion de $a^nb^m : m>n$ et $a^nb^m : n>m$

$G_1 = (V, \Sigma, R, S)$

$S_1 \rightarrow CB$

$B \rightarrow  Bb | b$

$C \rightarrow aCb | \epsilon$

$R_2$

$S_2 \rightarrow  AC$

$A \rightarrow  Aa | A$

$C \rightarrow aCb | \epsilon$

au complete : 
R 
$S \rightarrow  S_1|S_2$