# tp3
## correction devoir 3

### No 1

$$ L_1 = \{w \in \{a,b,c,d,e\}^* :\ |w|_a \equiv 3 (\bmod 6)\ ou\ |w|_b \equiv 1(\bmod 2)\} $$

On a lunion de 2 langages a cause du ou

on peut donc les separer en langages A et B
$$ A \cup B = \{w; w \in A\ ou\ w \in B\} = L_1 $$

Prouvons que pour un x in sigma i< k in N
$$ L = \{w \in \Sigma^* : |w|_x \equiv i (\bmod k)\} \in L_{reg} $$

$$M = (Q, \Sigma, \delta, s, F) \\ Q = \{0,...,k-1\}\\ s=0\\ F=\{i\}\\ \delta(q,x) = q+1(\bmod k)\ pour y \in \Sigma^* \setminus x, \delta(q,y)= q$$

Montrons 
$$ L(M)= L:\ w \in L(m) <=> \hat \delta(s,w) \in F <=> \hat \delta(s,w) = i $$

Montrons que 
$$ \forall w \in \Sigma^*, \hat \delta (s,w) = |w|_x (\bmod k).  $$

Base :
$$ \hat \delta (s, \epsilon) = 0 = |\epsilon|_x $$

Supposons
$$ \hat \delta(s,w) = |w|_x\ \forall|w| = n $$
soit
$$ a \in \Sigma \\ a = x: \hat \delta(s,wx) = \delta(\hat \delta(s,w),x)\delta(|w|_x \bmod k, x)= (|w|_x \bmod k +1)(\bmod k) = |wx|_x (\mod k) $$

soit
$$ a \neq x : \hat \delta(s,wa) = \delta(\hat \delta(s,w),a) = \hat \delta(s,w) = |w|_x \bmod k = |wa|_x \bmod k $$
donc
$$ \hat \delta(s,w) = i <=> |w|_x \equiv i \bmod k <=> w \in L \Rightarrow L=L(M)$$
donc en particulier a et B sont reguliers. 

donc A union B est regulier et L_1 eest regulier..

apparamment on peut appliquer cette preuve pour tous les langages regulier qui ont un mod dedans...

### No 2

$$ L_2 = \{w \in \{a,b,c,d,e\}^*:\ |w|_a \equiv |w|_b\ (\bmod 4) \} $$

ca aurait pu etre vu aussi comme 
$$ |w|_a - |w|_b \equiv 0 \bmod 4 $$
ca diminue pas mal le nombre detats (4 etats)
mq 
$$ \hat \delta(s, w) = |w|_a - |w|_b \bmod 4 \\ \hat \delta(s,wa) = \hat \delta(s,w)+1 \bmod 4 = (|w|_a+1)-|w| \bmod 4 = |wa|_a - |wa|_b \bmod 4$$

$$ \hat \delta(s,wb) = \hat \delta(s,w)-1 \bmod 4 = |w|- (|w|_b+1)\bmod 4  = |wb|_a - |wb|_b (\bmod 4)$$

base :
$$ \hat \delta(s, \epsilon) = 0 = |\epsilon|_a - |\epsilon|_b $$

### No 3

elle dit que cest le mot commence et fini par la meme lettre a

MQ
$$ L(M) = \{w \in \{a,b\}: w\ commence\ et\ fini\ par\ a\} $$
$$ w \in L(M) <=> \hat \delta(s,w) \in F =\{s,q\} $$
si
$$ \hat \delta(s,w) = s \Rightarrow w = s <=> \epsilon \in L $$
si
$$ \hat \delta(s,w) = q \Rightarrow |w|_{ab}- |w|_{ba} = 0 $$
 et
$$  \hat \delta(s,w) = p \Rightarrow |w|_{ba} - |w|_{ab} = 1$$

base :
$$ \hat \delta(s,a)  = q$$
et
$$ |a|_{ab} = |a|_{ba} $$
supposon que
$$ \hat \delta(s,w ) =q \Rightarrow |w|_{ab} = |w|_{ba} $$

### no4

on etait good wohooooo

$$ \{a^1b^1\}\cup \{a^2b^2\}\cup ... \cup \{a^nb^n\} $$

-------------------

## AFND
On va lappeler (AFN)

definit
$$ M = (Q, \Sigma, \delta, s, F)  $$

Cest surtout le delta qui change ici

$$ \delta : Q \times (\Sigma \cup \{\epsilon\}) \rightarrow 2^Q\\q \in Q, a \in \Sigma \\ \delta(q,a) \subseteq Q$$

$$ E_0(q)= \{q\}  \\ E_{k+1}(q) = E_k(q) \cup(\cup_{p \in E_k(q)}\delta(p,\epsilon))\\ k \in N $$

$$ E(q)= \Cup_{k<|Q|}E_k(q) $$
ici ce sont les etants atteignables a partir de q par epsilon 

$$ \hat \delta(q, \epsilon) = E(q) \\ \hat \delta(q, wa) = \Cup_{p \in \hat \delta(q,w)}\Cup_{r \in \delta(p,a)}E(r) $$

le langage dune machine AFN

$$ L(M) = \{w \in \Sigma^* : \hat \delta(s,w)\cup F \neq \emptyset \} $$

ex:
![tp3a](tp3a.svg)


| \delta | \epsilon |   a   |    b     |
| :----: | :------: | :---: | :------: |
|   s    |   {1}    |  {2}  | emptyset |
|   1    | emptyset |  {4}  | {s, 3  } |
|   2    |          |       |          |
|   3    |          |       |          |
|   4    |          |       |          |

en gros cest un AFD qui peut avoir plusieurs fleches pour la meme lettre et/ou des fleches pour epsilon

coment on trouve E(s)

$$ E_0 = \{s\} \\ E_1=E_0 \cup \{q_1\} $$
cest tous les etats quon atteint avec 0 et un epsilon

$$ E_2 = E_1 \cup \emptyset = \{s, q_1\}  \\ E(s) = \{s, q_1\}$$

$$ \hat \delta(s, baa)  \\ \hat \delta(s,b) = \{s, q_1, q_3\} \\ \hat \delta(s,ba) = \{q_2, q_3, q_4\}$$
donc
$$ \hat \delta(s, baa) = \{q_1\} $$

On peut prouver quun langage est regulier avec les AFN

soit
$$ w \in L_{fini}\\ w=a_1a_2... a_n  \\ x \ in L_{fini} \\ x=b_1b_2...b_k$$

on peut faire un dessin avec des ... et lecture de a_n et b_k. notre machine reconnait 2 mots et voila, les langages sont regulier pcq on a un AFN qui les reconnait.


--------

Concatenation est reguliere :
soit L_1 et L_2 des langages reguliers, alors
$$ L_1 \cdot L_2 \in L_{reg} $$
$$ L_1 \cdot L_2: \{w \in \Sigma^* : \exists x \in L_1, y \in L_2, x \cdot y = w\} $$
$$ L(M_1) = L \\ L(M_2) = L_2 $$

elle a dessiner des automates ben basics,on ajout un s avant s1 avec une transition epsilon vers s1.  on ajoute des transitions epsilon des etats acceptant de M1 vers s_2

on appelle notre nouvelle machine M
$$ M=(\{s \cup Q_1 \cup Q-2\}, \Sigma, \delta, s, F_2) \\
 \delta(q,a) = 
 \begin{cases}
 \delta_1(q,a)\ si\ q \in Q_1 \\
 \delta_2(q,a)\ si\ q \in Q_2 \\
 \end{cases}

  \delta(q,\epsilon) = 
 \begin{cases}
s_2\ si\ q \in F_1\\
 \delta_2(q,\epsilon)\ si\ q \in Q_i \\
 \end{cases}
 $$