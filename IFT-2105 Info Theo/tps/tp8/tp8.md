# TP8
## Corrections devoir 8

1. $L_1 = \{ w \in \{a,b,c,d\}^*: |w|_a = |w|_b\} \in L_{hc}$

$G_1=(V, \Sigma, R, S)$
R: 
$$ S \rightarrow AaASAbASA| AbASAaASA | \epsilon | A \\ A \rightarrow AA | c | d | \epsilon $$
$V =\{S,A\}, S = S, \Sigma=\{a,b,c,d\}$

2. $L_2 = \{ w \in \{a,b,c,d\}^*: |w|_a \neq |w|_b\} \in L_{hc}$

un truc poru savoir si cest hc, cest lunion de 2 langages hors contexts
$L_2=\{ |w|_a > |w|_b\}\in L_{hc} \cup  \{ |w|_a < |w|_b\}$

on en montre poun des 2 seulements
$$ S' \rightarrow SaS \\ S \rightarrow  AaASAbASA| AbASAaASA | \epsilon | A \\  A \rightarrow AA| a | c | d | \epsilon $$

ca cetait pour $|w|_a > |w|_b$, si on inverse les a et b, on va avoir pour $|w|_a < |w|_b$

$V_2 = \{S, S_a', S_b', S_a, S_b, A_a, A_b\}$

On prend ici toutes les variables quon  utilise en prenant soin de leur donner un autre nom blablabla

3. $L_3 = \{ w : |w|_a = |w|_b = |w|_c \} \notin L_{HC}$

ici le truc cest quon ne peut pas compter 3 affaires en ghc

Supponsons que $L_3 \in L_{Hc}$ alors $L_3$ respecte le pompiste, soit p>0 du pompiste

soit $w=a^pb^pc^p \in L_3$, $|w| \geq p$ Soit u,v,x,y,z $\in$ {a,b,c}* tq
1. $w = uvxyz$
2. $|vxy| \leq p$
3. $|vy|>0$

pcq les a et c les plus proches sont spearer par p symboles, vxy ne contient pas a la fin a, b, c etc. 

supposons sans perte de generatlite que vxy ne contient pas de c, vy contient un a ou un b (ou les 2).

posons $w_0= uv^0xy^0z$ je sais que $|w_0|_c = p$

Cas 1: vy contient un a donc $|w_0|_a < p$ donc $|w_0|_a \neq |w_0|_c$ donc $w_0 \notin L_3$

Cas 2 : vy contient un b, donc $|w_0|_b < p$ donc $|w_0|_b \neq |w_0|_c$ donc $w_0 \notin L_3$

Donc $L_3$ ne respecte pas le pompiste donc $L_3 \notin L_{hc}$


4. $L_4 = \{a^ib^jc^k  : i\ \neq j\ ||\ j\ \neq  k\} \in L_{hc}$

cest fermer sur lunion encore... oops
$L_4 = \{a^ib^jc^k  : i\ \neq j\} \cup \{a^ib^jc^k  : j\ \neq  k\}$

Rappellons nous que i!=j cest aussi que i < j ou i > j.

R: 
$$ P_1 \rightarrow aP_1b | \epsilon \\ P_2 \rightarrow bP_2c | \epsilon \\ A \rightarrow aA | \epsilon \\ B \rightarrow bB | \epsilon \\ C \rightarrow cC | \epsilon $$

$$ S \rightarrow aAP_1C | P_1bBC | AbBP_2 | AP_2cC $$

Cest un bon exemple de langage qui est non regulier qui est hors contexte. 

pour le shit pas dans le devoir
$L_5=\{\ w\in \{a,b\}^*\}, \exists u \in \{a,b\}^*: w = uu$ et $L \notin L_{reg}$

elle fait le pompiste a la vitesse de la lumiere avec $w = a^pba^pb$

pkoi cest pas hors contexte, cest pcq en ghc cest quelque qui est recursif, donc de quoi comme uu ca peut pas marcher

disons quon w = O O, en ghc, on le definierait comme ca w = x0x0 et vu que cest hors contexte le premier x na justement pas acces au context du 2e x, donc ca peut pas marcher

Supposons que $L_5 \in L_{hc}$ Soit p>0 du pompiste hors contexte

$w = a^pba^pb=uvxyz$ disons $v = a^k, y = a^k$ $|vxy| = 2k+1 < p$ a dit ca marche pas

$w = a^pb^pa^pb^p,\ |w| \geq p$, soit $w_1 = a^pb^p$ et $w_2=a^pb^p$

donc $w =uvxyz$, $|vxy| \leq p$, $|vy| > 0$

Cas 1 : vxy est dans $w_1$, premiere moitie, si vy contient un a, alors $uv^0xy^0z$ a seulement une sequece $a^p$ donc dans $w_2$ donc $uv^0xy^0z \notin L_5$

si vy contient un b, $uv^0xy^0z$ seulement une sequence $b^p$ donc $uv^0xy^0z \notin L_5$

Cas 2: vxy est dans $w_2$, deuxieme moitie, idem a cas 1

Cas 3 vxy chevauche $w_1$ et $w_2$ vxy ne contient pas de a de $w_1$ ni de b de $w_2$ car $|vxy| \leq p$

Avec  $uv^0xy^0z$, si vy contient des a, on a juste fois $a^p$ (au debut) si vy contient des v, on a juste une fois $b^p$ (a la fin)

Donc  $uv^0xy^0z \notin L_5$. Donc $L_5$ ne respecte pas le pompiste donc $L_5 \notin L_{hc}$

-----------

## Forme normale de chomsky

Forme des regles

$S \rightarrow \epsilon$, $A\rightarrow BC (A,B,C \in V, B,C \notin S$, $A \rightarrow a (A \in V, a \in \Sigma$
Toutes variables est utiles.

Algo : 
1. Si on a S a droite dans une regle, ajouter $S' \rightarrow S$ S' est la nouvelle var initiale.
2. Pour chaque $a \in \Sigma$ ajouter $N_a \rightarrow a$ et remplacer a par $N_a$ dans les regles
3. Pour chaque regle $A \rightarrow B_1B_2...B_K, k \geq 3$, creer $D_1, D_2, ...., D_{k-2}\rightarrow B_{k-1}B_k$ et enlever $A \rightarrow B1...B_k$
4. Pour chaque regle $A \rightarrow \epsilon, A \neq S'$ enlever la regle, si on a $B \rightarrow AC$ ajouter $B \rightarrow  C$, si on a $B \rightarrow A$, ajouter $B \rightarrow  \epsilon$, si $B \rightarrow AA$, ajouter $B \rightarrow A |\epsilon$ et repeter letape tant quil reste des $A \rightarrow \epsilon$