<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml"
    xmlns:bibli="bib.xml"
    exclude-result-prefixes="xs" version="2.0">
    <xsl:output method="xml" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
        doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" indent="yes"/>

   
    
    <!-- Search parameters-->
    <xsl:param name="titreRecherche" select="''"/>
    <xsl:param name="minPrix" select="0"/>
    <xsl:param name="maxPrix" select="100"/>
    
    <xsl:template match="/">
        <html>
            <!-- Head tags to import bootstrap-->
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                <meta charset="UTF-8" content="width=device-width, maximum-scale=1, minimum-scale=1"/>
                <link rel="stylesheet" href="cssStyles.css"/>
                <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"/>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"/>
                <link rel="stylesheet"
                    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
                    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
                    crossorigin="anonymous"/>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"/>
                <script src="https://code.jquery.com/jquery-3.2.1.min.js"/>
            </head>
            <body>
                <!-- Bootstrap nav-->
                <nav class="navbar navbar-expand-lg navbar-dark">
                    <a href="/">
                        <img
                            src="https://ville.montreal.qc.ca/idmtl/en/wp-content/uploads/sites/2/2016/09/udem.jpg"
                            alt="Logo" style="width: 150px;"/>
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"/>
                    </button>
                    <h1 style="color:#d7d5d7">Bibliotheque</h1>
                    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <ul class="navbar-nav btn ml-auto">
                            <li class="nav-item">
                                <a class="nav-link text-primary" href="./auteurs.xhtml">Auteurs</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-primary" href="./livres.xhtml">Livres</a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <br/>
                <div class="wrapper">
                    <h2>Livres</h2>
                    <table class="table table-responsive table-striped maintable">
                        <tr>
                            <th>Titre</th>
                            <th>Annee</th>
                            <th>Langue</th>
                            <th>Prix</th>
                            <th>commentaire</th>
                            <th>Couverture</th>
                            <th>Auteurs</th>
                        </tr>
                        <xsl:apply-templates select="bibliotheque/livres">
                           
                        </xsl:apply-templates>


                    </table>
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="livres">
        <xsl:for-each select="livre">
            <xsl:sort select="id(@auteurs)/nom" order='ascending'/>
            <xsl:if
                test="contains(titre, $titreRecherche) and (prix &lt;= $maxPrix) and (prix &gt;= $minPrix)">
                <tr>
                    <td>
                        <xsl:value-of select="titre"/>
                    </td>
                    <td>
                        <xsl:value-of select="annee"/>
                    </td>
                    <td>
                        <xsl:value-of select="@langue"/>
                    </td>
                    <td>
                        <xsl:value-of select="prix"/>
                        <xsl:choose>
                            <xsl:when test="prix/@devise = 'CAD'">$<xsl:value-of
                                    select="prix/@devise"/></xsl:when>
                            <xsl:when test="prix/@devise = 'USD'">$<xsl:value-of
                                    select="prix/@devise"/></xsl:when>
                            <xsl:when test="prix/@devise = 'EUR'">€<xsl:value-of
                                    select="prix/@devise"/></xsl:when>
                            <xsl:when test="prix/@devise = 'AUD'">$<xsl:value-of
                                    select="prix/@devise"/></xsl:when>
                            <xsl:when test="prix/@devise = 'GBP'">£<xsl:value-of
                                    select="prix/@devise"/></xsl:when>
                        </xsl:choose>
    
    
                    </td>
                    <td class='bookComments'>
                        <xsl:choose>
                            <xsl:when test="commentaire">
                                <xsl:value-of select="commentaire"/>
                            </xsl:when>
                            <xsl:otherwise> </xsl:otherwise>
                        </xsl:choose>
                    </td>
                    <td>
                        <xsl:choose>
                            <xsl:when test="couverture">
                                <img class="bookCover">
                                    <xsl:attribute name="src">
                                        <xsl:value-of select="couverture"/>
                                    </xsl:attribute>
                                </img>
                            </xsl:when>
                            <xsl:otherwise> </xsl:otherwise>
                        </xsl:choose>
    
                    </td>
                    <td>
                        <ul>
                            <xsl:variable name="tok" select="id(@auteurs)"/>
                            <xsl:for-each select="id(@auteurs)">
                                <li><xsl:value-of select="nom"/>, <xsl:value-of select="prenom"/>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </td>
                </tr>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
