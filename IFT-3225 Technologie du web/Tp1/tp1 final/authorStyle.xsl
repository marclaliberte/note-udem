<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="xs" version="2.0">
    <xsl:output method="xml" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
        doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" indent="yes"/>
    <xsl:key name="book" match="livre" use="@auteurs"/>
    
    <xsl:param name="recherche" select="''"/>
    
    <xsl:template match="/">
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                <meta charset="UTF-8" content="width=device-width, maximum-scale=1, minimum-scale=1"/>
                <link rel="stylesheet" href="cssStyles.css"/>
                <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"/>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"/>
                <link rel="stylesheet"
                    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
                    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
                    crossorigin="anonymous"/>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"/>
                <script src="https://code.jquery.com/jquery-3.2.1.min.js"/>
            </head>
            <body>
                <nav class="navbar navbar-expand-lg navbar-dark">
                    <a href="/">
                        <img
                            src="https://ville.montreal.qc.ca/idmtl/en/wp-content/uploads/sites/2/2016/09/udem.jpg"
                            alt="Logo" style="width: 150px;"/>
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"/>
                    </button>
                    <h1 style="color:#d7d5d7">Bibliotheque</h1>
                    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <ul class="navbar-nav btn ml-auto">
                            <li class="nav-item">
                                <a class="nav-link text-primary" href="./auteurs.xhtml">Auteurs</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-primary" href="./livres.xhtml">Livres</a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <br/>
                <div class="wrapper">
                    <h2>Auteurs</h2>
                    <table class="table table-responsive maintable">
                        <tr>
                            <th>Prenom</th>
                            <th>Nom</th>
                            <th>Pays</th>
                            <th style="width:50px">Commentaire</th>
                            <th>Photo</th>
                            <th>Livres</th>
                        </tr>
                        <xsl:apply-templates select="bibliotheque/auteurs/auteur">
                            <xsl:sort select="nom" order="ascending"/>
                            <xsl:sort select="prenom" order="ascending"/>
                        </xsl:apply-templates>

                    </table>
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="auteur">
       
        <xsl:if test="contains(nom, $recherche)">
        <tr>
            <td>
                <xsl:value-of select="prenom"/>
            </td>
            <td>
                <xsl:value-of select="nom"/>
            </td>
            <td>
                <xsl:value-of select="pays"/>
            </td>
            <td>
                <xsl:choose>
                    <xsl:when test="commentaire">
                        <xsl:value-of select="commentaire"/>
                    </xsl:when>
                    <xsl:otherwise> </xsl:otherwise>
                </xsl:choose>
            </td>
            <td>
                <img class='imgAuthor'>
                    <xsl:attribute name="src">
                        <xsl:value-of select="photo"/>
                    </xsl:attribute>
                </img>

            </td>
            <td class='authorBookSection'>
                
             
                    <xsl:variable name="id" select="@ident"/>
                
                   <xsl:for-each select="//bibliotheque/livres/livre">
                       <xsl:sort select="prix" order='descending' data-type="number"/>
                      <xsl:if test="contains(@auteurs,$id)">
                              <div class="card livres">
                                  <div class="card-header"><xsl:value-of select="titre"/></div>
                                  <div class="card-body">
                                      Annee: <xsl:value-of select="annee"/>
                                      <br/>
                                      Prix : <xsl:value-of select="prix"/><xsl:text> </xsl:text><xsl:value-of select="prix/@devise"/>
                                      <br/>
                                      <xsl:choose>
                                          <xsl:when test="commentaire">
                                              <span class="authorBookComments">Commentaire: <xsl:value-of select="commentaire"/></span>
                                          </xsl:when>
                                          <xsl:otherwise> </xsl:otherwise>
                                      </xsl:choose>
                                      <br/>
                                      <xsl:choose>
                                          <xsl:when test="couverture">
                                              <img>
                                                  <xsl:attribute name="src">
                                                      <xsl:value-of select="couverture"/>
                                                  </xsl:attribute>
                                                  
                                              </img>
                                          </xsl:when>
                                          <xsl:otherwise> </xsl:otherwise>
                                      </xsl:choose>
                                  </div>
                                  
                              
                          </div>
                      </xsl:if>
                       
                   </xsl:for-each>
               
            </td>
        </tr>
        </xsl:if>
    </xsl:template>


</xsl:stylesheet>
