var loremIpsum = require('lorem-ipsum');

var vowelsMediv = ['a', 'e', 'i', 'o', 'u', 'y', 'ie', 'ae', 'ui', 'ion'];
var medieval = [
    'tau',
    'iel',
    'ius',
    'd',
    'g',
    'k',
    'm',
    'p',
    'x',
    'th',
    'r',
    'kh',
    'gh',
    'ro',
    'rd',
    'uk',
    'ok',
    'il',
    'kan',
    'gn',
    'md',
    'gr',
    'hel',
    'gon',
    'wen',
    'hil',
    'mn',
    'nor',
    'rod',
    'gw',
    'thr',
    'dha',
    'ech',
    'oth',
    'abd',
    'rk'
];
var title = [
    'Destroyer',
    'Barbarian',
    'Just',
    'Evil',
    'Eater of Worlds',
    'Paragon',
    'Twisted',
    'Spiked',
    'Explorer',
    'Invisible',
    'Quick',
    'Deadly',
    'Kingslayer',
    'Dead',
    'Hunter',
    'Protector',
    'Nimble',
    'Savage',
    'Firelord',
    'Knight',
    'Battle Lord',
    'Challenged',
    'Broken',
    'Dark Master',
    'HighBorn',
    'LowBorn',
    'Gladiator',
    'Bastard',
    'Gorgeous',
    'Gullible',
    'Slayer',
    'Undying',
    'Vanquisher',
    'Betrayer',
    'Relentless',
    'Breaker of Chains',
    'Mad King'
];
var alpha2 = [vowelsMediv, medieval];

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function generateMedievalName() {
    var x = Math.round(Math.random());
    var result = '';
    for (var i = 0; i < 5; i++) {
        result += alpha2[x % 2][Math.floor(Math.random() * alpha2[x % 2].length)];
        if (i >= 2) {
            if (Math.random() < 1 / 3) {
                break;
            }
        }
        x++;
    }
    return capitalizeFirstLetter(result);
}

function generateTitle() {
    return getRandomfromArray(title);
}

function generateNameWithTitle() {
    return generateMedievalName() + ' the ' + generateTitle();
}
function randomWord() {
    var result = '';
    //des mots de 2-10 lettres
    var k = getRandomfromRange(2, 10);
    for (var i = 0; i < k; i++) {
        result += String.fromCharCode(getRandomfromRange(97, 122));
    }
    return result;
}

function randomPhrase() {
    var result = '';
    //2-8 mots par phrase
    var k = getRandomfromRange(2, 8);
    for (var i = 0; i < k; i++) {
        var newWord = randomWord();
        if (i == 0) {
            newWord = capitalizeFirstLetter(newWord);
        }
        if (i != k - 1) {
            newWord += ' ';
        }
        result += newWord;
    }

    result += '. ';
    return result;
}

function getRandomfromArray(array) {
    return array[Math.floor(Math.random() * array.length)];
}

function getRandomfromRange(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}

function randomDate() {
    var start = new Date('2018-01-01');
    var end = new Date('2018-12-31');
    var date = new Date(+start + Math.random() * (end - start));
    return date;
}

function date() {
    var today = randomDate();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    return yyyy + '-' + (mm < 10 ? '0' + mm : mm) + '-' + (dd < 10 ? '0' + dd : dd);
}

function now() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    var hours = today.getHours();
    var min = today.getMinutes();
    var sec = today.getSeconds();
    return (today = yyyy + '-' + (mm < 10 ? '0' + mm : mm) + '-' + (dd < 10 ? '0' + dd : dd));
}

function genLoremParagraph() {
    return loremIpsum({count:1, units:'paragraphs'})
}

function genLoremTitle() {
    return loremIpsum({count:Math.floor(Math.random() * 5) + 1, units:'words',})
}

function genLoremSentence() {
    return loremIpsum({count:1, units:'sentences',})
}


module.exports = {
    capitalizeFirstLetter: capitalizeFirstLetter,
    randomWord: randomWord,
    randomPhrase: randomPhrase,
    getRandomfromArray: getRandomfromArray,
    getRandomfromRange: getRandomfromRange,
    date: date,
    now: now,
    generateNameWithTitle: generateNameWithTitle,
    generateMedievalName: generateMedievalName,
    generateTitle: generateTitle,
    genLoremParagraph: genLoremParagraph,
    genLoremTitle: genLoremTitle,
    genLoremSentence: genLoremSentence
};
