var fs = require('fs');
var genHelper = require('./genHelpers.js');
var randomCountry = require('random-country');

var base = `<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="mainSchema.rnc" type="application/relax-ng-compact-syntax"?>
<bibliotheque>`
var footer = `</bibliotheque>`

var languages = ['Fr', 'En', 'De', 'Sp', "Zh"]
var devises = ["CAD" , "EUR",  "USD" ,  "AUD", "GBP"]
var photoURls = ["https://d28hgpri8am2if.cloudfront.net/tagged_assets/1967155/1666839_th.jpg",
"https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/George_R._R._Martin_by_Gage_Skidmore_2.jpg/800px-George_R._R._Martin_by_Gage_Skidmore_2.jpg",
"https://hpmedia.bloomsbury.com/rep/files/shot-c-021_withcredit_4001.png",
"https://www.telegraph.co.uk/content/dam/books/2015-08/11aug/Tolkein.jpg?imwidth=1400",
]

var coverUrls=["https://images.gr-assets.com/books/1436732693l/13496.jpg",
"https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9781/4736/9781473676350.jpg",
"https://hpmedia.bloomsbury.com/rep/s/9781408855942_309034.jpeg",
]


var authorIDs= [];
var inc =1;

function generateId(prenom, nom){
    var id = nom.charAt(0)+prenom.toUpperCase();
    id = id.split(" ").join("");
    if(authorIDs.includes(id)){
        id+=inc;
        inc++;
    }
    authorIDs.push(id);
    return id;
}

function getAuthorIds(){
    var output = ""
    var range = genHelper.getRandomfromRange(1,3);
    for(var i=0;i<range;i++){
        
        output+= genHelper.getRandomfromArray(authorIDs)
        if(i<range-1)
            output+=" "
    }
    return output;
}


function generateLivres(count){
    var output = "<livres>";
   

    for(var i=0;i<count;i++){
        var lang = genHelper.getRandomfromArray(languages);
        var authorID = getAuthorIds();
        var title = genHelper.capitalizeFirstLetter(genHelper.genLoremTitle());
        var annee = "20"+genHelper.getRandomfromRange(10,99);
        var prix = genHelper.getRandomfromRange(0,100);
        if(prix<100)
            prix+="."+genHelper.getRandomfromRange(10,99);
        else
            prix+=".00"
        var devise = genHelper.getRandomfromArray(devises);
        var comment = genHelper.genLoremParagraph().substring(0,59);
        var cover = genHelper.getRandomfromArray(coverUrls)

        var livre = `<livre langue="${lang}" auteurs="${authorID}">`
        livre += `<titre>${title}</titre>`
        livre += `<annee>${annee}</annee>`
        livre += `<prix devise="${devise}">${prix}</prix>` 
        if(Math.random()>0.5){
            livre += `<couverture>${cover}</couverture>`
        }
        if(Math.random()>0.5){
            livre += `<commentaire>${comment}</commentaire>`
        }
        livre += "</livre>"
        output += livre;
    }
    output += "</livres>"
    return output;

}



function generateAuteurs(count){
    var output = "<auteurs>\n"
    for(var i =0; i<count;i++){

        var prenom = genHelper.generateMedievalName();
        var nom = genHelper.generateTitle();
        var pays = randomCountry({ full: true });
        var id = generateId(prenom, nom);
        var comment = genHelper.genLoremParagraph().substring(0,59);
        var photo = genHelper.getRandomfromArray(photoURls)

        var auteur = `<auteur ident="${id}">`
        if(Math.random()>0.5){
            auteur += `<nom>${nom}</nom>\n<prenom>${prenom}</prenom>`
        }else{
            auteur += `<prenom>${prenom}</prenom>\n<nom>${nom}</nom>`
        }
        auteur += `<pays>${pays}</pays>`
        if(Math.random()>0.5){
            auteur += `<commentaire>${comment}</commentaire>`
        }
        auteur += `<photo>${photo}</photo>`
        auteur += "</auteur>"
        output += auteur;
    }
    output += "</auteurs>"
    return output;
}






function generateXml(){
    var output = base;
    output += generateAuteurs(300)
    output += generateLivres(800)
    output += footer;
    return output;
}



fs.writeFile('../bibli.xml', generateXml(), function(err) {
    if (err) {
        return console.log(err);
    }

    console.log('The XML file was saved!');
});