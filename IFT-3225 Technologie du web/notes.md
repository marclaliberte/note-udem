# Technologie du web

## Cours 1

### XHMTL 1.0 strict
1. Langague stricte
2. Separer contenu de la forme
3. suit la structure et syntaxe de XML

moins de balises plus de id : on utilise plus de la meme style de balise, mais ils vont avoir des ID (ou classe) differents. ex: beaucoup de div avec des ids differentes. 

ancetre de hmtl : SGML

pkoi cest strict : pour que les browsers puissent lindexer correctement.

quand on revoit un xhtml, on peut savoir que si on fait une erreure, notre code va poser probleme. donc le xhtml rend le tout plus maintenable. 

### url vs uri
url vs uri :
url : localisateur vers des pages web
uri : les ressources qui peuvent dans une page web. 

un url peut etre un uri mais linverse n'est pas vrai. 

ce qui est en flash (contenu) nest pas indexable par les moteurs de recherches. 


## Cours 2 - CSS
- classe generique : cest une classe qui est dans le tag classe et en css cest ce qui est .class
- classe specifique : cest ce qui est specifier par balise.classe 

la difference entre 
- body p = tous les p dans body peu importe leur parent
- body>p = les p qui sont enfants immediats de body

## Cours 3 - css selector plus difficiles

**id est plus prioritaire que la classe**

div + p	Selects all p elements that are placed immediately after div elements
p ~ ul	Selects every ul element that are preceded by a p element

https://www.w3schools.com/cssref/css_selectors.asp

li : first-child = tous les li qui sont premiers enfants de leur parent. 

## Cours 4 - XML

le doc type, cest le dtd qui annonce les regles qui sont definis dans ce document. donc xhtml est un document xml aussi

le xml permet de modeliset un domaine particulier (etudiants) et chaque etudiant par exemple aurait un cv propre a lui. il est serais possible davoir un validateur de cv (xml parser) pour toutes la base de donnees. permetterais davoir une presentation de tous les cv uniforme, peut importe le gabarit utiliser a la base pour les cvs. 

**xhtml a ete recommender en 2001**

schemas = le truc quon declare en haut (DTD, xml schemas, relax ng)

sans schemas, on ne peut pas faire de verification de syntaxe

difference entre bien former et valide cest que le schema va valider le document. 

sil y une declaration de DTD en haut, cest quon peut dire que le document est valide.

dans le dtd, on peut choisir comment imbriquer les balises et est-ce que lordre est important ou non, le contenu des balises, (le style de valeur, format des emails, range de int, no de telephone)

dans le dtd de xml, le br est declarer comme ca

`<!ELEMENT br EMPTY>`

une composition : 

`<!ELEMENT etudiant (nom, prenom, note?,cours+)>`


si on decide ce mettre note dans cours

``` xml
<!ELEMENT nom (#PCDATA)>
<!ELEMENT cours (note?)>
<!ELEMENT note (#PCDATA)>
```
## Cours 5 - XML suite

xhtml est un format xml valide pcq il declare un schema. 

les 3 regles

1. balise ouvrante doit donner une balise fermante
2. lordre d'imbrications est important.
3. la valeure des attributs doit etre guillemet.

le schema declarer contient les regles du modele. 

ex
``` xml
<!ELEMENT personne (nom, prenom+, tel?, email?, adresse) >
<!ELEMENT nom (#PCDATA) >
<!ELEMENT prenom (#PCDATA) >
<!ELEMENT tel(#PCDATA) >
<!ELEMENT email (#PCDATA) >
<!ELEMENT adresse (ANY) >
```
ici la racine du document est personne

document xml

``` xml
<xml version="1.0" encoding="UTF-8">
<!DOCTYPE personne SYSTEM "info.dtd">
<personne>
    <nom>Mon Nom</nom>
    <prenom>Prenom1</prenom>
    <prenom>Prenom2</prenom>
    <email>lesle@brule.com</email>
    <adresse>2020 du finfin</adresse>
</personne>

```

ca serais une meilleure pratique de declarer des tags pour adrsese

```xml
<!ELEMENT adresse (no, code, rue) >
```
ca ce veut dire qu'on doit respecter cet ordre (gauche a droite)

comment changer le dtd pour quon le mettre dans nimporte quel ordre?

```xml
<!ELEMENT adresse (no | code| rue)+ >
```
mais ca fait en sorte quon peut en mettre autant qu'on veut, cest une des limites de dtd. 

exemple dtd(2) cest une declaration interne dans le document xml.

on fait linverse, il nous donne le xml, on doit faire le dtd

``` xml
<!ELEMENT catalog (cd+)>
<!ELEMENT cd (title, artist, country, company, price, year)>
<!ATTLIST cd code CDATA #IMPLIED>
<!ELEMENT title (#PCDATA)>
<!ATTLIST tile tag CDATA>
<!ELEMENT country(#PCDATA)>
<!ELEMENT company (#PCDATA)>
<!ELEMENT price (#PCDATA)>
<!ELEMENT year (#PCDATA)>
```

autre exemple

- un base de donnees detudiants
- cotient au moins 1 etudiant
- chaque etudian est identifie par son code permanent, caracteries par son nom, preno, et email facultatif dans nimporte quel ordre
- un etudiant a plusieurs cours
- un cours est identifie par son matricule

```xml
<!ELEMENT etudiants (etudiant+)>
<!ELEMENT etudiant ((nom| prenom| email?)+ , cours+)>
<!ATTLIST etudiant codePermanent CDATA #REQUIRED>
<!ELEMENT nom (#PCDATA)>
<!ELEMENT prenom (#PCDATA)>
<!ELEMENT email (#PCDATA)>
<!ELEMENT cours (#PCDATA)>
<!ATTLIST cours matricule CDATA #REQUIRED>
```
```xml
<etudiants >
    <etudiant codeParmanent="1234">
        <prenom>Prenom</prenom>
        <nom>Nom</nom>
        <cours matricule="98391823"/>
        <cours matricule="alksjdklasjd"/>
    </etudiant>
     <etudiant codeParmanent="1435476">
        <nom>Nom2</nom>
        <prenom>Prenom2</prenom>
        <cours matricule="asdasdsglk"/>
    </etudiant>
</etudiants >
```
## Cours 9 - RelaxNG Compact

exemple de relaxNG

Une seule occurence de chaque balise, mais nimporte quel ordre
element x{A&B&C}
```xml
<x>
    <a></a>
    <b></b>
    <c></c>
</x>
```
Plusieurs occurences de la meme chose, nimorte quelle ordre mais en triplet (pas deux A de suite par exemple)
element x{(A&B&C)+}
```xml
<x>
    <b></b>
    <a></a>
    <c></c>
    <c></c>
    <a></a>
    <b></b>
</x>
```
element x{(A|B|C)*}
```xml
<xsl:choose>
    <a></a>
    <a></a>
    <b></b>
    <c></c>
<xsl:otherwise>
```
## XSLT

en xml le \ est un pointeur vers l'arborescence

le prof dit que ce lien est tres importnat

https://docs.microsoft.com/fr-fr/previous-versions/ms256086(v=vs.120)

## Cours 10 - XSLT (suite)

xslt = prend le xml file et le transorme en un autre document, ex xhtml

il utilise surtout des templates, notammment le template de base qui match a la racine du xml. 

```xml
<xsl:function ...>
    <xsl:param>
```

quand on appel une function

```xml
<xsl:value-of select="fct(param)">
```
et le param est un XPath

pour caller un template avec param

```xml
<xsl:call-template name="fact">
    <xsl:with-param name="paramName" select="XPath">
</xsl:call-template>

<xsl:template name="fact">
<xsl:param>
```

## par rapport a lintra
- questions de cours 30pts (avantages de ca p/r a ca, differences, etc.)
- xslt tres similaire au tp
- relax ng pas de dtd
- xml
- un peu de css (css selectors)

types de xhtml?
- stricts (lui du cours)
- transitionnels (similaire au html)
- framesets (deprecated)

valide vs bien former?
- bien former : respecte les regles du xml de base (balises ouvrantes et fermantes, contient une racine)
- valide : verifier par un schema

url = localization de page web
uri = meme forme, mais peut ne pas etre url, cest pour identifier les ressources sur le web (namespace)

## cours 11 
### Rappel XSLT


ici on voulait afficher le nom des facultes qui sont dans le meme cartier que luniversite
```xml
<xsl:apply-template select="/Universite/faculte[emplacement=//Universite/@cartier]">
```
afficher tous les livres dun auteurs

```xml
<xsl:template name='fact'>
    <xsl:param name='n'/>
    <xsl:choose>
        <xsl:when test='n=1'>
            <xsl:value-of select='$n'>
        </xsl:when>
        <xsl:otherwise>
            <xsl:call-template name='fact'> 
                <xsl:with-parm name='n' select='$n*($n-1)'/>
            </xsl:call-template>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>
```

si cest demander la somme, quantite, position, il faut utiliser sum(), count(), position(). le prof dit qu'un bon xpath va regler bien des problemes.

### on va essayer de prendre des notes

pkoi ces juste en minuscules, cest a cause des variables d'environnement de certains OS qui ne serait qu'en majuscule. 

== cest la valeure
=== cest valeur et type

```php
$var = "test";
"$var hello"
```
va etre un automatique string literal, mais ca ne marchera pas avec juste ''.

### Cours 15-16-17... je sais pu...

besoins de 3 fichiers php pour mysql; 1 pour la config, 1 pour louverture et 1 pour la fermeture

si on veut pas faire de try catch, on peut faire

```php
mysql_connect($sql, $conn) or die("Error");
```

```php
$row = mysql_fetch_assoc($res)
```
retourne ligne par ligne de $res a chaque call, $row devient un tableau associatif.

```php
mysql_num_rows($res)
```
retourne le nombre de ligne de $res

implode — Join array elements with a string


## AJAX

le prof explique le schema : le gros de ses explications est qu'avec la maniere traditionnelle, quand le client fait une request au server, un nouveau fichier html est envoyer, donc la page est refreshed on the client side

le principe d'ajax est que le ajax engine fait la request au serveur, le serveur retourne un text,json ou un xml a la ademande du engine. le engine, comme il est dans le JS, on a acces au DOM et on peut modifier la page en "runtime" sans refresh la page. je trouve le prof over complique les choses encore une fois.

dans son exemple il utilise l'objet `XmlHTTPRequest`

la reponse du serveur est traiter dans le ajax engine avec un callback.

tout le principe est tres certainement a lexamen. 

voici la syntax avec jquery qui simplifie le tout

```js
$.ajax({
    type: 'POST',
    url: '/getMonthPosts/'+getNowMonth(),
    data: {},
    success: function(result) {
        $('#newsContainer').html(parseNews(result));
    },
});
```

voici un exemple du W3
```html
<html>
<head>
<script>
function showHint(str) {
    if (str.length == 0) { 
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET", "gethint.php?q=" + str, true);
        xmlhttp.send();
    }
}
</script>
</head>
<body>

<p><b>Start typing a name in the input field below:</b></p>
<form> 
First name: <input type="text" onkeyup="showHint(this.value)">
</form>
<p>Suggestions: <span id="txtHint"></span></p>
</body>
</html>
```